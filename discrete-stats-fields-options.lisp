;; *-* lisp *-

;; Copyright (C) 2020 by Tim Josling. See BSD.txt for terms.

;; Which counters to report, exclude, synonyms, order

(defconstant+ +extra-accumulator-labels+
  '(
    ix-population
    ix-alive-count
    ix-active-count
    ix-infectious-count
    ix-hedonic-cost
    ix-qaly-lost-total-hedonic-cost
    ix-icu-needed-days-hedonic-cost
    ix-mask-days-hedonic-cost
    ix-quarantine-effectiveness-days-hedonic-cost
    ix-recuperating-days-hedonic-cost
    ix-social-isolation-effectiveness-days-hedonic-cost
    ix-social-isolation-a-effectiveness-days-hedonic-cost
    ix-social-isolation-b-effectiveness-days-hedonic-cost
    ix-social-isolation-c-effectiveness-days-hedonic-cost
    ix-symptomatic-days-hedonic-cost
    ix-taiwan-isolation-effectiveness-days-hedonic-cost
    ix-economic-cost
    ix-qaly-lost-total-economic-cost
    ix-icu-days-economic-cost
    ix-mask-days-economic-cost
    ix-quarantine-effectiveness-days-economic-cost
    ix-recuperating-days-economic-cost
    ix-social-isolation-effectiveness-days-economic-cost
    ix-social-isolation-a-effectiveness-days-economic-cost
    ix-social-isolation-b-effectiveness-days-economic-cost
    ix-social-isolation-c-effectiveness-days-economic-cost
    ix-symptomatic-days-economic-cost
    ix-taiwan-isolation-effectiveness-days-economic-cost
    ix-total-cost
    ix-last-dayix
    ix-dayix-adjustment-factor
    ix-rate-adjustment-factor
    ix-life-years-lost
    ix-qaly-lost-total
    ix-qaly-lost-death
    ix-qaly-lost-disability
    ix-icu-days
    ix-icu-needed-days
    ix-mask-days
    ix-quarantine-effectiveness-days
    ix-recuperating-days
    ix-social-isolation-effectiveness-days
    ix-social-isolation-a-effectiveness-days
    ix-social-isolation-b-effectiveness-days
    ix-social-isolation-c-effectiveness-days
    ix-symptomatic-days
    ix-taiwan-isolation-effectiveness-days
    ))

(defparameter *counters-accumulators-to-exclude*
  (if t
      ;;all the fields are useful for debug, but turn some off for use
      nil
      (list
       "IX-COUNT-TURNED-ON-PERSON-ICU-BED-ALLOCATED-BB"
       "IX-COUNT-TURNED-ON-PERSON-IN-QUARANTINE-DIAGNOSED-BB"
       "IX-COUNT-TURNED-ON-PERSON-QUARANTINE-BED-ALLOCATION-FAILURE-BB"
       "IX-COUNT-TURNED-ON-PERSON-CONTACT-TRACING-BB"
       "IX-COUNT-EVER-PERSON-RECENTLY-TESTED-BB"
       "IX-COUNT-TURNED-ON-PERSON-RECENTLY-TESTED-BB"
       "IX-COUNT-TURNED-ON-PERSON-IN-ISOLATION-BB"
       "IX-COUNT-TURNED-ON-PERSON-IN-TAIWAN-ISOLATION-BB"
       "IX-COUNT-TURNED-ON-PERSON-DELIBERATELY-INFECTED-BB"
       "IX-COUNT-TURNED-ON-PERSON-IN-QUARANTINE-DELIBERATELY-INFECTED-BB"
       "IX-COUNT-TURNED-ON-PERSON-IN-QUARANTINE-VULNERABLE-BB"
       "IX-COUNT-TURNED-ON-PERSON-EXOGENOUSLY-INFECTED-BB"
       "IX-COUNT-TURNED-ON-PERSON-HAS-BEEN-RECENTLY-INFECTED-BB"
       "IX-COUNT-TURNED-ON-PERSON-HAS-BEEN-EVER-INFECTED-BB"
       "IX-COUNT-TURNED-ON-PERSON-ANTIGEN-TESTED-POSITIVE-BB"
       "IX-COUNT-TURNED-ON-PERSON-ANTIGEN-TESTED-NEGATIVE-BB"
       "IX-COUNT-TURNED-ON-PERSON-RECUPERATING-BB"
       "IX-COUNT-TURNED-ON-PERSON-RECENTLY-DECEASED-BB"
       "IX-COUNT-TURNED-ON-PERSON-STATE-NEVER-INFECTED"
       "IX-COUNT-TURNED-ON-PERSON-STATE-INFECTED"
       "IX-COUNT-TURNED-ON-PERSON-STATE-SHEDDING"
       "IX-COUNT-TURNED-ON-PERSON-STATE-SYMPTOMATIC"
       "IX-COUNT-TURNED-ON-PERSON-STATE-ICU-NEEDED"
       "IX-COUNT-TURNED-ON-PERSON-STATE-DECEASED"
       "IX-COUNT-CURRENT-PERSON-STATE-DECEASED"
       "IX-COUNT-TURNED-ON-PERSON-STATE-IMMUNIZED"
       "IX-COUNT-CURRENT-PERSON-STATE-IMMUNIZED"
       "IX-COUNT-EVER-PERSON-STATE-STILL-SHEDDING"
       "IX-COUNT-TURNED-ON-PERSON-STATE-STILL-SHEDDING"
       ))
  "string names of counters to exclude")

(defparameter *counters-accumulators-synonyms*
  '((IX-COUNT-EVER-PERSON-ICU-BED-ALLOCATED-BB EVER-ICU-BED-ALLOCATED)
    (IX-COUNT-TURNED-ON-PERSON-ICU-BED-ALLOCATED-BB TURNED-ON-ICU-BED-ALLOCATED)
    (IX-COUNT-CURRENT-PERSON-ICU-BED-ALLOCATED-BB CURRENT-ICU-BED-ALLOCATED)
    (IX-COUNT-EVER-PERSON-ICU-BED-ALLOCATION-FAILURE-BB EVER-ICU-BED-ALLOCATION-FAILURE)
    (IX-COUNT-TURNED-ON-PERSON-ICU-BED-ALLOCATION-FAILURE-BB TURNED-ON-ICU-BED-ALLOCATION-FAILURE)
    (IX-COUNT-CURRENT-PERSON-ICU-BED-ALLOCATION-FAILURE-BB CURRENT-ICU-BED-ALLOCATION-FAILURE)
    (IX-COUNT-EVER-PERSON-IN-QUARANTINE-DIAGNOSED-BB EVER-IN-QUARANTINE-DIAGNOSED)
    (IX-COUNT-TURNED-ON-PERSON-IN-QUARANTINE-DIAGNOSED-BB TURNED-ON-IN-QUARANTINE-DIAGNOSED)
    (IX-COUNT-CURRENT-PERSON-IN-QUARANTINE-DIAGNOSED-BB CURRENT-IN-QUARANTINE-DIAGNOSED)
    (IX-COUNT-EVER-PERSON-QUARANTINE-BED-ALLOCATION-FAILURE-BB EVER-QUARANTINE-BED-ALLOCATION-FAILURE)
    (IX-COUNT-TURNED-ON-PERSON-QUARANTINE-BED-ALLOCATION-FAILURE-BB TURNED-ON-QUARANTINE-BED-ALLOCATION-FAILURE)
    (IX-COUNT-CURRENT-PERSON-QUARANTINE-BED-ALLOCATION-FAILURE-BB CURRENT-QUARANTINE-BED-ALLOCATION-FAILURE)
    (IX-COUNT-EVER-PERSON-TESTED-POSITIVE-BB EVER-TESTED-POSITIVE)
    (IX-COUNT-TURNED-ON-PERSON-TESTED-POSITIVE-BB TURNED-ON-TESTED-POSITIVE)
    (IX-COUNT-CURRENT-PERSON-TESTED-POSITIVE-BB CURRENT-TESTED-POSITIVE)
    (IX-COUNT-EVER-PERSON-CONTACT-TRACING-BB EVER-CONTACT-TRACING)
    (IX-COUNT-TURNED-ON-PERSON-CONTACT-TRACING-BB TURNED-ON-CONTACT-TRACING)
    (IX-COUNT-CURRENT-PERSON-CONTACT-TRACING-BB CURRENT-CONTACT-TRACING)
    (IX-COUNT-EVER-PERSON-RECENTLY-TESTED-BB EVER-RECENTLY-TESTED)
    (IX-COUNT-TURNED-ON-PERSON-RECENTLY-TESTED-BB TURNED-ON-RECENTLY-TESTED)
    (IX-COUNT-CURRENT-PERSON-RECENTLY-TESTED-BB CURRENT-RECENTLY-TESTED)
    (IX-COUNT-EVER-PERSON-IN-ISOLATION-BB EVER-IN-ISOLATION)
    (IX-COUNT-TURNED-ON-PERSON-IN-ISOLATION-BB TURNED-ON-IN-ISOLATION)
    (IX-COUNT-CURRENT-PERSON-IN-ISOLATION-BB CURRENT-IN-ISOLATION)
    (IX-COUNT-EVER-PERSON-IN-TAIWAN-ISOLATION-BB EVER-IN-TAIWAN-ISOLATION)
    (IX-COUNT-TURNED-ON-PERSON-IN-TAIWAN-ISOLATION-BB TURNED-ON-IN-TAIWAN-ISOLATION)
    (IX-COUNT-CURRENT-PERSON-IN-TAIWAN-ISOLATION-BB CURRENT-IN-TAIWAN-ISOLATION)
    (IX-COUNT-EVER-PERSON-DELIBERATELY-INFECTED-BB EVER-DELIBERATELY-INFECTED)
    (IX-COUNT-TURNED-ON-PERSON-DELIBERATELY-INFECTED-BB TURNED-ON-DELIBERATELY-INFECTED)
    (IX-COUNT-CURRENT-PERSON-DELIBERATELY-INFECTED-BB CURRENT-DELIBERATELY-INFECTED)
    (IX-COUNT-EVER-PERSON-IN-QUARANTINE-DELIBERATELY-INFECTED-BB EVER-IN-QUARANTINE-DELIBERATELY-INFECTED)
    (IX-COUNT-TURNED-ON-PERSON-IN-QUARANTINE-DELIBERATELY-INFECTED-BB TURNED-ON-IN-QUARANTINE-DELIBERATELY-INFECTED)
    (IX-COUNT-CURRENT-PERSON-IN-QUARANTINE-DELIBERATELY-INFECTED-BB CURRENT-IN-QUARANTINE-DELIBERATELY-INFECTED)
    (IX-COUNT-EVER-PERSON-IN-QUARANTINE-VULNERABLE-BB EVER-IN-QUARANTINE-VULNERABLE)
    (IX-COUNT-TURNED-ON-PERSON-IN-QUARANTINE-VULNERABLE-BB TURNED-ON-IN-QUARANTINE-VULNERABLE)
    (IX-COUNT-CURRENT-PERSON-IN-QUARANTINE-VULNERABLE-BB CURRENT-IN-QUARANTINE-VULNERABLE)
    (IX-COUNT-EVER-PERSON-EXOGENOUSLY-INFECTED-BB EVER-EXOGENOUSLY-INFECTED)
    (IX-COUNT-TURNED-ON-PERSON-EXOGENOUSLY-INFECTED-BB TURNED-ON-EXOGENOUSLY-INFECTED)
    (IX-COUNT-CURRENT-PERSON-EXOGENOUSLY-INFECTED-BB CURRENT-EXOGENOUSLY-INFECTED)
    (IX-COUNT-EVER-PERSON-HAS-BEEN-RECENTLY-INFECTED-BB EVER-HAS-BEEN-RECENTLY-INFECTED)
    (IX-COUNT-TURNED-ON-PERSON-HAS-BEEN-RECENTLY-INFECTED-BB TURNED-ON-HAS-BEEN-RECENTLY-INFECTED)
    (IX-COUNT-CURRENT-PERSON-HAS-BEEN-RECENTLY-INFECTED-BB CURRENT-HAS-BEEN-RECENTLY-INFECTED)
    (IX-COUNT-EVER-PERSON-HAS-BEEN-EVER-INFECTED-BB EVER-HAS-BEEN-EVER-INFECTED)
    (IX-COUNT-TURNED-ON-PERSON-HAS-BEEN-EVER-INFECTED-BB TURNED-ON-HAS-BEEN-EVER-INFECTED)
    (IX-COUNT-CURRENT-PERSON-HAS-BEEN-EVER-INFECTED-BB CURRENT-HAS-BEEN-EVER-INFECTED)
    (IX-COUNT-EVER-PERSON-ANTIGEN-TESTED-POSITIVE-BB EVER-ANTIGEN-TESTED-POSITIVE)
    (IX-COUNT-TURNED-ON-PERSON-ANTIGEN-TESTED-POSITIVE-BB TURNED-ON-ANTIGEN-TESTED-POSITIVE)
    (IX-COUNT-CURRENT-PERSON-ANTIGEN-TESTED-POSITIVE-BB CURRENT-ANTIGEN-TESTED-POSITIVE)
    (IX-COUNT-EVER-PERSON-ANTIGEN-TESTED-NEGATIVE-BB EVER-ANTIGEN-TESTED-NEGATIVE)
    (IX-COUNT-TURNED-ON-PERSON-ANTIGEN-TESTED-NEGATIVE-BB TURNED-ON-ANTIGEN-TESTED-NEGATIVE)
    (IX-COUNT-CURRENT-PERSON-ANTIGEN-TESTED-NEGATIVE-BB CURRENT-ANTIGEN-TESTED-NEGATIVE)
    (IX-COUNT-EVER-PERSON-RECUPERATING-BB EVER-RECUPERATING)
    (IX-COUNT-TURNED-ON-PERSON-RECUPERATING-BB TURNED-ON-RECUPERATING)
    (IX-COUNT-CURRENT-PERSON-RECUPERATING-BB CURRENT-RECUPERATING)
    (IX-COUNT-EVER-PERSON-RECENTLY-DECEASED-BB EVER-RECENTLY-DECEASED)
    (IX-COUNT-TURNED-ON-PERSON-RECENTLY-DECEASED-BB TURNED-ON-RECENTLY-DECEASED)
    (IX-COUNT-CURRENT-PERSON-RECENTLY-DECEASED-BB CURRENT-RECENTLY-DECEASED)
    (IX-COUNT-EVER-PERSON-STATE-NEVER-INFECTED EVER-NEVER-INFECTED)
    (IX-COUNT-TURNED-ON-PERSON-STATE-NEVER-INFECTED TURNED-ON-NEVER-INFECTED)
    (IX-COUNT-CURRENT-PERSON-STATE-NEVER-INFECTED CURRENT-NEVER-INFECTED)
    (IX-COUNT-EVER-PERSON-STATE-INFECTED EVER-INFECTED)
    (IX-COUNT-TURNED-ON-PERSON-STATE-INFECTED TURNED-ON-INFECTED)
    (IX-COUNT-CURRENT-PERSON-STATE-INFECTED CURRENT-INFECTED)
    (IX-COUNT-EVER-PERSON-STATE-SHEDDING EVER-SHEDDING)
    (IX-COUNT-TURNED-ON-PERSON-STATE-SHEDDING TURNED-ON-SHEDDING)
    (IX-COUNT-CURRENT-PERSON-STATE-SHEDDING CURRENT-SHEDDING)
    (IX-COUNT-EVER-PERSON-STATE-SYMPTOMATIC EVER-SYMPTOMATIC)
    (IX-COUNT-TURNED-ON-PERSON-STATE-SYMPTOMATIC TURNED-ON-SYMPTOMATIC)
    (IX-COUNT-CURRENT-PERSON-STATE-SYMPTOMATIC CURRENT-SYMPTOMATIC)
    (IX-COUNT-EVER-PERSON-STATE-ICU-NEEDED EVER-ICU-NEEDED)
    (IX-COUNT-TURNED-ON-PERSON-STATE-ICU-NEEDED TURNED-ON-ICU-NEEDED)
    (IX-COUNT-CURRENT-PERSON-STATE-ICU-NEEDED CURRENT-ICU-NEEDED)
    (IX-COUNT-EVER-PERSON-STATE-RECOVERED EVER-RECOVERED)
    (IX-COUNT-TURNED-ON-PERSON-STATE-RECOVERED TURNED-ON-RECOVERED)
    (IX-COUNT-CURRENT-PERSON-STATE-RECOVERED CURRENT-RECOVERED)
    (IX-COUNT-EVER-PERSON-STATE-DECEASED DECEASED)
    (IX-COUNT-TURNED-ON-PERSON-STATE-DECEASED TURNED-ON-DECEASED)
    (IX-COUNT-CURRENT-PERSON-STATE-DECEASED CURRENT-DECEASED)
    (IX-COUNT-EVER-PERSON-STATE-IMMUNIZED EVER-IMMUNIZED)
    (IX-COUNT-TURNED-ON-PERSON-STATE-IMMUNIZED TURNED-ON-IMMUNIZED)
    (IX-COUNT-CURRENT-PERSON-STATE-IMMUNIZED CURRENT-IMMUNIZED)
    (IX-COUNT-EVER-PERSON-STATE-STILL-SHEDDING EVER-STILL-SHEDDING)
    (IX-COUNT-TURNED-ON-PERSON-STATE-STILL-SHEDDING TURNED-ON-STILL-SHEDDING)
    (IX-COUNT-CURRENT-PERSON-STATE-STILL-SHEDDING CURRENT-STILL-SHEDDING)
    (IX-ACTIVE-COUNT ACTIVE-COUNT)
    (IX-ALIVE-COUNT ALIVE-COUNT)
    (IX-INFECTIOUS-COUNT INFECTIOUS-COUNT)
    (IX-HEDONIC-COST HEDONIC-COST)
    (IX-ECONOMIC-COST ECONOMIC-COST)
    (ix-qaly-lost-total-hedonic-cost qaly-lost-total-hedonic-cost)
    (ix-icu-needed-days-hedonic-cost icu-needed-days-hedonic-cost)
    (ix-mask-days-hedonic-cost mask-days-hedonic-cost)
    (ix-quarantine-effectiveness-days-hedonic-cost quarantine-effectiveness-days-hedonic-cost)
    (ix-recuperating-days-hedonic-cost recuperating-days-hedonic-cost)
    (ix-social-isolation-effectiveness-days-hedonic-cost social-isolation-effectiveness-days-hedonic-cost)
    (ix-social-isolation-a-effectiveness-days-hedonic-cost social-isolation-a-effectiveness-days-hedonic-cost)
    (ix-social-isolation-b-effectiveness-days-hedonic-cost social-isolation-b-effectiveness-days-hedonic-cost)
    (ix-social-isolation-c-effectiveness-days-hedonic-cost social-isolation-c-effectiveness-days-hedonic-cost)
    (ix-symptomatic-days-hedonic-cost symptomatic-days-hedonic-cost)
    (ix-taiwan-isolation-effectiveness-days-hedonic-cost taiwan-isolation-effectiveness-days-hedonic-cost)
    (ix-qaly-lost-total-economic-cost qaly-lost-total-economic-cost)
    (ix-icu-days-economic-cost icu-days-economic-cost)
    (ix-mask-days-economic-cost mask-days-economic-cost)
    (ix-quarantine-effectiveness-days-economic-cost quarantine-effectiveness-days-economic-cost)
    (ix-recuperating-days-economic-cost recuperating-days-economic-cost)
    (ix-social-isolation-effectiveness-days-economic-cost social-isolation-effectiveness-days-economic-cost)
    (ix-social-isolation-a-effectiveness-days-economic-cost social-isolation-a-effectiveness-days-economic-cost)
    (ix-social-isolation-b-effectiveness-days-economic-cost social-isolation-b-effectiveness-days-economic-cost)
    (ix-social-isolation-c-effectiveness-days-economic-cost social-isolation-c-effectiveness-days-economic-cost)
    (ix-symptomatic-days-economic-cost symptomatic-days-economic-cost)
    (ix-taiwan-isolation-effectiveness-days-economic-cost taiwan-isolation-effectiveness-days-economic-cost)
    (ix-population population)
    (IX-TOTAL-COST TOTAL-COST)
    (IX-LAST-DAYIX LAST-DAY-NBR)
    (IX-DAYIX-ADJUSTMENT-FACTOR SCALE-UP-DAY-RESULTS-FACTOR)
    (IX-RATE-ADJUSTMENT-FACTOR SCALE-UP-RATE-RESULTS-FACTOR)
    (IX-LIFE-YEARS-LOST LIFE-YEARS-LOST)
    (IX-QALY-LOST-TOTAL QALY-LOST-TOTAL)
    (IX-QALY-LOST-DEATH QALY-LOST-DEATH)
    (IX-QALY-LOST-DISABILITY QALY-LOST-DISABILITY)
    (IX-ICU-DAYS ICU-DAYS)
    (IX-ICU-NEEDED-DAYS ICU-NEEDED-DAYS)
    (IX-MASK-DAYS MASK-DAYS)
    (IX-QUARANTINE-EFFECTIVENESS-DAYS QUARANTINE-EFFECTIVENESS-DAYS)
    (IX-RECUPERATING-DAYS RECUPERATING-DAYS)
    (IX-SOCIAL-ISOLATION-EFFECTIVENESS-DAYS SOCIAL-ISOLATION-EFFECTIVENESS-DAYS)
    (IX-SOCIAL-ISOLATION-a-EFFECTIVENESS-DAYS SOCIAL-ISOLATION-a-EFFECTIVENESS-DAYS)
    (IX-SOCIAL-ISOLATION-b-EFFECTIVENESS-DAYS SOCIAL-ISOLATION-b-EFFECTIVENESS-DAYS)
    (IX-SOCIAL-ISOLATION-c-EFFECTIVENESS-DAYS SOCIAL-ISOLATION-c-EFFECTIVENESS-DAYS)
    (IX-SYMPTOMATIC-DAYS SYMPTOMATIC-DAYS)
    (IX-TAIWAN-ISOLATION-EFFECTIVENESS-DAYS TAIWAN-ISOLATION-EFFECTIVENESS-DAYS)
    )
  "string names of counters and friendly names")

(defparameter *counters-accumulators-sort-order*
  '(
    "ix-population"
    "ix-count-ever-person-state-deceased"
    "ix-active-count"
    "ix-alive-count"
    "ix-infectious-count"
    ;;
    "ix-life-years-lost"
    "ix-qaly-lost-total"
    "ix-qaly-lost-death"
    "ix-qaly-lost-disability"
    ;;
    "ix-total-cost"
    "ix-hedonic-cost"
    "ix-economic-cost"
    ;;
    "ix-qaly-lost-total-hedonic-cost"
    "ix-icu-needed-days-hedonic-cost"
    "ix-mask-days-hedonic-cost"
    "ix-quarantine-effectiveness-days-hedonic-cost"
    "ix-recuperating-days-hedonic-cost"
    "ix-social-isolation-effectiveness-days-hedonic-cost"
    "ix-social-isolation-a-effectiveness-days-hedonic-cost"
    "ix-social-isolation-b-effectiveness-days-hedonic-cost"
    "ix-social-isolation-c-effectiveness-days-hedonic-cost"
    "ix-symptomatic-days-hedonic-cost"
    "ix-taiwan-isolation-effectiveness-days-hedonic-cost"
    "ix-qaly-lost-total-economic-cost"
    "ix-icu-days-economic-cost"
    "ix-mask-days-economic-cost"
    "ix-quarantine-effectiveness-days-economic-cost"
    "ix-recuperating-days-economic-cost"
    "ix-social-isolation-effectiveness-days-economic-cost"
    "ix-social-isolation-a-effectiveness-days-economic-cost"
    "ix-social-isolation-b-effectiveness-days-economic-cost"
    "ix-social-isolation-c-effectiveness-days-economic-cost"
    "ix-symptomatic-days-economic-cost"
    "ix-taiwan-isolation-effectiveness-days-economic-cost"
    "ix-last-dayix"
    "ix-dayix-adjustment-factor"
    "ix-rate-adjustment-factor"
    "ix-icu-days"
    "ix-icu-needed-days"
    "ix-mask-days"
    "ix-quarantine-effectiveness-days"
    "ix-recuperating-days"
    "ix-social-isolation-effectiveness-days"
    "ix-social-isolation-a-effectiveness-days"
    "ix-social-isolation-b-effectiveness-days"
    "ix-social-isolation-c-effectiveness-days"
    "ix-symptomatic-days"
    "ix-taiwan-isolation-effectiveness-days"
    ;;
    "ix-count-current-person-state-never-infected"
    "ix-count-ever-person-deliberately-infected-bb"
    "ix-count-ever-person-exogenously-infected-bb"
    "ix-count-ever-person-state-infected"
    ;;
    "ix-count-ever-person-icu-bed-allocated-bb"
    "ix-count-turned-on-person-icu-bed-allocated-bb"
    "ix-count-current-person-icu-bed-allocated-bb"
    "ix-count-ever-person-icu-bed-allocation-failure-bb"
    "ix-count-turned-on-person-icu-bed-allocation-failure-bb"
    "ix-count-current-person-icu-bed-allocation-failure-bb"
    "ix-count-ever-person-in-quarantine-diagnosed-bb"
    "ix-count-turned-on-person-in-quarantine-diagnosed-bb"
    "ix-count-current-person-in-quarantine-diagnosed-bb"
    "ix-count-ever-person-quarantine-bed-allocation-failure-bb"
    "ix-count-turned-on-person-quarantine-bed-allocation-failure-bb"
    "ix-count-current-person-quarantine-bed-allocation-failure-bb"
    "ix-count-ever-person-tested-positive-bb"
    "ix-count-turned-on-person-tested-positive-bb"
    "ix-count-current-person-tested-positive-bb"
    "ix-count-ever-person-contact-tracing-bb"
    "ix-count-turned-on-person-contact-tracing-bb"
    "ix-count-current-person-contact-tracing-bb"
    "ix-count-ever-person-recently-tested-bb"
    "ix-count-turned-on-person-recently-tested-bb"
    "ix-count-current-person-recently-tested-bb"
    "ix-count-ever-person-in-isolation-bb"
    "ix-count-turned-on-person-in-isolation-bb"
    "ix-count-current-person-in-isolation-bb"
    "ix-count-ever-person-in-taiwan-isolation-bb"
    "ix-count-turned-on-person-in-taiwan-isolation-bb"
    "ix-count-current-person-in-taiwan-isolation-bb"
    "ix-count-turned-on-person-deliberately-infected-bb"
    "ix-count-current-person-deliberately-infected-bb"
    "ix-count-ever-person-in-quarantine-deliberately-infected-bb"
    "ix-count-turned-on-person-in-quarantine-deliberately-infected-bb"
    "ix-count-current-person-in-quarantine-deliberately-infected-bb"
    "ix-count-ever-person-in-quarantine-vulnerable-bb"
    "ix-count-turned-on-person-in-quarantine-vulnerable-bb"
    "ix-count-current-person-in-quarantine-vulnerable-bb"
    "ix-count-ever-person-exogenously-infected-bb"
    "ix-count-turned-on-person-exogenously-infected-bb"
    "ix-count-current-person-exogenously-infected-bb"
    "ix-count-ever-person-has-been-recently-infected-bb"
    "ix-count-turned-on-person-has-been-recently-infected-bb"
    "ix-count-current-person-has-been-recently-infected-bb"
    "ix-count-ever-person-has-been-ever-infected-bb"
    "ix-count-turned-on-person-has-been-ever-infected-bb"
    "ix-count-current-person-has-been-ever-infected-bb"
    "ix-count-ever-person-antigen-tested-positive-bb"
    "ix-count-turned-on-person-antigen-tested-positive-bb"
    "ix-count-current-person-antigen-tested-positive-bb"
    "ix-count-ever-person-antigen-tested-negative-bb"
    "ix-count-turned-on-person-antigen-tested-negative-bb"
    "ix-count-current-person-antigen-tested-negative-bb"
    "ix-count-ever-person-recuperating-bb"
    "ix-count-turned-on-person-recuperating-bb"
    "ix-count-current-person-recuperating-bb"
    "ix-count-ever-person-recently-deceased-bb"
    "ix-count-turned-on-person-recently-deceased-bb"
    "ix-count-current-person-recently-deceased-bb"
    "ix-count-ever-person-state-never-infected"
    "ix-count-turned-on-person-state-never-infected"
    "ix-count-current-person-state-never-infected"
    "ix-count-ever-person-state-infected"
    "ix-count-turned-on-person-state-infected"
    "ix-count-current-person-state-infected"
    "ix-count-ever-person-state-shedding"
    "ix-count-turned-on-person-state-shedding"
    "ix-count-current-person-state-shedding"
    "ix-count-ever-person-state-symptomatic"
    "ix-count-turned-on-person-state-symptomatic"
    "ix-count-current-person-state-symptomatic"
    "ix-count-ever-person-state-icu-needed"
    "ix-count-turned-on-person-state-icu-needed"
    "ix-count-current-person-state-icu-needed"
    "ix-count-ever-person-state-recovered"
    "ix-count-turned-on-person-state-recovered"
    "ix-count-current-person-state-recovered"
    "ix-count-ever-person-state-deceased"
    "ix-count-turned-on-person-state-deceased"
    "ix-count-current-person-state-deceased"
    "ix-count-ever-person-state-immunized"
    "ix-count-turned-on-person-state-immunized"
    "ix-count-current-person-state-immunized"
    "ix-count-ever-person-state-still-shedding"
    "ix-count-turned-on-person-state-still-shedding"
    "ix-count-current-person-state-still-shedding"
    )
  "string names of counters in sort order")
