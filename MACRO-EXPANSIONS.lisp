;; *-* lisp *-

(format t "Load the main file and then this file to expand the data and struct defining macros.~%This is to help the poor hapless programmer.~%")

(format t "Note also~%mx is pretty print of macro-expand-1~%mxx is pretty print of macroexpand~%")

(format t "defconstant+ is a more friendly defconstant (does not complain of trivial changes)~%")

(format t "~%~%Expansion of (make-parms-struct) ~%~%~S~%" (macroexpand-1 '(make-parms-struct)))

(format t "~%~%Expansion of (using-parms +body+)~%~%~S~%" (macroexpand-1 '(using-parms +body+)))

(format t "~%~%Expansion of (make-getters-ht)~%~%~S~%" (macroexpand-1 '(make-getters-ht)))

(format t "~%~%Expansion of (create-enum-variables +person-state-labels+ person-state-max person-state-count)~%~%~S~%" (macroexpand-1 '(create-enum-variables +person-state-labels+ person-state-max person-state-count)))

(format t "~%~%Expansion of (create-enum-names +person-state-names+ +person-state-labels+)~%~%~S~%" (macroexpand-1 '(create-enum-names +person-state-names+ +person-state-labels+)))

(format t "~%~%Expansion of (create-enum-variables +event-labels+ person-state-max person-state-count)~%~%~S~%" (macroexpand-1 '(create-enum-variables +event-labels+ person-state-max person-state-count)))

(format t "~%~%Expansion of (build-flag-indexes)~%~%~S~%" (macroexpand-1 '(build-flag-indexes)))

(format t "~%~%Expansion of (person-define-nonflag-variables)~%~%~S~%" (macroexpand-1 '(person-define-nonflag-variables)))

(format t "~%~%Expansion of (person-define-flag-operations)~%~%~S~%" (macroexpand-1 '(person-define-flag-operations)))

(format t "~%~%Expansion of +counter-and-accumulator-names-vec+ (contents)~%~%~S~% " +counter-and-accumulator-names-vec+) ;; not a macro but w/e

(format t "~%~%Expansion of (person-define-accessors)~%~%~S~%" (macroexpand-1 '(person-define-accessors)))

(format t "~%~%Expansion of (person-define-flag-operations)~%~%~S~%" (macroexpand-1 '(person-define-flag-operations)))

(format t "~%~%Expansion of (display-personm pix)~%~%~S~%" (macroexpand-1 '(display-personm pix)))

(format t "~%~%Expansion of +quarantine-counts-labels+ (contents)~%~%~S~% " +quarantine-counts-labels+) ;; also not a macro

(format t "~%~%Expansion of +all-normal-quarantines+ (contents)~%~%~S~% " +all-normal-quarantines+) ;; also not a macro

(format t "~%~%Expansion of +quarantine-types-to-occupying-count-ixs+ (contents)~%~%~S~% " +quarantine-types-to-occupying-count-ixs+) ;; also not a macro

(format t "~%~%Expansion of +quarantine-types-to-missing-out-count-ixs+ (contents)~%~%~S~% " +quarantine-types-to-missing-out-count-ixs+) ;; also not a macro

(format t "~%~%Expansion of (build-counter-and-accumulator-indexes)~%~%~S~%" (macroexpand-1 '(build-counter-and-accumulator-indexes)))

(format t "~%~%Expansion of +states-to-counters-ever+ (contents)~%~%~S~%" +states-to-counters-ever+) ;; also not a macro

(format t "~%~%Expansion of +states-to-counters-turned-on+ (contents)~%~%~S~%" +states-to-counters-turned-on+) ;; also not a macro

(format t "~%~%Expansion of +states-to-counters-current+ (contents)~%~%~S~%" +states-to-counters-current+) ;; also not a macro

(format t "~%~%Expansion of (person-normal-arrays-initm)~%~%~S~%" (macroexpand-1 '(person-normal-arrays-initm)))
