;; *-* lisp *-

;; To do list
;; 67. (Space and number for the next one)

;; Maybe to do later
;; 48. incorporate geography (density) family/household, events, work, social, travel, extended, shopping, cafes/restuarants - customer facing people

;; Done - functional
;; 1. adjust death rates by segment
;; 2. adjust death rates when icu capacity exceeded
;;    (rh has two formulas i have one which is that if you don't fit in an icu you have (parms-death-rate-multiple-needs-icu-no-icu-bed *parms*) times the risk of dying)
;;    in my model you get an icu bed or not.
;; 16. quarantine cases tj
;; - put in quarantine when test +ve if spot available
;; - if in quarantine infectivity is less
;; - initially use :simple-biased
;; 19. possibility of complete suppression to zero cases tj (implied as all runs stop with no active cases)
;; 17. case and contact tracing effectiveness, (combined with isolate/quarantine cases) tj
;; 26. add concise modes and report parameters on that one line of the very concise mode, with duration, no space quarantine/test
;; 27. contact tracing effectiveness above 80% tj
;; 28. test all contacts tj
;; 28b. test all contacts even before symptoms appear tj
;; 29. test x% of those with symptoms tj, isolating +ves requires adjusting testing capacity
;; 18. social / physical isolation policies and spontaneous actions by citizens tj
;; 24. smart semi-isolation taiwan style - if symptoms, isolate (cannot work, shop, school, transport) tj- clean load and runs
;; 37. extra parameter test-all-contacts
;; 34. not everyone has synptoms - state diagram
;; 35. loss of life years calculation
;; 38. add deliberate infection to select groups rh1
;; 4. add quarantine for deliberately infected. 
;; 7b. split quarantine between deliberately infected and other groups.
;; 6. add permanent quarantine to select other groups.
;; 10. 2 levels of quarantine rh+ (isolation + quarantine)
;; 9. 3 groups young/old/middle rh+
;; 8. add variolation with various degrees of improvements in outcomes rh+
;; 12. short lived immunity of survivors tj
;; 13. impact of only a fraction of people becoming immune post infection/variolation/vaccine tj subsumed by 12
;; 5. test various levels of effectiveness of quarantine tj
;; not do at this point
;; 7a. maybe prioritize different groups for quarantine rh1 - too hard, though possible
;; 11. quarantine budget the dates being moveabale rh+.
;;     so the following can change over time
;;     - deliberate infection rate (adjustable)
;;     - icu-beds-per-capita
;;     - quarantine-diagnosed-beds-per-capita
;;     - quarantine-deliberately-infected-beds-per-capita
;;     - testing-capacity-per-capita (adjustable)
;;     - exogenously-reintroduce-infection-rate (adjustable)
;;     - social-isolation-ineffectiveness
;;     - taiwan-isolation-ineffectiveness
;;     - immunization-rate (new) (add with new capability see 44) (adjustable)
;;     form: listof (start-day value), or just a number
;;     start-day is adjustable by maybe-adjust-parms-for-size
;; 44. add immunization rate capability
;; 41. remove from quarantine if died, recovered, immunized.
;; 47. remove from isolation if died, recovered, immunized. (just a flag) - automatically times out
;; 43. kick out of quarantine if overloaded. need to perhaps revert to isolation if kicked out of quarantine
;; 42. add people to quarantine if slots available, may need to remove from isolation.
;; 14. impact of qaly impact on survivors i.e. permanent disability and deceased tj
;; 23. impact of antigen testing. save isolation/quarantine resources. tj
;; 21. test idea of masks = asymmetric reduction in infectivity
;;     https://www.lesswrong.com/posts/ykyg6d7hnxlujdcls/hammer-and-mask-wide-spread-use-of-reusable-particle
;; 32. distribtions of r0 not normal distribution see paper "individual variation in susceptibility or exposure to sars-cov-2 lowers the herd immunity threshold"
;;     (ql:quickload "cl-randist")
;;     (randist::random-gamma gamma=1/16 scale=1.0) => cv ~= 4
;;     number of recipients
;;     whether a recipient gets picked
;; 50. Add cost metrics hedonic and economic costs
;; 56. Selective reporting of fields. Coded and clean load.
;; 51. Shared quarantine facilities with priority
;; 57. Scaling factor for days on output also numbers but not pro-rata
;; 64. Better way to override paramaters. Ensure defaults good
;; 62. Prefix for heading and summary
;; 63. Optional Scaling for costs and last-dayix
;; 46. Documentation. make code easy to read, pseudocodable, document variables structs fields and function, overview.
;; 15. Vaccine becomes available in x months/years with semi-sup1pression until then tj - not a functional requirement, just set up a test
;; 65. Rerun delierately infected tests since bug fix

;; Done - technical
;; 0. quantize numbers of people to round integers (implicit)
;; 20. stop when no cases active
;; 3  test 327m people - done - 6 hours
;; 3.1 make structs smaller to support large numbers of people
;; 22. rewrite with bitmaps etc now 3.5 hours for 327m people.
;; 23. day/event array
;; 17b. clean up initialization
;; 30. specify run for minimum # days
;; 25. make parameters changeable
;; 33. straighten out options and parameters so they are consistent and simpler.
;; 36. add those temp stats into the standard report
;; 39. refactor calling parameter set
;; 40. parms to specify days in terms of expected duration for population 327m then adjust to formula 10.13 ln(pop-size) + 43.13
;; 45. fix rates and days so that arrays can have either. merge arrays and non-arrays. everything modifiable has other variable.
;; 7. (fixed) If a person is in vulnerable quarantine and they get sick, they still stay in quarantine when they get well unless kicked out by overload (no longer true)
;; 55. Fix person counts etc repetititive inefficient  code. Coded and clean load.
;; 58. Tuning, use of defconstant and inlining
;; 59. Checking for capacity should also check in bulk and also check for latent demand from higher priority quarantines
;; 60. Rebalance at end and start of day only
;; 61  Set off protective options during initial calculations to ensure they are correct i.e. raw daily r0 etc
;; 66. Fix up calculation of days infectious and probability adjustment factors for state transitions.

;; Not do
;; 59. Assumes infectivity is uniform while infectious - maybe change? But no data.
