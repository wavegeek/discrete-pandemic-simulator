;; *-* lisp *-

;; Copyright (C) 2020 by Tim Josling. See BSD.txt for terms.

;; See README.txt

;; see MACRO-EXPANSIONS.lisp for what the macro-expansions look like. 

;;;ABBREVIATIONS + DEFINITIONS
;; hop head of population
;; quarantine - strict isolation under supervision. limited supply of beds (3 pools of these or shared).
;; isolation - less strict / semi-voluntary. nay be sharing house with family. no limit.
;; fr - fraction
;; pct - percent
;; fun - function
;; qaly - quality adjusted life year

;;;DEBUGGING CODE - SBCL SPECIFIC

(if (find :sbcl cl:*features*)
    (progn
      (defun verbose-slow-mem (&key (verbose nil))
        (if verbose
            (sb-vm:memory-usage)
            (sb-vm:memory-usage :print-spaces t :count-spaces t :print-summary nil :cutoff 0.052f0)) ;; see room.lisp in sbcl src
        (let* ((ms (dynamic-space-size))
               (k 1024.0))
          (format t "~%dynamic allowed ~:d bytes ~:d gib~%" ms (/ ms (expt k 3))))
        (format t "~%gc time ~f secs~%" (/ *gc-run-time* 1d0 internal-time-units-per-second)))
      
      (defun mem (&key (verbose nil)) ;; :default t
        (room verbose)
        (let* ((ms (dynamic-space-size))
               (k 1024.0))
          (format t "~%dynamic allowed ~:d bytes ~:d gib~%" ms (/ ms (expt k 3))))
        (format t "~%gc time ~f secs~%" (/ *gc-run-time* 1d0 internal-time-units-per-second)))
      
      (if *speed-above-all* (proclaim '(SB-EXT:MAYBE-INLINE)))  ;; sbcl specific 
      ))
;;;DEBUGGING AND SPEED OPTIONS

(defmacro inline+ (fun-name)
  (if *speed-above-all* `(declaim (inline ,fun-name))))

(defvar *optimization-options* nil) ;; allow prior override
(proclaim *optimization-options*)

(defvar *speed-above-all* nil) ;; allow prior override

;;;GENERIC UTILITIES

(defun csv (list)
  "print list of lists as csv-ish lines"
  (loop for ll in list do
       (format t "~{~a,~}~%" ll)))

(inline+ vectorize)

(defun vectorize (something)
  "turn a list something into a vector, or leave alone if already a vector"
  (assert something)
  (coerce something 'vector))

(inline+ listify)

(defun listify (something)
  (assert something)
  (coerce something 'list))

(defun intify (x)
  "integer part of a real number x"
  (let ((result (truncate x 1)))
    result))

(inline+ nonzerop)

(defun nonzerop (x)
  (not (zerop x)))

(defun list2ht (list test)
    (let ((ht (make-hash-table :test test)))
      (loop for el in list do (setf (gethash (first el) ht) (second el)))
      ht))

(defun ht2list (ht)
  "return a list based on hash table ht, elements in the form (key data)"
  (loop for key being the hash-key using (hash-value data) of ht collect (list key data)))

(inline+ fill-vec)

(defun fill-vec (vec val) (loop for ix from 0 by 1 until (>= ix (length vec)) do (setf (aref vec ix) val)))

(inline+ zero-vec)

(defun zero-vec (vec) (fill-vec vec 0))
(inline+ real-to-integer-probabilistically)

(defun get-array-entry-or-scalar (x n)
  (if (vectorp x)
      (aref x n)
      x))

(inline+ get-first-or-scalar)

(defun get-first-or-scalar (x)
  (get-array-entry-or-scalar x 0))

(defun group-n (n source)
  (if (zerop n) (error "zero length"))
  (if (not (integerp (/ (length source) n))) (error "length(source) not multiple of n"))
  (loop for left = source then (nthcdr n left) until (not left)
       collect (subseq left 0 n)))

(defun note (&rest x)
  x)

(defmacro mx (expr) ;; pretty print macro expansion
  `(pprint (macroexpand-1 ',expr)))

(defmacro mxx (expr) ;; pretty print macro expansion
  `(pprint (macroexpand ',expr)))

(defmacro defconstant+ (name value &optional doc)
  "defconstant but allows you to redefine as same complex value w/o spurious error message"
  (let* ((value+ (gensym)))
    `(let ((,value+ ,value))
       (defconstant ,name
         (if (boundp ',name)
             (if (not (equalp (symbol-value ',name) ,value+))
                 (progn
                   (warn "failed attempt to change constant ~S ~S->~S, ignored" ',name (symbol-value ',name) ,value+)
                   (symbol-value ',name))
                 (symbol-value ',name))
             ,value+) ,doc))))

;; Concise doubly linked lists
;; How many times have I coded up linked lists?

(defstruct multiple-dll-eps add pop del meta-iter dbg id)

(defun mmd-add (id first-ixs nexts prevs checks item-ix0 list-nbr)
  id
  ;;(format t "..dbg adding id1 ~S item-ix0 ~S list-nbr ~S~%" id1 item-ix0 list-nbr)
  (assert (and (not (minusp list-nbr)) (<= list-nbr (length first-ixs))))
  (let ((item-ix (1+ item-ix0))
        (old-first (aref first-ixs list-nbr)))
    (assert (zerop (aref checks item-ix)))
    (setf (aref first-ixs list-nbr) item-ix)
    (setf (aref nexts item-ix) old-first)
    (if (not (zerop old-first))
        (setf (aref prevs old-first) item-ix))
    (setf (aref checks item-ix) list-nbr)
    (list :OK item-ix0)))

(defun mmd-pop (id first-ixs nexts prevs checks list-nbr)
  id
  (assert (and (not (minusp list-nbr)) (<= list-nbr (length first-ixs))))
  (let* ((old-first+ (aref first-ixs list-nbr))
         (old-first (- old-first+ 1)))
    ;;(format t "..dbg popping id1 ~S old-first ~S list-nbr ~S~%" id1 old-first list-nbr)
    (if (zerop old-first+)
        nil
        (let ((result old-first)
              (new-first+ (aref nexts old-first+)))
          (if (not (zerop new-first+))
              (setf (aref prevs new-first+) 0))
          (setf  (aref nexts old-first+) 0)
          (setf  (aref checks old-first+) 0)
          (setf  (aref first-ixs list-nbr) new-first+)
          result))))

(defun mmd-del (id first-ixs nexts prevs checks item-ix0 list-nbr)
  id
  ;;(format t "..dbg removing id1 ~S item-ix0 ~S list-nbr ~S~%" id1 item-ix0 list-nbr)
  (assert (and (not (minusp list-nbr)) (<= list-nbr (length first-ixs))))
  (let ((item-ix (1+ item-ix0))
        (old-first (aref first-ixs list-nbr)))
    old-first
    (assert (eql (aref checks item-ix) list-nbr))
    (if (not (zerop (aref nexts item-ix)))
        (setf (aref prevs (aref nexts item-ix)) (aref prevs item-ix)))
    (if (not (zerop (aref prevs item-ix)))
        (setf (aref nexts (aref prevs item-ix)) (aref nexts item-ix))
        (setf (aref first-ixs list-nbr) (aref nexts item-ix)))
    (setf (aref nexts item-ix) 0)
    (setf (aref prevs item-ix) 0)
    (setf (aref checks item-ix) 0)
    (list :OK item-ix0)))

(defun mmd-meta-iter (id first-ixs nexts prevs checks list-nbr)
  id prevs
  (assert (and (not (minusp list-nbr)) (<= list-nbr (length first-ixs))))
  (let ((next-ix+ (aref first-ixs list-nbr))
        (save-list-nbr list-nbr))
    (lambda ()
      (block fun1
        (if (zerop next-ix+)
            (return-from fun1 nil)
            (let ((next-ix (- next-ix+ 1)))
              (assert (eql (aref checks next-ix+) save-list-nbr))
              (setf next-ix+ (aref nexts next-ix+))
              next-ix))))))

(defun make-multiple-dll (size number-of-lists id)
  "simple system to concisely store multiple doubly linked lists."
  (assert (plusp number-of-lists))
  (assert (plusp size))
  (let* ((id1 id)
         (struct nil)
         (type  (list 'integer 0 size)) ;; 0 = nil in these lists, so we add one to the customer's ixs
         (first-ixs (make-array number-of-lists :element-type type :initial-element 0))
         (nexts (make-array (1+ size) :element-type type :initial-element 0))
         (prevs (make-array (1+ size) :element-type type :initial-element 0))
         (checks (make-array (1+ size) :element-type (list 'integer 0 number-of-lists) :initial-element 0))
         (add-fun (lambda (item-ix0 list-nbr) (mmd-add id first-ixs nexts prevs checks item-ix0 list-nbr)))
         (pop-fun (lambda (list-nbr) (mmd-pop id first-ixs nexts prevs checks list-nbr)))
         (del-fun (lambda (item-ix0 list-nbr) (mmd-del id first-ixs nexts prevs checks item-ix0 list-nbr)))
         (meta-iter-fun (lambda (list-nbr) (mmd-meta-iter id first-ixs nexts prevs checks list-nbr))) ;; iterator not safe to changes underneath it
         (dbg-fun (lambda () (list :OK :first-ixs first-ixs :nexts nexts :prevs prevs :checks checks :struct struct))))
    id1
    (setf struct (make-multiple-dll-eps :add add-fun :del del-fun :pop pop-fun :meta-iter meta-iter-fun :dbg dbg-fun :id id))))


;; keep for testing
(if nil
(defun test-make-mutiple-dll ()
  (let ((mleps  (make-multiple-dll 10 3 :test)))
    (loop for ix from 0 by 1 until (>= ix 3) do (format t "add ~S~%" (funcall (multiple-dll-eps-add mleps) ix 0)))
    (loop for ix from 3 by 1 until (>= ix 6) do (format t "add ~S~%" (funcall (multiple-dll-eps-add mleps) ix 1)))
    (loop for ix from 6 by 1 until (>= ix 10) do (format t "add ~S~%" (funcall (multiple-dll-eps-add mleps) ix 2)))
    ;;(loop for ix from 6 by 1 until (>= ix 10) do (format t "del ~S~%" (funcall (multiple-dll-eps-del mleps) ix 2)))
    ;;(loop for ix from 3 by 1 until (>= ix 6) do (format t "del ~S~%" (funcall (multiple-dll-eps-del mleps) ix 1)))
    ;;(loop for ix from 0 by 1 until (>= ix 3) do (format t "del ~S~%" (funcall (multiple-dll-eps-del mleps) ix 0)))
    (loop for li from 0 by 1 until (>= li 3) do
         (block done1
           (let ((iter-fun (funcall (multiple-dll-eps-meta-iter mleps) li)))
             (loop do
                  (let ((res (funcall iter-fun)))
                    (format t "list ~S iter ~S~%" li res)
                    (if (not res) (return-from done1)))))))
    (loop for li from 0 by 1 until (>= li 3) do
         (block done1
           (loop do
                (let ((res (funcall (multiple-dll-eps-pop mleps) li)))
                  (format t "list ~S popped ~S~%" li res)
                  (if (not res) (return-from done1))))))
    (loop for li from 0 by 1 until (>= li 3) do
         (block done1
           (let ((iter-fun (funcall (multiple-dll-eps-meta-iter mleps) li)))
             (loop do
                  (let ((res (funcall iter-fun)))
                    (format t "list ~S iter ~S~%" li res)
                    (if (not res) (return-from done1)))))))
    (funcall (multiple-dll-eps-dbg mleps)))))


(defun merge-symbols (&rest symbols)
  (let ((str (mapcar #'symbol-name symbols)))
    (intern (apply 'concatenate 'string str))))

(defun create-enum-name (name)
  (intern (concatenate 'string "+" (symbol-name name) "+")))

(defun create-enum-variable (name nbr)
  `(defconstant+
       ,(create-enum-name name)
     ,nbr))

(defun create-enum-variables-fun (list max-name count-name)
  (let ((count (length list)))
    (cons 'progn
          (append
           (loop for el in list
              for ix from 0 by 1 collect
                (create-enum-variable el ix))
           (list 
            (create-enum-variable max-name (- count 1))
            (create-enum-variable count-name count))))))

(defmacro create-enum-variables (list max-name count-name)
  (create-enum-variables-fun (symbol-value list) max-name count-name))

(defmacro create-enum-names (varname labels)
  (list 'defconstant+ varname
        (list 'vectorize (cons 'list (mapcar #'symbol-name (symbol-value labels))))))

(defun remove-dups (list test)
  (let ((ht (make-hash-table :test test))
        (acc nil))
    (loop for el in list do
         (let ((prev (gethash el ht)))
           (if (not prev)
               (progn
                 (setf (gethash el ht) t)
                 (push el acc)))))
    (reverse acc)))

;;;APP-SPECIFIC utilities

(defun error-later () (error "should be replaced later in file; inserted due to recursion"))

(defparameter *q+d-random-0-1* :not-inited-q+d-random-0-1 "lookup for q&d random fun")

(inline+ choose-with-p-q+d)

(defun choose-with-p-q+d (p)
  (< (funcall *q+d-random-0-1*) p))

(inline+ choose-with-p-q+d)

(defun choose-with-p-slow (p)
  (< (random 1.0) p))

(defun make-q+d-random-0-1 (&key (size 1000)) ;; for when calls to rancom are too slow suggest power of 10 or any other number relatively prime to 79
  (let ((arr (make-array size))
        (inc 79)
        (next 0))
    (loop for ix from 0 by 1 until (>= ix size) do (setf (aref arr ix) (random 1.0)))
    (lambda ()
      (setf next (mod (+ next inc) size))
      (values (aref arr next) arr))))

(defun real-to-integer-probabilistically (r0)
  (let* ((r0-trunc (truncate r0))
         (rem (- r0 r0-trunc))
         (make-up-your-mind (if (choose-with-p-q+d rem) 1 0)) ;; for fraction convert to 0/1 probabilitstically depending on the fraction
         (nbr-to-infect (+ r0-trunc make-up-your-mind)))
    nbr-to-infect))

(defconstant+ +scan-increments+ '(18089 18097 18119 18121 18127 18131 18133 18143 18149 18169 18181)) ;; primes-larger-sqrt-usa-population

#| ;; keep for constants calculations.
(defun is-prime (n)
  (if (< n 2)
      (return-from is-prime nil))
  (if (and (> n 2) (integerp (/ n 2)))
      (return-from is-prime nil))
  (let ((nsqrt (intify (sqrt n))))
    (loop for i from 3 by 2 until (> i nsqrt) do
         (if (integerp (/ n i))
             (return-from is-prime nil))))
  t)

(defun prime-larger-than (n)
  (loop for i from (1+ n) by 1 do
       (if (is-prime i)
           (return-from prime-larger-than i))))

(defun primes-larger-sqrt-usa-population (&key (us-pop 327000000) (nbr-to-get 10))
  (let ((ct 0)
        (acc nil))
    (loop for i from (intify (sqrt us-pop)) by 1 until (> ct nbr-to-get) do
         (let ((pli (prime-larger-than i)))
           (if (not (find pli acc))
               (progn
                 (push pli acc)
                 (incf ct)))))
    (reverse acc)))
|#

(defun always-true () t)

(defun symbol-with-suffix (symbol suffix)
  (intern (string-upcase (concatenate 'string (symbol-name symbol) suffix))))

(defun symbol-with-prefix (symbol prefix)
  (intern (string-upcase (concatenate 'string prefix (symbol-name symbol)))))

;; global variable used everywhere = today

(defparameter *dayix* :not-init-dayix) ;; current day number

;;;ADVANCE DEFINITIONS FOR RECURSION forward references

(defun maybe-test-person (person-ix &optional (test-validity-override nil)) person-ix test-validity-override (error-later))

(defun  process-event (event) event (error-later))

(defun compute-days-infectious () (error-later))

(defun compute-fatality-rate-adjusters () (error-later))

(defun contact-tracing-on () (error "dummy should not be called - defined later"))

(defun change-of-flag (flag-ix person-ix flag-on-p) flag-ix person-ix flag-on-p (error-later))

;;(defun rebalance-quarantines-by-priority () (error-later))
  
;;;PARAMETERS AND VALIDATION (and adjustment for scale)

(defun valid-r0-distribution (r0-distribution) (or (not r0-distribution) (eq r0-distribution :gamma-distribution)))

(defun is-binary (x) (or (eq x nil) (eq x t)))

(defun is-float-0-to-1 (x) (and (numberp x) (realp x) (floatp x) (>= x 0.0) (<= x 1.0)))

(defun is-float-minus-1-to-0 (x) (and (numberp x) (realp x) (floatp x) (>= x -1.0) (<= x 0.0)))

(defun is-float-plusp-less-than-10 (x) (and (numberp x) (realp x) (floatp x) (plusp x) (< x 10.0)))

(defun not-minusp (x) (not (minusp x)))

(defun valid-testing-regime (testing-regime) (or (not testing-regime) (eq testing-regime :simple-biased) (eq testing-regime :contact-tracing)))
  

(defun fa (x) (format nil "~a" x)) ;; format standard

(defun fix-commas (str)
  (let ((fstr (make-array '(0) :element-type 'base-char :fill-pointer 0 :adjustable t)))
    (with-output-to-string (s fstr)
      (loop for c across str do (format s "~A" (if (eq #\, c) "(comma)" c))))
    fstr))

(defun fac (x)
  (let ((wo-commas (fix-commas x)))
    (format nil "~a" wo-commas)))

(defun flm (x &optional (filler-string "-")) (if (and x (listp x)) ;; format maybe list with '-' separators
                   (let ((str1 (format nil (concatenate 'string "~{~a" filler-string "~}") x)))
                     (subseq str1 0 (- (length str1) 1)))
                   (format nil "~a" x)))

(defparameter *terse-flags-allowed*
  '(:summary :running-stats :schedule-too-late :initial-infection-failure :initial-immunization-failure
    :quarantine-vulnerable-failure :computing-adjusted-fatality-rates :daily-all-stats :group-stats :report-why-end
    :easy-to-read-output :probability-warnings :nondefault-parms-detailed :adjuster-calculation-results))

(defun valid-terseness (x)
  (let ((failp nil))
    (if (listp x)
        (loop for el in x do
             (if (not 
                  (find el *terse-flags-allowed*))
                 (setf failp t)))
        (setf failp t))
    (values (not failp) :var x :allowed *terse-flags-allowed*)))


(defconstant+ +full-quarantine-details+ `((:deliberately-infected quarantine-deliberately-infected :not-global :fallback-isolation)
                                          (:vulnerable quarantine-vulnerable :not-global :not-fallback-isolation)
                                          (:diagnosed quarantine-diagnosed :not-global :not-fallback-isolation)
                                          ((gensym) quarantine-global :global :not-fallback-isolation)))

(defconstant+ +valid-quarantine-priorities+ (remove-if-not #'keywordp (mapcar #'first +full-quarantine-details+)))

(defconstant+ +basic-quarantine-categories+ (mapcar (lambda (keyname) (intern (symbol-name keyname))) +valid-quarantine-priorities+))

(defconstant+ +basic-quarantine-categories-lists+ (mapcar #'list +basic-quarantine-categories+))

(defconstant+ +quarantine-details+ (mapcar #'rest +full-quarantine-details+))

(defun valid-quarantine-priority-list (x)
  (let ((failp nil))
    (if (listp x)
        (loop for el in x do
             (if (not 
                  (find el +valid-quarantine-priorities+))
                 (setf failp t)))
        (setf failp t))
    (values (not failp) :var x :allowed +valid-quarantine-priorities+)))

(defparameter *parms* :not-init-parms) ;; global parameters see parms above

;; list of defstruct field parameters, then formatting function then setter-fn-to-store-by-day-array adjustable-rate-flag then adjustable-days-flag then doc"
(defconstant+ +fields-in-parms+
    '(
      ;;
      ((base-r0 2.4) fa nil nil nil "|<h1>Global Constants and Parameters</h1><p>Note on time-varying parameters</h1><p>These are the ones whose names end in -fv. They can be specified as a constant number, or as a list of pairs of starting day number and value from that day number e.g. 0.1 which spefifies always 0.1, or '((0 0.01) (100 0.02) (200 0)) which specifies 0.01 from day 0, 0.02 from day 100, 0.02 from day 200.|<p>Number of people infected by a case in a non-immune population without isolation measures. much debated. for r0 see epidemiology 101")
      ((min-days-to-run-base 1) fa *min-days-to-run-adj* nil :days "Run at least this number of days. Useful when only exogenous infections are enabled or if seeding infections after a period of time.")
      ((max-days-to-run 3650) fa nil nil nil "Run for up to this many days, stop after that regardless of active cases. Useful when exogenous infections are enabled.")
      ((pop-size 327000000) fa nil nil nil "The number of people to simulate. Limit 327 Million. Run time goes up sharply with this number roughly proportional to ~~ N log(N). The limit is 327M. This could be easily changed by changing a few constants. But note that over about 2^32 people the memory requirements would double. 327M takes about 32Gb i.e. about 10 bytes per person. Above 2^32 people it would be about 20 bytes per person.")
      ((initially-infected-nbr 100) fa nil nil nil "|<h1>Initial and exogenous infection</h1>|Number exogenously infected initially, assumed infected just now. Note that specifying less than 100 often produces highly random results; even highly infectious situations can die out randomly")
      ((exogenously-reintroduce-infection-p nil :type (satisfies is-binary)) fa nil nil nil "if true we reintroduce infection exogenously")
      ((exogenously-reintroduce-infection-rate-fv (* 1.0 1/100) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *exogenously-reintroduce-infection-rate-vec* :rates :days "fraction of population to exogenously reinfect per day. This is an optionlly time-varying parameter, see above.")
      ;;
      ((fatality-rate-sampling-count 10000)  fa nil nil nil "|<h1>Technical parameters.</h1>|Number of samples used check state transition probabilities generate the correct fatality rate;  Should be 10000 or greater")
      ;;
      ((headings t :type (satisfies is-binary)) fa nil nil nil "|<h1>Presentation options</h1>|Print headings for csv file output for summary and group summary information.")
      ((terse (list :summary :running-stats :group-stats) :type (satisfies valid-terseness)) fa nil nil nil "Reporting options. What things to display. A  list including <p>:summary :group-stats :running-stats :schedule-too-late :initial-infection-failure :initial-immunization-failure :quarantine-vulnerable-failure :computing-adjusted-fatality-rates :daily-all-stats.")
      ((heading-prefix "" :type (satisfies stringp)) fac nil nil nil "Prefix for heading in output summary global and by groups report")
      ((summary-line-prefix "" :type (satisfies stringp)) fac nil nil nil "Prefix for output line in output summary global and by groups reports, not including running-stats.")
      ;;
      ((scale-to-pop-size t :type (satisfies is-binary)) fa nil nil nil "|<h1>Scaling of parameters and results to population size</h1><p>Due to slow run times with high populations, it is useful to run smaller siumulations. The scaling allows input parameters to be adjusted according to population size. Input items with :rates or :days in the 4th and 5th parameters are adjusted according to the scaling formula. See adjust-days-a and adjust-days-b.|adjust some days and some rates for population size; raw duration estimate = (a=43.13)+ (b=10.13) * ln(pop-size), rates inverse of this")
      ;;
      ((adjust-days-canonical-population-size 327000000 :type (and (satisfies integerp) (satisfies plusp))) fa nil nil nil "Base population size use for parameter adjustment. That is, input the parameters as though for that size and they will be adjusted to the actual population size as required. Days are scaled down and rates are scaled up when the population is smaller thatn 327M.")
      ((adjust-days-a 5.11 :type (satisfies floatp)) fa nil nil nil "Adjust days for population size parameter A, Where days = adjust-days-a + adjust-days-b ln (population) = 5.11  + 15.69 * ln (populpation) see spreadsheet calibration of scaling.ods")
      ((adjust-days-b 15.69 :type (satisfies floatp)) fa nil nil nil "Adjust days for population size parameter B, Where days = adjust-days-a + adjust-days-b ln (population) = 5.11  + 15.69 * ln (populpation) see spreadsheet discrete-calibration-of-scaling.ods ")
      ((scale-output-costs-and-dayix t :type (or nil t)) fa nil nil nil "If on, scale outputs to match canonical population size. Ignored if scale-to-pop-size is off.")
      ;; 
      ((economic-cost-icu-day 3000.0 :type (satisfies floatp)) fa nil nil nil "|<h1>Economic Costs</h1>|Economic cost of a day in an ICU in dollars")
      ((economic-cost-mask-day 5.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of wearing a mask per day")
      ((economic-cost-quarantine-effectiveness-day 500.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of quarantine person/day")
      ((economic-cost-recuperating-day 150.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of a day recuperating in dollars")
      ((economic-cost-social-isolation-a-effectiveness-day 100.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of social isolation A person-effectiveness/day")
      ((economic-cost-social-isolation-b-effectiveness-day 300.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of social isolation B person-effectiveness/day")
      ((economic-cost-social-isolation-c-effectiveness-day 150.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of social isolation C person-effectiveness/day")
      ((economic-cost-symptomatic-day 200.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of a symptomatic day in dollars")
      ((economic-cost-taiwan-isolation-effectiveness-day 300.0 :type (satisfies floatp)) fa nil nil nil "Economic cost of taiwan isolation person-effectiveness/day")
      ((economic-value-qaly-dollars 30000.0 :type (satisfies floatp)) fa nil nil nil "Economic value of a quality adjusted life year in dollars")
      ;;
      ((hedonic-cost-icu-needed-day 1000.0 :type (satisfies floatp)) fa nil nil nil "|<h1>Hedonic Costs</h1><p>These attempt to measure non-financial costs in dollar terms or various states and events, for comparability.|Hedonic cost of a day that you need to be in an ICU in dollars")
      ((hedonic-cost-mask-day 10.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of wearing a mask per day") ;; Robin Hanson says more
      ((hedonic-cost-quarantine-effectiveness-day 200.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of quarantine person/day")
      ((hedonic-cost-recuperating-day 150.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of a day recuperating in dollars")
      ((hedonic-cost-social-isolation-a-effectiveness-day 150.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of social isolation A person-effectiveness/day")
      ((hedonic-cost-social-isolation-b-effectiveness-day 150.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of social isolation B person-effectiveness/day")
      ((hedonic-cost-social-isolation-c-effectiveness-day 150.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of social isolation C person-effectiveness/day")
      ((hedonic-cost-symptomatic-day 200.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of a symptomatic day in dollars")
      ((hedonic-cost-taiwan-isolation-effectiveness-day 150.0 :type (satisfies floatp)) fa nil nil nil "Hedonic cost of taiwan isolation person-effectiveness/day")
      ((hedonic-value-qaly-dollars 30000.0 :type (satisfies floatp)) fa nil nil nil "Hedonic value of a quality adjusted life year in dollars")
      ;;
      ((recuperation-period-icu 30 :type (satisfies integerp)) fa nil nil nil "|<h1>Costing related parameters</h1>|Time to recover from ICU tier illness")
      ((recuperation-period-symptomatic 10 :type (satisfies integerp)) fa nil nil nil "Time to recover from symptomatic tier illness")
      ;; 
      ((survivor-immunity-limitation-p nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Recovery and relapse parameters.</h1>|if true, recovered people may lose immunity")
      ((survivor-immunity-failure-fraction 0.2 :type (satisfies is-float-0-to-1)) fa nil nil nil "Fraction of recovered who lose immunity")
      ((survivor-immunity-failure-days-base 243 :type (satisfies integerp)) fa *survivor-immunity-failure-days-adj* nil :days "Days after recovery immunity of surivor is lost if lost")
      ;; 
      ((qaly-age-adjustment-base 16.0 :type (and (satisfies floatp) (satisfies not-minusp))) fa nil nil nil "|<h1>Quality adjustment for age.</h1>|QALY adjustment parameter. Formula is qaly-year-diff-factor^(abs(age - qaly-age-adjustment-base)).")
      ((qaly-year-diff-factor 0.99 :type (satisfies is-float-0-to-1)) fa nil nil nil "QALY adjustment parameter. Formula is qaly-year-diff-factor^(abs(age - qaly-age-adjustment-base)).")
      ;;
      ((qaly-fraction-retain-full-quality 0.9 :type (satisfies is-float-0-to-1)) fa nil nil nil "|<h1>QALY impact and immunity after recovery from illness.</h1>|When recovered from illness what fraction of people recover full quality of life.")
      ((qaly-fraction-retained-when-not-fully-retained-after-recovery 0.80 :type (satisfies is-float-0-to-1)) fa nil nil nil "when recovered from illness what fraction of life quality is retained for those people for whom not all is regained,")
      ((qaly-not-healthy-adjustment-factor 0.8 :type (satisfies is-float-0-to-1)) fa nil nil nil "Quality of life impact for being in a group market not healthy")
      ;;((recovered-fraction-not-fully-immune 0.05 :type (satisfies is-float-0-to-1)) fa nil nil nil "Fraction of the recovered from illness still somewhat susceptible to reinfection.")
      ;;((recovered-ineffectiveness-when-not-fully-immune 0.50 :type (satisfies is-float-0-to-1)) fa nil nil nil "Infectivity of the still-susceptible recovered from illness as fraction of that of the normal population.")
      ;;
      ((testing-regime nil :type (satisfies valid-testing-regime)) fa nil nil nil "|<h1>Overall proactive testing regime.</h1>|Contains either nil (no proactive testing) :simple-biased (test baised to those with symptoms) or :contact-tracing (trace contacts and possibly test persons found)")
      ((test-symptomatic-p nil :type (satisfies is-binary)) fa nil nil nil "Do we test the symptomatic who come in.")
      ((disclose-symptomatic-fraction 0.7 :type (satisfies is-float-0-to-1)) fa nil nil nil "Fraction with symptoms that report it thus testable i.e. we find out about, >0 means we test those ones")
      ((test-validity 7) fa nil nil nil "How long is a test valid until we might retest")
      ((testing-bias-infected-people 10.0) fa nil nil nil "How much the testing is biased to infected people. 4.0 = infected person is 4x times more likely to be tested than random.")
      ((testing-capacity-per-capita-fv (/ 1000000.0 327000000.0) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *testing-capacity-per-capita-vec* :rates :days "Tests per day per capita capacity based on 1m/327m for USA. This is an optionlly time-varying parameter, see above.")
      ;;
      ((contact-tracing-fraction-contacts-found 0.8 :type (satisfies is-float-0-to-1)) fa nil nil nil "|<h1>Contact Tracing Parameters, used if testing-regime = :contact-tracing.</h1>|Success probability in finding a given contact.")
      ((contact-tracing-test-validity-days-override 1) fa nil nil nil "How long a test is considered valid if done from contact tracing")
      ((monitoring-duration 28) fa nil nil nil "How long we monitor someone for symptoms after being traced as a contact.")
      ((number-of-contacts-per-person 10) fa nil nil nil "How many contacts a person has, of which we would find . ")
      ((test-all-contacts-p nil :type (satisfies is-binary)) fa nil nil nil "when contacts found, test even without symptoms, also test if symptoms as well")
      ;; 
      ((social-isolation-a-p nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Social Isolation parameters</h1>|Turns social isolation A on i.e. lockdown type intervention for selected groups.")
      ((social-isolation-b-p nil :type (satisfies is-binary)) fa nil nil nil "Turns social isolation B on i.e. lockdown type intervention for selected groups.")
      ((social-isolation-c-p nil :type (satisfies is-binary)) fa nil nil nil "Turns social isolation C on i.e. lockdown type intervention for selected groups.")
      ((social-isolation-a-groups '(0 1 2 3 4 5 6 7 8 9 10 11 12 13 16 17)) flm nil nil nil "Groups to socially isolate group A, typically children.  See +age-health-sex-groups+ for group IDs.")
      ((social-isolation-b-groups '(14 15 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45)) flm nil nil nil "Groups to socially isolate B typically healthy adults.  See +age-health-sex-groups+ for group IDs.")
      ((social-isolation-c-groups '(46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71)) flm nil nil nil "Groups to socially isolate C usually old/unhealthy.  See +age-health-sex-groups+ for group IDs.")
      ((social-isolation-ineffectiveness-a-fv 0.45 :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *social-isolation-ineffectiveness-a-vec* nil nil "What fraction of normal infectivity is retained with social isolation A. 1.0 = useless. 0.0 = totally blocks infection. This is an optionlly time-varying parameter, see above.")
      ((social-isolation-ineffectiveness-b-fv 0.45 :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *social-isolation-ineffectiveness-b-vec* nil nil "What fraction of normal infectivity is retained with social isolation B. 1.0 = useless. 0.0 = totally blocks infection. This is an optionlly time-varying parameter, see above.")
      ((social-isolation-ineffectiveness-c-fv 0.45 :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *social-isolation-ineffectiveness-c-vec* nil nil "What fraction of normal infectivity is retained with social isolation C. 1.0 = useless. 0.0 = totally blocks infection. This is an optionlly time-varying parameter, see above.")
      ;;
      ((icu-beds-per-capita-fv (* 3 3.0d-4) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *icu-beds-for-pandemic-per-capita-vec* :rates :days "|<h1>ICU parameters>/h1>|ICU beds per head of population including 3 fold ramp-up of capacity 3*100000/327000000.0. This is an optionlly time-varying parameter, see above.")
      ((icu-beds-per-capita-spoken-for (* 1/2 3.0d-4)) fa nil nil nil "ICU beds per head of ppulation that are required for other uses than CV treatment needed for other things. Based on 1/2 normally so with 3X ramp-up it's 1/6. This is an optionlly time-varying parameter, see above.")
      ((death-rate-multiple-needs-icu-no-icu-bed 3.999) fa nil nil nil "What to multiply death rate by for people who need ICU when no icu bed is available. Note the death rate may max out to 1.0 if already high.")
      ;;
      ((antigen-testing-program-p nil :type (or nil t)) fa nil nil nil "|<h1>Antigen Testing controls</h1>|Random antigen testing is done")
      ((antigen-testing-rate-fv 0.005 :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *antigen-testing-rate-vec* :rates :days "Antigen testing rate")
      ((antigen-testing-positive-remove-quarantine-p t :type (or nil t)) fa nil nil nil "if you have the antigen you are out of quarantine.")
      ((antigen-testing-positive-remove-isolations-p t :type (or nil t)) fa nil nil nil "if you have the antigen you are out of isolation.")
      ((antigen-testing-positive-remove-deliberately-infect-p t :type (or nil t)) fa nil nil nil "if you have the antigen we won't deliberately infect you.")
      ((antigen-false-positive-rate 0.02 :type (satisfies is-float-0-to-1)) fa nil nil nil "Fraction of people who never had it who test positive on a given test.")
      ((antigen-false-negative-rate 0.02 :type (satisfies is-float-0-to-1)) fa nil nil nil "Fraction of people who *have* had it who test *negative* on a given test.")
      ;;
      ((deliberately-infect-p nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Deliberate infection controls.</h1>|Deliberately infect some people.")
      ((deliberately-infect-fraction 0.4 :type (satisfies is-float-0-to-1)) fa nil nil nil "Fraction to deliberately infect (limit).")
      ((deliberately-infect-groups '(1 3 9 11 14 15 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41)) flm nil nil nil "Groups to deliberately infect. See +age-health-sex-groups+.")
      ((deliberately-infect-rate-fv (* 1.0 (/ 0.4 90 1.0)) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *deliberately-infect-rate-vec* :rates :days "Daily rate to deliberately infect (fraction of population) subject to overall limit. This is an optionlly time-varying parameter, see above.")
      ;;
      ((immunizationp nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Immunization controls and immunzation fidelity of the recovered.</h1>|We do immunization if true")
      ((immunization-ineffectiveness 0.2 :type (satisfies is-float-0-to-1)) fa nil nil nil "Infectivity of the immunized as fraction of that of the normal population.")
      ((immunize-initial-fraction 0.0) fa nil nil nil "Fraction of population initially in immunized state.")
      ((immunize-rate-fv '((0 0.01) (100 0.02) (200 0)) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *immunize-rate-vec* :rates :days "Immunizateion rate fraction pop per day. This is an optionlly time-varying parameter, see above.")
      ;;
      ((isolation-for-diagnosed-p nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Isolation Parameters.</h1>|Diagnosed go into isolation if quarantine not enabled or no bed available.")
      ((isolation-duration 14 :type (and (satisfies integerp) (satisfies plusp))) fa nil nil nil "H long we keep someone isolated due to symptoms or diagnosis.")
      ((isolation-for-diagnosed-ineffectiveness 0.30 :type (satisfies is-float-0-to-1)) fa nil nil nil "Infection leakage for those isolated compared to infectivity of normal persion.")
      ;;
      ((mask-distancing-p nil :type (or nil t)) fa nil nil nil "|<h1>Mask distancing controls.</h1>|Turns on mask physical distancing measures e.g. face mask. See e.g. \"Face Masks for the General Public\" Royal Society 2020")
      ((mask-distancing-fraction-compliant-fv 0.8 :type (or (satisfies is-float-0-to-1) (satisfies listp))) fa *mask-distancing-fraction-compliant-vec* nil :days "Fraction of people compliant to wearing face masks. This is an optionlly time-varying parameter, see above.")
      ((mask-distancing-outbound-ineffectiveness 0.3 :type (satisfies is-float-0-to-1)) fa nil nil nil "Remaining tendency of someone wearing a mask to infect someone compared to non-mask wearer.")
      ((mask-distancing-inbound-ineffectiveness 0.8 :type (satisfies is-float-0-to-1)) fa nil nil nil "Remaining tendency of someone wearing a mask to get infected compared to non-mask wearer.")
      ((taiwan-isolation-p nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Taiwan isolation controls</h1><p>This refers to measures that require people to avoid shops, trains, schools, workplaces if feverish or showing cold or flu symptoms, and which require those running these establishments to check and exclude same. This is an imperfect measure but cheap and rapid and surprisingly effective. Possibly better than higher status measures such as PCR tests which are slow, expensive, not that much more reliable, and which produce delayed results.|Taiwan isolation is happening.")
      ((taiwan-isolation-duration 1 :type (and (satisfies integerp) (satisfies plusp))) fa nil nil nil "How long we keep someone taiwan-isolated due to symptoms or fever.")
      ((taiwan-isolation-ineffectiveness-fv 0.25 :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm *taiwan-isolation-ineffectiveness-vec* nil nil "Infectivity of those identified as symptomatic or feverish compared to people not identified i.e. what fraction of the infected and contagious are not detected or do not go home and thus might infect someone.")
      ((not-cv-symptomatic-fraction 0.02  :type (satisfies is-float-0-to-1)) fa nil nil nil "Fraction of the population with fever or cold/flu symptoms at any time, not from CV")
      ;;
      ((quarantine-diagnosed-p nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Quarantine control</h1>|Diagnosed go into quarantine.")
      ((quarantine-vulnerable-p nil :type (satisfies is-binary)) fa nil nil nil "Quarantine some of the people in vulnerable groups")
      ((quarantine-deliberately-infected-p nil :type (satisfies is-binary)) fa nil nil nil "We do quanatine for deliberately infected if true")
      ((quarantine-ineffectiveness 0.02 :type (satisfies is-float-0-to-1)) fa nil nil nil "Infection leakage for those in quaranting relative to normal people, residual infectivity 0.20 or 0.10 or 0.02") 
      ;;
      ((quarantine-global-pool-p nil :type (or nil t)) fa nil nil nil "|<h1>Quarantine global pool control</h1><p>If used, all quarantine pools are subject to an overall global quarantine limit as well as the pool specific limit, with a priority scheme.|<p>If true, all quarantines use a single pool of beds.")
      ((quarantine-global-pool-priority-list (list :deliberately-infected :diagnosed :vulnerable) :type (satisfies valid-quarantine-priority-list)) fa nil nil nil "List of quarantines in priority order, hgh to low. If not in the list it does not participate in the priority scheme, just first in best dressed. The names of the pools are :diagnosed :deliberately-infected :vulnerable.")
      ((quarantine-global-beds-per-capita-fv (* 4 (/ 249000 25000000.0)) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm (aref *quarantine-beds-per-capita-vec* +quarantine-global+) :rates :days "What fraction of the population can we quarantine? Use (australia) hotel rooms per capita as a rough estimate, ( 2 (/ 249000 25000000.0)) x 2 (2  0.00996); usa ( 2 (/ 5000000 327000000.0)) 0.01529  2 produces a similar result; also agrees with rh figure of 10m. This is an optionlly time-varying parameter, see above.")
      ;;
      ((quarantine-diagnosed-beds-per-capita-fv (* 2 (/ 249000 25000000.0)) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm (aref *quarantine-beds-per-capita-vec* +quarantine-diagnosed+) :rates :days "|<h1>Control for quarantine of diagnosed</h1><p>See also quarantine-diagnosed-p.|<p>What fraction of the population can we quarantine? Use (australia) hotel rooms per capita as a rough estimate, ( 2 (/ 249000 25000000.0)) x 2 (2  0.00996); usa ( 2 (/ 5000000 327000000.0)) 0.01529,  a similar result; also agrees with rh figure of 10m. This is an optionlly time-varying parameter, see above.")
      ;;
      ((quarantine-deliberately-infected-required-to-infect-p nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Deliberate infection control</h1><p>See also quarantine-deliberately-infected-p.|<p>If true, do not deliberately infect if no quarantine spot free")
      ((quarantine-deliberately-infected-beds-per-capita-fv (* 2 (/ 249000 25000000.0)) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm (aref *quarantine-beds-per-capita-vec* +quarantine-deliberately-infected+) nil nil "Beds per capita for deliberately infected quarantine. This is an optionlly time-varying parameter, see above.")
      ((deliberately-infect-max-days-base 180 :type (and (satisfies integerp) (satisfies not-minusp))) fa *deliberately-infect-max-days-adj* nil :days "maximum days to do deliberate infection")
      ((variolationp nil :type (satisfies is-binary)) fa nil nil nil "|<h1>Variolation control</h1><p>I.e. deliberate infection with lowest effective dose to reduce infection severity.|<p>if true, do variolation ie dliberately infect with low safer dose, if not true, infect with high/normal/random dose.")
      ((variolation-severity-factor 0.1 :type (satisfies is-float-0-to-1)) fa nil nil nil "How severe variolation produced infection is relative to normal. Usually 0.03-0.1")
      ;;
      ((quarantine-vulnerable-groups '(46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71)) flm nil nil nil "|<h1>Control for quarantining vulnerable groups</h1><p>See also quarantine-vulnerable-p.|<p>The vulnerable groups as list see discrete-data.lisp for list and can be filtered by various parameters such as survival rates and age.")
      ((quarantine-vulnerable-beds-per-capita-fv (* 2 (/ 249000 25000000.0)) :type (or (satisfies is-float-0-to-1) (satisfies listp))) flm (aref *quarantine-beds-per-capita-vec* +quarantine-vulnerable+) nil nil "How many beds exist per capita for quarantining the vulnerable. This is an optionlly time-varying parameter, see above.")
      ;;
      ((r0-distribution-type nil :type (satisfies valid-r0-distribution)) fa nil nil nil "|<h1>Infectivity variation parameters</h1>|Distribution for daily-r0 nil = constant, :gamma-distribution = gamma with cv see r0-distribution-cv.")
      ((r0-distribution-cv 4.0 :type (satisfies is-float-plusp-less-than-10)) fa nil nil nil "Coefficient of variation of the r0 distribution (stdev/mean). Note a change to this forces a resampling of the daily-r0 parameter which is slow (~~5 seconds).")
      ((r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0 :type (satisfies is-float-0-to-1)) fa nil nil nil "This parameter controls the relationship between propensity to infect others and propensity to get infected. We use (r0-this-person)^RDE *daily-r0* where RDE is this parameter, to compute the relative probabilities of picking people as receivers for the illnes 0 = all same, 1 = according to infectivity daily-r0 for person. In effect this creates a second order effect for infectivity. See \"Individual variation in susceptibility or exposure to SARS-CoV-2 lowers the herd immunity threshold\" https://doi.org/10.1101/2020.04.27.20081893")
      ))

;; adjusted values of parameters - list or scalar

(defparameter *min-days-to-run-adj* :min-days-to-run-adj-not-init)

(defparameter *deliberately-infect-max-days-adj* :deliberately-infect-max-days-adj-not-init)

(defparameter *survivor-immunity-failure-days-adj* :survivor-immunity-failure-days-adj-not-init)

(defmacro make-parms-struct ()
  `(defstruct parms ,@(mapcar #'first +fields-in-parms+)))

(make-parms-struct)

(defmacro using-parms (&body body) 
  `(with-slots ,(mapcar (lambda (el) (first (first el))) +fields-in-parms+)
       *parms*
     ,@body))

(defconstant+ +adjustment-list+
  (remove-if #'not
    (loop for el in +fields-in-parms+ collect
         (let ((name (caar el))
               (fun (third el))
               (rates (fourth el))
               (days (fifth el)))
           (if (and (not fun) (or rates days)) (error "rates of days specified but not setter function for variable field ~s in ~s" fun el))
           (if fun
               (list name fun rates days)
               nil)))))

(defmacro make-getters-ht ()
  `(defparameter *getter-ht*
     (list2ht
      ,(cons 'list
             (loop for el in +fields-in-parms+ collect
                  (let ((name (caar el)))
                    `(list (quote ,name)
                           (lambda (&optional (parms nil)) ;;(format t "...dbg1644 parms ~S~%" parms)
                             (,(intern (concatenate 'string (string-upcase "parms-") (symbol-name name))) (or parms *parms*)))))))
      'equalp)))

(make-getters-ht)

(defun contact-tracing-on ()
  (eq (parms-testing-regime *parms*) :contact-tracing))

;;;OTHER GLOBAL PARAMETERS THAT SHOULD RARELY CHANGE NEED EDIT AND RELOAD

;; see also discrete-data.lisp file and state diagrams

;; global parameters that may change via parameters

;; see also +age-health-sex-groups+ for life expectancy and corona virus fatality rates by segment
;; see also *next-states-raw* for probability transitions during the disease
;; structs and constants

;;;GROUPS

(load "discrete-data.lisp")

(defstruct age-health-sex-group life-years fr-death-rate-cv fr-pop sex age-first age-last healthy age-mid group-nbr fatality-rate-adjuster)

(defconstant+ +group-assignments-rough-nbr+ 100000) ;; for pseudo random assignments of people to groups

(defparameter *group-assignments-by-proportion* (gensym))

(defun init-age-health-sex-groups ()
  (let* (;;(pop-accum 0.0)
         (ct 0)
         (nbr-groups (length +age-health-sex-groups+))
         (acc-grp-prob nil) ;; q+d way to generate random group, otherwise too slow
         (gps 
          (loop for gr in +age-health-sex-groups+ for ix from 0 by 1 collect
               (destructuring-bind (gn ly fdrc fp s af al h) gr
                 (assert (eql gn ix))
                 (let* ((mid (/ (+ af (1+ al)) 2.0)))
                   (progn
                     (incf ct)
                     (loop repeat (intify (* +group-assignments-rough-nbr+ fp)) do
                          (push (list (mod (sxhash (+ ix ct)) nbr-groups) ix) acc-grp-prob))
                     (make-age-health-sex-group :life-years ly :fr-death-rate-cv fdrc :fr-pop fp ;;:fr-pop-accum pop-accum
                                                :sex s :age-first af :age-last al :healthy h :age-mid mid :group-nbr ix
                                                :fatality-rate-adjuster 1.0)))))))
    (setf *group-assignments-by-proportion* ;; sort by semi-random nbr
          (vectorize (mapcar #'second (sort (copy-list acc-grp-prob) (lambda (el1 el2) (< (second el1) (second el2)))))))
    ;;(setf *age-health-sex-groups-vec* (vectorize gps))))
    (vectorize gps)))

(defparameter *age-health-sex-groups-vec* (init-age-health-sex-groups) "vector of age-health-sex-group sorted by increasing death rate from cv") 

;;;PERSON-STATES

(defconstant+ +person-state-labels+
  '(person-state-never-infected             
    person-state-infected                   
    person-state-shedding                   
    person-state-symptomatic                
    person-state-icu-needed
    person-state-recovered                  
    person-state-deceased                   
    person-state-immunized                 
    person-state-still-shedding))

(create-enum-names +person-state-names+ +person-state-labels+)

(defconstant+ +person-state-names-as-lists+ (mapcar #'list +person-state-labels+))

(create-enum-variables +person-state-labels+ person-state-max person-state-count)

(defconstant+ +infectable-states+
  (list +person-state-never-infected+ +person-state-immunized+)) ;; immunized may be partly infectable

(defconstant+ +infectious-states+
  (list +person-state-shedding+ +person-state-symptomatic+ +person-state-icu-needed+ +person-state-still-shedding+))

(defconstant+ +quarantine-states+
  (list +person-state-never-infected+ +person-state-infected+ +person-state-shedding+ +person-state-symptomatic+ +person-state-icu-needed+ +person-state-still-shedding+))

(defconstant+ +active-states+
  (list +person-state-infected+ +person-state-shedding+ +person-state-symptomatic+ +person-state-icu-needed+ +person-state-still-shedding+))

(defconstant+ +infected-states+
  (list +person-state-infected+ +person-state-shedding+ +person-state-symptomatic+ +person-state-icu-needed+ +person-state-still-shedding+))

;;;PERSON FIELDS

(defconstant+ +person-fields+ ;; name, initial value, type test to init (else set to default keyword) whether flag (thus different macros generated)
  '(
    (person-antigen-tested-negative-bb 0 'bit #'always-true :flag)
    (person-antigen-tested-positive-bb 0 'bit #'always-true :flag)
    (person-contact-tracing-bb 0 'bit #'always-true :flag)
    (person-deliberately-infected-bb 0 'bit #'always-true :flag)
    (person-exogenously-infected-bb 0 'bit #'always-true :flag)
    (person-group-nbr 0 '(integer 0 100) #'always-true :not-flag)
    (person-has-been-ever-infected-bb 0 'bit #'always-true :flag)
    (person-has-been-recently-infected-bb 0 'bit #'always-true :flag)
    (person-icu-bed-allocated-bb 0 'bit #'always-true :flag)
    (person-icu-bed-allocation-failure-bb 0 'bit #'always-true :flag)
    (person-in-isolation-bb 0 'bit #'always-true :flag)
    (person-occupying-quarantine-deliberately-infected-bb 0 'bit #'always-true :flag)
    (person-occupying-quarantine-diagnosed-bb 0 'bit #'always-true :flag)
    (person-occupying-quarantine-vulnerable-bb 0 'bit #'always-true :flag)
    (person-missing-out-quarantine-deliberately-infected-bb 0 'bit #'always-true :flag)
    (person-missing-out-quarantine-diagnosed-bb 0 'bit #'always-true :flag)
    (person-missing-out-quarantine-vulnerable-bb 0 'bit #'always-true :flag)
    (person-in-taiwan-isolation-bb 0 'bit #'always-true :flag)
    (person-quarantine-bed-allocation-failure-bb 0 'bit #'always-true :flag)
    (person-recently-deceased-bb 0 'bit #'always-true :flag)
    (person-recently-tested-bb 0 'bit #'always-true :flag)
    (person-recuperating-bb 0 'bit #'always-true :flag)
    (person-state 0 '(integer 0 11) #'always-true :not-flag)
    (person-tested-positive-bb 0 'bit #'always-true :flag)
    (person-who-infected-me 0 '(integer 0 327000001) #'contact-tracing-on :not-flag) ;; stored as person-ix+1, 0 = none, due to inefficient storage with -1
    (person-lost-immunity-bb 0 'bit #'always-true :flag)))

(defun person-field-is-flag (el)
  (let ((flag1 (fifth el)))
    (assert (find flag1 '(:flag :not-flag)))
    (eq flag1 :flag)))

(defun not-person-field-is-flag (el)
  (not (person-field-is-flag el)))

(defconstant+ +person-fields-flags+ (remove-if #'not-person-field-is-flag +person-fields+))

(defconstant+ +person-labels-flags+ (mapcar #'first +person-fields-flags+))

(defconstant+ +person-labels-flags-vec+ (vectorize +person-labels-flags+))

(create-enum-variables +person-labels-flags+ person-flag-max person-flag-count)

(defconstant+ +person-fields-not-flags+ (remove-if #'person-field-is-flag +person-fields+))

(defparameter *person-flags* :not-init-person-flags) ;; 2D array for storing person flags

(defun person-flag-indexes-fun ()
  (let* ((person-fields2
          (loop for el in (mapcar #'first +person-fields-flags+) collect
               (symbol-with-suffix el "-ix"))))
    person-fields2))

(defmacro build-flag-indexes ()
  (create-enum-variables-fun (person-flag-indexes-fun) 'person-flag-indexes-ix-max 'person-flag-indexes-ix-count))

(build-flag-indexes) ;;flag indexes

(defun person-define-variable-name (name)
  (intern (concatenate 'string "*" (symbol-name name) "*")))

(defun person-define-variable-default-init (name)
  (intern (concatenate 'string "NOT-INIT-" (symbol-name name)) (find-package "KEYWORD")))

(defun person-define-variable (name)
  `(defparameter
       ,(person-define-variable-name name)
     ,(person-define-variable-default-init name)))

(defmacro person-define-nonflag-variables ()
  (cons 'progn
        (loop for el in +person-fields-not-flags+ collect
             (person-define-variable (first el)))))

(person-define-nonflag-variables)

(defparameter *person-state-history*  :not-init-person-state-history)

(defparameter *person-flag-history*  :not-init-person-flag-history)

(inline+ person-flag-is-on)

(defun person-flag-is-on (person-ix flag-ix)
  (nonzerop (aref *person-flags* person-ix flag-ix)))

(inline+ person-flag-is-off)

(defun person-flag-is-off (person-ix flag-ix)
  (zerop (aref *person-flags* person-ix flag-ix)))

(defun person-define-flag-operations-one-var (name)
   (let* ((flag-ix (intern (string-upcase (concatenate 'string "+" (symbol-name name) "-ix+"))))) ;; "-tracking-flag-ix+"
     ;;(format t "..dbg675 name ~S flag-ix ~S~%" name flag-ix)
     `((defmacro ,(symbol-with-suffix name "-is-on") (person-ix)
         (list 'person-flag-is-on person-ix ',flag-ix))
       (defmacro ,(symbol-with-suffix name "-is-off") (person-ix)
         (list 'person-flag-is-off person-ix ',flag-ix))
       (defmacro ,(symbol-with-prefix name "set-") (person-ix)
         (list 'change-of-flag ',flag-ix person-ix t))
       (defmacro ,(symbol-with-prefix name "unset-") (person-ix)
         (list 'change-of-flag ',flag-ix person-ix nil))
       (defmacro ,(symbol-with-suffix name "-access") (person-ix)
         (list 'aref '*person-flags* ',flag-ix person-ix)))))

(defmacro person-define-flag-operations ()
  (cons 'progn (loop for el in +person-fields-flags+ append (person-define-flag-operations-one-var (first el)))))

(person-define-flag-operations)

(defun person-define-accessor (name)
  (let* ((array-name (intern (concatenate 'string "*" (symbol-name name) "*"))))
    `(defmacro ,name (person-ix)
       (list 'aref ',array-name person-ix))))

(defmacro person-define-accessors ()
  (cons 'progn
        (loop for el in +person-fields-not-flags+
           collect (person-define-accessor (first el)))))

(person-define-accessors) ;; OK

(defun valid-person-flag-ix (fix)
  (and (integerp fix)
       (not (minusp fix))
       (< fix +person-flag-count+)))

(defun person-display-flag-field (fix person-ix)
  (assert (valid-person-flag-ix fix))
  (list (aref +person-labels-flags-vec+ fix)
        (aref *person-flags* person-ix fix)))

(defun person-display-nonflag-field (name person-ix)
  (list 'list (intern (symbol-name name) "KEYWORD")
        (list 'if (list 'arrayp (person-define-variable-name name))
              (list 'aref (person-define-variable-name name) person-ix)
              :unused)))

(defmacro display-personm (person-ix)
  (list 'append
        (cons 'list
              (append (list (list 'list :nbr person-ix))
                      (loop for el in +person-fields-not-flags+ collect
                           (let ((name (first el)))
                             (person-display-nonflag-field name person-ix)))))
        `(loop for fix from 0 by 1 until (>= fix +person-flag-count+) collect
                       (person-display-flag-field fix ,person-ix))))

(defun display-person (person-ix)
  (display-personm person-ix))

(defun quarantine-excluded-antigen (person-ix)
  (and (parms-antigen-testing-positive-remove-quarantine-p *parms*) (person-antigen-tested-positive-bb-is-on person-ix)))

;;;EVENTS

(defstruct event
  (id  nil :type (integer 0 31))
  (person-ix nil :type (integer 0 327000000))
  (originating-person-ix -1 :type (integer -1 327000000)))

(defparameter *events-by-day* :not-init-events-by-day "linked list of events for each forthcoming day")

(defun schedule-event (new-dayix id person-ix &optional (originating-person-ix -1))
  (push (make-event :id id :person-ix person-ix :originating-person-ix originating-person-ix) (aref *events-by-day* new-dayix)))

(defun schedule-event-maybe (new-dayix event-type person-ix &optional (originating-person-ix -1))
  (if (< new-dayix (length *events-by-day*))
      (schedule-event new-dayix event-type person-ix originating-person-ix)
      (if (find :schedule-too-late (parms-terse *parms*))
          (warn "schedule event too late ~s" (list new-dayix event-type person-ix originating-person-ix)))))

(defconstant+ +event-labels+
  '(event-no-event
    event-get-infected
    event-get-shedding
    event-get-symptomatic
    event-get-icu
    event-get-recovered
    event-get-deceased
    event-get-immunized
    event-stop-monitoring
    event-unrecently-tested
    event-stop-isolating
    event-stop-taiwan-isolating
    event-get-still-shedding
    event-get-infected-deliberately
    event-get-lost-immunity
    event-get-infected-exogenously
    event-get-fully-recuperated
    event-get-contact-tracing))

(create-enum-variables +event-labels+ event-max event-count)

;;;STATE TRANSITIONS

 ;; event, prev state, new state, (transitions) list of subsequent-event, days lag, probability (nil=all remaining probability)
;; if prev-state is zero that is the default action for that event, usually an error
;; adjustable probability is to adjust the probabilitiies for different groups to produce the different death rates nil/t

(defstruct state-table-entry event prev-state next-state transitions)
(defstruct state-table-transition event days-delay probability adjustable-probability-power adjustable-probability-no-icu-bed
           adjustable-probability-variolation adjustable-probability-survivors)

;; never-infected => infected => symptomatic/icu => recovered/deceased
;; oversimplified
(defconstant+ +next-states-raw+
  (list
   (make-state-table-entry
    :event +event-get-infected+ :prev-state +person-state-never-infected+ :next-state +person-state-infected+
    :transitions (list
                  (make-state-table-transition :event +event-get-shedding+ :days-delay 4 :probability nil)))
   (make-state-table-entry
    :event +event-get-infected+ :prev-state +person-state-immunized+ :next-state +person-state-infected+
    :transitions (list
                  (make-state-table-transition :event +event-get-shedding+ :days-delay 4 :probability nil)))
   (make-state-table-entry :event +event-get-lost-immunity+ :prev-state +person-state-recovered+ :next-state +person-state-never-infected+ :transitions nil)
   (make-state-table-entry
    :event +event-get-infected-deliberately+ :prev-state +person-state-never-infected+ :next-state +person-state-infected+
    :transitions (list
                  (make-state-table-transition :event +event-get-shedding+ :days-delay 4 :probability nil)))
   (make-state-table-entry
    :event +event-get-infected-deliberately+ :prev-state +person-state-immunized+ :next-state +person-state-infected+
    :transitions (list
                  (make-state-table-transition :event +event-get-shedding+ :days-delay 4 :probability nil)))
   (make-state-table-entry :event +event-get-infected+ :prev-state nil :next-state nil :transitions nil) ;; other states the bugs bounce off
   (make-state-table-entry
    :event +event-get-immunized+ :prev-state +person-state-never-infected+ :next-state +person-state-immunized+
    :transitions nil)
   (make-state-table-entry :event +event-get-immunized+ :prev-state nil :next-state nil :transitions nil)
   (make-state-table-entry
    :event +event-get-shedding+ :prev-state +person-state-infected+ :next-state +person-state-shedding+
    :transitions (list
                  (make-state-table-transition :event +event-get-icu+ :days-delay 3 :probability 0.025 :adjustable-probability-power 2/3 :adjustable-probability-variolation t)
                  (make-state-table-transition :event +event-get-symptomatic+ :days-delay 3 :probability 0.775 :adjustable-probability-power nil)
                  (make-state-table-transition :event +event-get-still-shedding+ :days-delay 3 :probability nil)))
   (make-state-table-entry
    :event +event-get-still-shedding+ :prev-state +person-state-shedding+ :next-state +person-state-still-shedding+
    :transitions (list
                  (make-state-table-transition :event +event-get-recovered+ :days-delay 7 :probability nil)))
   (make-state-table-entry
    :event +event-get-symptomatic+ :prev-state +person-state-shedding+ :next-state +person-state-symptomatic+
    :transitions (list
                  (make-state-table-transition :event +event-get-deceased+ :days-delay 7 :probability 0.0005 :adjustable-probability-power 1 :adjustable-probability-variolation t)
                  (make-state-table-transition :event +event-get-recovered+ :days-delay 7 :probability nil)))
   (make-state-table-entry
    :event +event-get-icu+ :prev-state +person-state-shedding+ :next-state +person-state-icu-needed+
    :transitions (list
                  (make-state-table-transition :event +event-get-deceased+ :days-delay 7 :probability 0.15 :adjustable-probability-power 1/3 :adjustable-probability-no-icu-bed t)
                  (make-state-table-transition :event +event-get-recovered+ :days-delay 7 :probability nil)))
   (make-state-table-entry :event +event-get-recovered+ :prev-state nil :next-state +person-state-recovered+ :transitions nil)
   (make-state-table-entry :event +event-get-fully-recuperated+ :prev-state nil :next-state nil :transitions nil) ;; ignore for states, used to set off flag
   (make-state-table-entry :event +event-get-infected-exogenously+ :prev-state nil :next-state +person-state-never-infected+ :transitions nil)
   (make-state-table-entry :event +event-get-deceased+ :prev-state nil :next-state +person-state-deceased+ :transitions nil)
   (make-state-table-entry :event +event-stop-monitoring+ :prev-state nil :next-state nil :transitions nil) ;; ignore - handled by tracing type specific code if at all
   (make-state-table-entry :event +event-unrecently-tested+ :prev-state nil :next-state nil :transitions nil) ;; ignore - handled by tracing type specific code if at all
   (make-state-table-entry :event +event-stop-isolating+ :prev-state nil :next-state nil :transitions nil) ;; ignore - handled by tracing type specific code if at all
   (make-state-table-entry :event +event-stop-taiwan-isolating+ :prev-state nil :next-state nil :transitions nil) ;; ignore - tracing type specific code if at all
   (make-state-table-entry :event +event-get-contact-tracing+ :prev-state nil :next-state nil :transitions nil) ;; ignore - tracing type specific code if at all
   ))

;;;QUARANTINES

(defconstant+ +quarantine-names+ (mapcar #'first +quarantine-details+))

(defconstant+ +normal-quarantine-names+ (remove-if (lambda (el) (eql el 'quarantine-global)) (mapcar #'first +quarantine-details+)))

(create-enum-variables +quarantine-names+ quarantine-names-ix-max quarantine-names-ix-count)

(defconstant+ +quarantine-global-flags+
    (vectorize (mapcar (lambda (global-p-ish)
                         (assert (find global-p-ish '(:global :not-global)))
                         (eq global-p-ish :global))
                       (mapcar #'second +quarantine-details+))))

(defconstant+ +quarantine-fallback-isolation-flags+
    (vectorize (mapcar (lambda (fallback-p-ish)
                         (assert (find fallback-p-ish '(:fallback-isolation :not-fallback-isolation)))
                         (eq fallback-p-ish :fallback-isolation))
                       (mapcar #'third +quarantine-details+))))

(defconstant+ +quarantine-names-as-lists+ (mapcar #'list +quarantine-names+))

(defconstant+ +normal-quarantine-names-as-lists+ (mapcar #'list +normal-quarantine-names+))

(defun simple-rep-fun (entry-skeleton entry-parameters)
  `(list
    ,@(loop for ep in (if (symbolp entry-parameters) (symbol-value entry-parameters) entry-parameters)
         collect `(funcall (lambda (parms) parms ,entry-skeleton) ',ep))))

(defmacro simple-rep (entry-skeleton entry-parameters)
  (simple-rep-fun entry-skeleton entry-parameters))

(defmacro very-simple-rep (symbol1 symbol2 &optional (entry-parameters +quarantine-names-as-lists+))
  `(simple-rep (merge-symbols ,symbol1 (first parms) ,symbol2) ,entry-parameters))

(defconstant+ +quarantine-counts-labels+
    (append '(quarantine-dummy-ix)
            (very-simple-rep 'occupying- '-ix)
            (very-simple-rep 'missing-out- '-ix)))

(create-enum-variables +quarantine-counts-labels+ quarantine-counts-ix-max quarantine-counts-ix-count)

(defconstant+ +all-normal-quarantines+ (mapcar #'symbol-value (simple-rep (merge-symbols '+ (first parms) '+) +normal-quarantine-names-as-lists+)))

(defconstant+ +quarantine-types-to-occupying-count-ixs+
  (vectorize (mapcar #'symbol-value (simple-rep (merge-symbols '+OCCUPYING- (first parms) '-ix+) +quarantine-names-as-lists+))))

(defconstant+ +quarantine-types-to-missing-out-count-ixs+
  (vectorize (mapcar #'symbol-value (simple-rep (merge-symbols '+MISSING-OUT- (first parms) '-ix+) +quarantine-names-as-lists+))))

(inline+ q2o)

(defun q2o (qix)
  "quarantine index to occupying count / queue index"
  (aref +quarantine-types-to-occupying-count-ixs+ qix))

(inline+ q2m)

(defun q2m (qix)
  "quarantine index to missing out count / queue index"
  (aref +quarantine-types-to-missing-out-count-ixs+ qix))

(defun make-filled-in-entries (size ixs vals)
  (let ((arr (make-array size :initial-element nil)))
    (loop for ix in ixs for val in vals do
         (assert (numberp ix))
         (assert (numberp val))
         (setf (aref arr ix) val))
    arr))

(defconstant+ +quarantine-types-to-missing-out-flag-ixs+
  (make-filled-in-entries
   +quarantine-counts-ix-count+
   (mapcar #'symbol-value (simple-rep (merge-symbols '+quarantine- (first parms) '+) +basic-quarantine-categories-lists+)) ;; ixs
   (mapcar #'symbol-value (simple-rep (merge-symbols '+person-missing-out-quarantine- (first parms) '-bb-ix+) +basic-quarantine-categories-lists+))));; vals

(defconstant+ +quarantine-types-to-occupying-flag-ixs+
  (make-filled-in-entries
   +quarantine-counts-ix-count+
   (mapcar #'symbol-value (simple-rep (merge-symbols '+quarantine- (first parms) '+) +basic-quarantine-categories-lists+)) ;; ixs
   (mapcar #'symbol-value (simple-rep (merge-symbols '+person-occupying-quarantine- (first parms) '-bb-ix+) +basic-quarantine-categories-lists+))));; vals

;; Priorities vary according to the call parameters thus redone per run

(defparameter *quarantine-priorities-numeric* :not-init-quarantine-priorities-numeric)

(defparameter *quarantine-priorities-numeric-reverse* :not-init-quarantine-priorities-numeric)

(defparameter *quarantine-lower-priorities-numeric-vec* :not-init-quarantine-lower-priorities-numeric-vec)

(defparameter *quarantine-higher-priorities-numeric-vec* :not-init-quarantine-higher-priorities-numeric-vec)

(defparameter *quarantine-unprioritized-numeric* :not-init-quarantine-unprioritized-numeric)

(defparameter *quarantine-all-reverse-priority-numeric* :not-init-quarantine-all-reverse-priority-numeric)

(defparameter *quarantine-all-normal-priority-numeric* :not-init-quarantine-all-normal-priority-numeric)

(defparameter *quarantine-beds-tally-count* (make-array +quarantine-counts-ix-count+ :initial-element 0))

(defparameter *quarantine-beds-per-capita-vec* (make-array +quarantine-counts-ix-count+ :initial-element :not-init-quarantine-beds-per-capita-vec))

;; (defun set-quarantine-beds-per-capita-vec (ix val)
;;   (setf (aref *quarantine-beds-per-capita-vec* ix) val))

(defparameter *quarantine-priority-mapping-lookup-ht*
  (list2ht (mapcar #'list
                   +valid-quarantine-priorities+
                   (list  +quarantine-deliberately-infected+ +quarantine-vulnerable+ +quarantine-diagnosed+))
           'eq))

(defun set-up-quarantine-priorities ()
  (setf *quarantine-priorities-numeric*
        (remove-dups 
         (mapcar (lambda (el) (gethash el *quarantine-priority-mapping-lookup-ht*))
                 (parms-quarantine-global-pool-priority-list *parms*))
         'equal))
  (setf *quarantine-priorities-numeric-reverse* (reverse *quarantine-priorities-numeric*))
  (setf *quarantine-lower-priorities-numeric-vec* (make-array +quarantine-counts-ix-count+ :initial-element nil))
  (loop for quar-ix in *quarantine-priorities-numeric* do
       (assert (not (aref *quarantine-lower-priorities-numeric-vec* quar-ix)))
       (let ((posn (position quar-ix *quarantine-priorities-numeric*)))
         (setf (aref *quarantine-lower-priorities-numeric-vec* quar-ix)
               (if posn
                   (reverse (subseq *quarantine-priorities-numeric* (1+ posn))) ;; reverse so lowest priority come up first
                   nil))))
  (setf *quarantine-higher-priorities-numeric-vec* (make-array +quarantine-counts-ix-count+ :initial-element nil))
  (loop for quar-ix in *quarantine-priorities-numeric* do
       (assert (not (aref *quarantine-higher-priorities-numeric-vec* quar-ix)))
       (let ((posn (position quar-ix *quarantine-priorities-numeric*)))
         (setf (aref *quarantine-higher-priorities-numeric-vec* quar-ix)
               (if posn
                   (subseq (listify *quarantine-higher-priorities-numeric-vec*) 0 posn) ;; do not reverse, so highest priority come up first
                   nil))))
  (setf *quarantine-unprioritized-numeric*
        (remove-if (lambda (qix) (find qix *quarantine-priorities-numeric*))
                   (mapcar (lambda (el) (gethash el *quarantine-priority-mapping-lookup-ht*))
                           +valid-quarantine-priorities+)))
  (setf *quarantine-all-reverse-priority-numeric* (append *quarantine-unprioritized-numeric* *quarantine-priorities-numeric-reverse*))
  (setf *quarantine-all-normal-priority-numeric* (reverse *quarantine-all-reverse-priority-numeric*))
  :OK)

(defparameter *quarantine-linked-lists-mleps* :not-init-quarantine-linked-lists-mleps)

(defun person-init-quarantine-linked-lists (pop-size)
  (setf *quarantine-linked-lists-mleps* (make-multiple-dll pop-size +quarantine-counts-ix-count+ :person-init-quarantine-linked-lists)))

(defparameter *quarantine-parm-on-by-quarantine-ix* (make-array +quarantine-counts-ix-count+))

;;;PERSON UTILITIES RELATED TO QUARANTINE

(defun isolate-person (pix)
  (set-person-in-isolation-bb pix)
  (schedule-event (+ *dayix* (parms-isolation-duration *parms*)) +event-stop-isolating+ pix -1))

(defun maybe-isolate-person (pix)
  (if (not (quarantine-excluded-antigen pix))
      (isolate-person pix)))

(defun unisolate-person (pix)
  (unset-person-in-isolation-bb pix))

(defun quarantine-in-out-action (qix pix in-flag)
  (if (aref +quarantine-fallback-isolation-flags+ qix)
      (if in-flag
          (progn
            (unisolate-person pix)) ;; into q
          (progn
            (maybe-isolate-person pix)))))

(defun scan-people-do-things (try-fun want-nbr limit-try-nbr)
  "call try function with semi-random person-ix up to limit-try-nbr times, or until it returns t want-nbr times"
  (block fun
    (using-parms
      (let ((next-person-ix (random pop-size))
            (inc (block done1
                   (loop for inc1 in +scan-increments+ do
                        (if (<= (gcd inc1 pop-size) 1)
                            (return-from done1 inc1)))
                   (error "population must be insanely high")))
            ;;(if (/= pop-size +scan-increment1+) +scan-increment1+ +scan-increment2+))
            (got-count 0)
            (try-count 0))
        (loop do
             (let ((result (funcall try-fun next-person-ix)))
               (if result (incf got-count))
               (if (>= got-count want-nbr)
                   (return-from fun (values t got-count try-count)))
               (incf try-count)
               (if (> try-count limit-try-nbr)
                   (return-from fun (values nil got-count try-count)))
               (setf next-person-ix (mod (+ next-person-ix inc) pop-size))))))))

;;;STATISTICS, COUNTERS AND REPORTING DATA

(load "discrete-stats-fields-options.lisp") ;; Options for capture and reporting of data

(defun symbol-count-ever (symbol) (symbol-with-prefix symbol "ix-count-ever-"))

(defun symbol-count-current (symbol) (symbol-with-prefix symbol "ix-count-current-"))

(defun symbol-count-turned-on (symbol) (symbol-with-prefix symbol "ix-count-turned-on-"))

(defun build-counter-and-accumulator-indexes-fun ()
  (let* ((person-flag-counters
          (loop for el in +person-labels-flags+ append
               (list (symbol-count-ever el) (symbol-count-turned-on el) (symbol-count-current el))))
         (state-counters
          (loop for el in +person-state-labels+ append
               (list (symbol-count-ever el) (symbol-count-turned-on el) (symbol-count-current el))))
         (extra-counters
          (loop for el in +extra-accumulator-labels+ collect el))
         (all-counters (append person-flag-counters state-counters extra-counters)))
    all-counters))

(defmacro build-counter-and-accumulator-indexes ()
  (create-enum-variables-fun (build-counter-and-accumulator-indexes-fun) 'counter-max 'counter-count))

(build-counter-and-accumulator-indexes)

(defconstant+ +states-to-counters-ever+
  (make-filled-in-entries
   +person-state-count+
   (mapcar #'symbol-value (simple-rep (merge-symbols '+ (first parms) '+) +person-state-names-as-lists+)) ;; ixs
   (mapcar #'symbol-value (simple-rep (merge-symbols '+ix-count-ever- (first parms) '+) +person-state-names-as-lists+))));; vals

(defconstant+ +states-to-counters-turned-on+
  (make-filled-in-entries
   +person-state-count+
   (mapcar #'symbol-value (simple-rep (merge-symbols '+ (first parms) '+) +person-state-names-as-lists+)) ;; ixs
   (mapcar #'symbol-value (simple-rep (merge-symbols '+ix-count-turned-on- (first parms) '+) +person-state-names-as-lists+))));; vals

(defconstant+ +states-to-counters-current+
  (make-filled-in-entries
   +person-state-count+
   (mapcar #'symbol-value (simple-rep (merge-symbols '+ (first parms) '+) +person-state-names-as-lists+)) ;; ixs
   (mapcar #'symbol-value (simple-rep (merge-symbols '+ix-count-current- (first parms) '+) +person-state-names-as-lists+))));; vals

(defconstant+ +counter-and-accumulator-names+ (build-counter-and-accumulator-indexes-fun))

(defconstant+ +counter-and-accumulator-names-vec+ (vectorize +counter-and-accumulator-names+))

(defparameter *vec-stats-all* :not-inited-vec-stats-all "counter stats global")

(defparameter *stats-vec-groups-vecs* :not-inited-*stats-vec-groups-vecs* "array of counter stats per group")

;;;COUNTER IXS AND FUNCTIONS RELATING TO PERSON ATTRIBUTES

(defconstant+ +person-flag-ever-ixs+
    (vectorize (loop for ix from 0 by 1 until (>= ix +person-flag-indexes-ix-count+) collect
                    (symbol-value (merge-symbols '+ (symbol-count-ever (nth ix +person-labels-flags+)) '+)))))
                    
(defconstant+ +person-flag-current-ixs+
    (vectorize (loop for ix from 0 by 1 until (>= ix +person-flag-indexes-ix-count+) collect
                    (symbol-value (merge-symbols '+ (symbol-count-current (nth ix +person-labels-flags+)) '+)))))
                    
(defconstant+ +person-flag-turned-on-ixs+
    (vectorize (loop for ix from 0 by 1 until (>= ix +person-flag-indexes-ix-count+) collect
                    (symbol-value (merge-symbols '+ (symbol-count-turned-on (nth ix +person-labels-flags+)) '+)))))

(fmakunbound 'change-of-flag)

;;(flag-ix person-ix flag-on-p)
(defun change-of-flag (flag-ix person-ix flag-on-p)
  (assert (or (eq flag-on-p t) (eq flag-on-p nil)))
  ;;(format t "..dbg657 change of flag ~S~%" (list name array flag-ix base-offset person-ix new-value))
  (let* ((new-value (if flag-on-p 1 0))
         (old-value (aref *person-flags* person-ix flag-ix)))
    (if (not (eql old-value new-value))
        (let ((group-stats (aref *stats-vec-groups-vecs* (person-group-nbr person-ix))))
          (if flag-on-p
              (progn
                (incf (aref group-stats (aref +person-flag-current-ixs+ flag-ix)))
                (incf (aref group-stats (aref +person-flag-turned-on-ixs+ flag-ix)))
                (if (zerop (aref *person-flag-history* flag-ix person-ix))
                    (progn
                      (incf (aref group-stats (aref +person-flag-ever-ixs+ flag-ix)))
                      (setf (aref *person-flag-history* flag-ix person-ix) 1))))
              (progn
              ;; new-off
                (decf (aref group-stats (aref +person-flag-current-ixs+ flag-ix)))
                (assert (not (minusp (aref group-stats (aref +person-flag-current-ixs+ flag-ix)))))))
          (setf (aref *person-flags* person-ix flag-ix) new-value))
        :no-change)))

;;;GLOBAL VARIABLES AND INIT

;; Person attributes and data are all vectors or arrays (flags); much more compact in memory this way

(defparameter *pop-size* :not-init-pop-size "number of people in simulation")

(defun to-pop-size (rate)
  (intify (* rate *pop-size*)))

(defparameter *new-pop-size* :not-init-new-pop-size "number of people in new simulation")

(defparameter *infection-lists-mleps* :not-init-infection-lists-mleps)

;; replaced by *quarantine-beds-per-capita-vec*

(defparameter *icu-beds-for-pandemic-per-capita-vec* :icu-beds-for-pandemic-vec-not-inited)

(defparameter *deliberately-infect-rate-vec* :deliberately-infect-rate-not-inited "rate of infection")

(defparameter *exogenously-reintroduce-infection-rate-vec* :exogenously-reintroduce-infection-rate-vec-not-init "")

(defparameter *icu-beds-used* 0)

(defparameter *icu-failure-count* :icu-failure-count-not-inited)

(defparameter *quarantine-failure-count* :quarantine-failure-count-not-inited)

(defparameter *tests-done-today* :tests-done-today-not-inited)

(defparameter *mask-distancing-fraction-compliant-vec* :mask-distancing-fraction-compliant-vec-not-inited)

(defparameter *social-isolation-ineffectiveness-a-vec* :social-isolation-ineffectiveness-a-vec-not-inited)

(defparameter *social-isolation-ineffectiveness-b-vec* :social-isolation-ineffectiveness-b-vec-not-inited)

(defparameter *social-isolation-ineffectiveness-c-vec* :social-isolation-ineffectiveness-c-vec-not-inited)

(defparameter *taiwan-isolation-ineffectiveness-vec* :taiwan-isolation-ineffectiveness-vec-not-inited)

(defparameter *avg-days-infectious* :not-init-avg-days-infectious)

(defparameter *next-states-ht* nil)

(defparameter *quarantine-vulnerable-groups* :quarantine-vulnerable-groups-not-inited)

(defparameter *daily-r0* :not-init-daily-r0) ;; r0 on a daily basis - depends on days infectious

(defparameter *daily-stats* :not-inited-daily-stats)

(defparameter *deliberately-infect-count* :deliberately-infect-count-not-inited "# infected")

(defparameter *deliberately-infect-desired-number* :deliberately-infected-number-not-inited "# to infect")

(defparameter *deliberately-infecting-groups-vec* :deliberately-infecteding-groups-vec-not-inited)

(defparameter *social-isolation-groups-vec* :social-isolation-groups-vec-not-inited)

(defparameter *last-dayix* :last-dayix-not-inited)

(defparameter *exogenously-reintroduce-infection-count* :exogenously-reintroduce-infection-count-not-inited "# exogenously reinfected")

(defparameter *exogenously-reintroduce-infection-failure-count* :exogenously-reintroduce-infection-failure-count-not-init "# times failed to exogenously reinfect")

(defparameter *deliberately-infected-tried-count* :deliberately-infected-tried-count-not-inited "# tried to infect")

(defparameter *immunize-rate-vec* :immunize-rate-vec-not-inited "rate of immunization over time")

(defparameter *quarantine-deliberately-infected-failure-count* :quarantine-deliberately-infected-failure-count-not-inited)

(defparameter *antigen-testing-rate-vec* :antigen-testing-rate-vec-not-inited)

(defparameter *quarantine-diagnosed-failure-count* :quarantine-diagnosed-failure-count-not-inited)

(defparameter *dayix-adjustment-factor-out* :not-init-dayix-adjustment-factor)

(defparameter *rate-adjustment-factor-out* :not-init-rate-adjustment-factor)

(defparameter *testing-capacity-per-capita-vec* :not-init-testing-capacity-per-capita-vec) ;; tests per day based on 100k/327m for usa

(defun person-init-infection-lists (pop-size)
  (setf *infection-lists-mleps* (make-multiple-dll pop-size pop-size :person-init-infection-lists)))

;;;PERSON-STATE AND PERSON-FLAG RELATED COMPLEX DECISION PREDICATES

(defconstant+ +infectious-state-flags+
  (let ((infectious-state-flags (make-array +person-state-count+ :initial-element nil :element-type '(or null t))))
    (loop for istate in +infectious-states+ do
         (setf (aref infectious-state-flags istate) t))
    infectious-state-flags))

(inline+ person-is-infectious)

(defun person-is-infectious (person-ix)
  (aref +infectious-state-flags+ (person-state person-ix)))

(inline+ person-is-in-quarantine)

(defun person-is-in-quarantine (person-ix)
  (or (person-occupying-quarantine-diagnosed-bb-is-on person-ix)
      (person-occupying-quarantine-deliberately-infected-bb-is-on person-ix)
      (person-occupying-quarantine-vulnerable-bb-is-on person-ix)))

(inline+ person-missing-out-quarantine)

(defun person-missing-out-quarantine (person-ix)
  (or (person-missing-out-quarantine-diagnosed-bb-is-on person-ix)
      (person-missing-out-quarantine-deliberately-infected-bb-is-on person-ix)
      (person-missing-out-quarantine-vulnerable-bb-is-on person-ix)))

(inline+ person-in-or-missing-out-quarantine)

(defun person-in-or-missing-out-quarantine (person-ix)
  (or (person-is-in-quarantine person-ix)
      (person-missing-out-quarantine person-ix)))

(inline+ person-is-in-isolation)

(defun person-is-in-isolation (person-ix)
  (or (person-in-isolation-bb-is-on person-ix)
      (person-in-taiwan-isolation-bb-is-on person-ix)))

(inline+ person-is-distanceable)

(defun person-is-distanceable (person-ix)
  (and (not (eql (person-state person-ix) +person-state-deceased+))
       (not (eql (person-state person-ix) +person-state-icu-needed+))
       (not (person-is-in-quarantine person-ix))
       (not (person-is-in-isolation person-ix))))

(defconstant+ +infected-state-flags+
  (let ((infected-state-flags (make-array +person-state-count+ :initial-element nil :element-type '(or null t))))
    (loop for istate in +infected-states+ do
         (setf (aref infected-state-flags istate) t))
    infected-state-flags))

(defconstant+ +quarantine-applicable-flags+
  (let ((quarantine-applicable-flags (make-array +person-state-count+ :initial-element nil :element-type '(or null t))))
    (loop for istate in +quarantine-states+ do
         (setf (aref quarantine-applicable-flags istate) t))
    quarantine-applicable-flags))

(inline+ quarantine-is-applicable)

(defun quarantine-is-applicable (state)
  (aref +quarantine-applicable-flags+ state))

(defconstant+ +active-state-flags+
  (let ((active-state-flags (make-array +person-state-count+ :initial-element nil :element-type '(or null t))))
    (loop for istate in +active-states+ do
         (setf (aref active-state-flags istate) t))
    active-state-flags))

(inline+ person-is-active)

(defun person-is-active (person-ix)
  (or
   (aref +active-state-flags+ (person-state person-ix))
   (person-contact-tracing-bb-is-on person-ix)))

(inline+ person-is-infected)

(defun person-is-infected (person-ix)
  (aref +infected-state-flags+ (person-state person-ix)))

(defparameter *infectable-state-flags*
  (let ((infectable-state-flags (make-array +person-state-count+ :initial-element nil :element-type '(or null t))))
    (loop for istate in +infectable-states+ do
         (setf (aref infectable-state-flags istate) t))
    infectable-state-flags))

(inline+ person-is-infectable)

(defun person-is-infectable (person-ix)
  (aref *infectable-state-flags* (person-state person-ix)))

;;;COMPLEX UTILITY FUNCTIONS

(defun record-exit-old-state (old-state person-ix)
  (let ((group-stats (aref *stats-vec-groups-vecs* (person-group-nbr person-ix))))
    (decf (aref group-stats (aref +states-to-counters-current+ old-state)))
    :OK))

(defun record-entry-new-state (new-state person-ix)
  (let ((group-stats (aref *stats-vec-groups-vecs* (person-group-nbr person-ix))))
    (if (zerop (aref *person-state-history* new-state person-ix))
        (progn
          (incf (aref group-stats (aref +states-to-counters-ever+ new-state)))
          (setf (aref *person-state-history* new-state person-ix) 1)))
    (incf (aref group-stats (aref +states-to-counters-turned-on+ new-state)))
    (incf (aref group-stats (aref +states-to-counters-current+ new-state)))
    :OK))

(defun set-up-person-specials (person-ix)
  (let* ((gn (aref *group-assignments-by-proportion* (random (length *group-assignments-by-proportion*))))
         (gr (aref *age-health-sex-groups-vec* gn)))
    (setf (person-group-nbr person-ix) (age-health-sex-group-group-nbr gr))
    (setf (person-state person-ix) +person-state-never-infected+)
    (record-entry-new-state (person-state person-ix) person-ix)
    (person-group-nbr person-ix)))

(defun current-quarantine-beds (qix)
  (to-pop-size
   (get-array-entry-or-scalar (aref *quarantine-beds-per-capita-vec* qix) *dayix*)))

(defun current-icu-beds-for-pandemic ()
  (to-pop-size (get-array-entry-or-scalar *icu-beds-for-pandemic-per-capita-vec* *dayix*)))

(defun current-deliberately-infect-number ()
  (to-pop-size (get-array-entry-or-scalar *deliberately-infect-rate-vec* *dayix*)))

(defun current-spontaneous-infection-rate ()
  (get-array-entry-or-scalar *exogenously-reintroduce-infection-rate-vec* *dayix*))

;;;ALLOCATION and DEALLOCATION AND STATUS CHANGE

(defun deallocate-icu-bed (person-ix)
  (decf *icu-beds-used*)
  (unset-person-icu-bed-allocated-bb person-ix)
  (assert (not (minusp *icu-beds-used*)))
  (values *icu-beds-used* person-ix))

(defun deallocate-icu-bed-if-needed (person-ix new-state)
  (if (not (eql new-state +person-state-icu-needed+))
      (progn
        (if (person-icu-bed-allocated-bb-is-on person-ix)
            (deallocate-icu-bed person-ix)
            nil))
      (unset-person-icu-bed-allocation-failure-bb person-ix)))

(defun allocate-icu-bed (person-ix)
  (assert (person-icu-bed-allocated-bb-is-off person-ix))
  (if (< *icu-beds-used* (current-icu-beds-for-pandemic))
      (progn
        (incf *icu-beds-used*)
        (set-person-icu-bed-allocated-bb person-ix))
      (progn
        (incf *icu-failure-count*)
        (set-person-icu-bed-allocation-failure-bb person-ix)))
  (values *icu-beds-used* person-ix))

(defun taiwan-isolate-person (person-ix)
  (set-person-in-taiwan-isolation-bb person-ix)
  (schedule-event (+ *dayix* (parms-taiwan-isolation-duration *parms*)) +event-stop-taiwan-isolating+ person-ix -1))

(defun identify-bump-candidate (quarantine-ix)
  (block fun1
    (if (not (parms-quarantine-global-pool-p *parms*))
        (return-from fun1 (values nil :pool-full-no-global-pool-to-bump-from)))
    (let ((quar-victims (aref *quarantine-lower-priorities-numeric-vec* quarantine-ix)))
      (loop for quar-victim in quar-victims do
           (let ((occupying-ix (q2o quar-victim)))
             (if (plusp (aref *quarantine-beds-tally-count* occupying-ix))
                 (return-from fun1 (values quar-victim :found-bump-class))))))
    (values nil :no-bump-candidate-found)))

(defun person-direct-into-missing-out (person-ix quarantine-ix) ;; person is not in any quarantine; put into missing out queue then rebalance
  "put a person into the missing-out group for a quarantine, then call rebalance to possibly put them into the quarantine"
  (if (aref *quarantine-parm-on-by-quarantine-ix* quarantine-ix)
      (let ((missing-out-flag-ix (aref +quarantine-types-to-missing-out-flag-ixs+ quarantine-ix))
            (missing-out-ix (q2m quarantine-ix)))
        (assert (not (person-in-or-missing-out-quarantine person-ix)))
        (incf (aref *quarantine-beds-tally-count* missing-out-ix))
        (change-of-flag missing-out-flag-ix person-ix t)
        (funcall (multiple-dll-eps-add *quarantine-linked-lists-mleps*) person-ix missing-out-ix)
        (if (aref +quarantine-fallback-isolation-flags+ quarantine-ix)
            (maybe-isolate-person person-ix)) ;; isolate until put in the quarantine
        )
      :OK)
  :option-not-on)

(defun demote-person-from-quarantine (person-ix quarantine-ix)
  "put a person into the missing-out group for a quarantine from occupying"
  (if (aref *quarantine-parm-on-by-quarantine-ix* quarantine-ix)
      (let ((occupying-flag-ix (aref +quarantine-types-to-occupying-flag-ixs+ quarantine-ix))
            (missing-out-flag-ix (aref +quarantine-types-to-missing-out-flag-ixs+ quarantine-ix))
            (missing-out-ix (q2m quarantine-ix))
            (occupying-ix (q2o quarantine-ix)))
          (assert (person-flag-is-on person-ix occupying-flag-ix))
          ;; Put in missing out bucket and then rebalance
          (incf (aref *quarantine-beds-tally-count* missing-out-ix))
          (decf (aref *quarantine-beds-tally-count* occupying-ix))
          (change-of-flag missing-out-flag-ix person-ix t)
          (change-of-flag occupying-flag-ix person-ix nil)
          (funcall (multiple-dll-eps-del *quarantine-linked-lists-mleps*) person-ix occupying-ix)
          (funcall (multiple-dll-eps-add *quarantine-linked-lists-mleps*) person-ix missing-out-ix)
          (if (aref +quarantine-fallback-isolation-flags+ quarantine-ix)
              (maybe-isolate-person person-ix)) ;; isolate until put in the quarantine
          ;;(rebalance-quarantines-by-priority)
          )
      :OK)
  :option-not-on)

(defun demote-1-from-quarantine (qix)
  (let* ((occupying-ix (q2o qix))
         (pix (funcall (multiple-dll-eps-pop *quarantine-linked-lists-mleps*) occupying-ix)))
    (assert pix)
    (demote-person-from-quarantine pix qix)))

(defun daily-trim-quarantines ()  ;; First thing each day resiZe the quarantines to reflect possibly changed sizes
  ;; 1.  for all quarantines even those not prioritized
  ;; If global limit:
  ;; 2. in order of priority unprioritized then lowest to highest:
  ;; - reduce all quarantines to or below their own limit or lower if global limit is exceeded
  (let* ((global-limit-p (parms-quarantine-global-pool-p *parms*))
         (global-q-limit
          (if global-limit-p
              (to-pop-size (get-array-entry-or-scalar (aref *quarantine-beds-per-capita-vec* +quarantine-global+) *dayix*))
              MOST-POSITIVE-FIXNUM))) ;; should be enough for anyone
    (loop for qix in *quarantine-all-reverse-priority-numeric* do
         (if (aref *quarantine-parm-on-by-quarantine-ix* qix)
             (let* ((global-q-actual (aref *quarantine-beds-tally-count* +occupying-quarantine-global-ix+))
                    (global-overshoot (max 0 (- global-q-actual global-q-limit)))
                    (local-q-actual (aref *quarantine-beds-tally-count* (q2o qix)))
                    (local-q-limit (to-pop-size (get-array-entry-or-scalar (aref *quarantine-beds-per-capita-vec* qix) *dayix*)))
                    (local-overshoot (max 0 (- local-q-actual local-q-limit)))
                    (desired-trim (max local-overshoot global-overshoot))
                    (actual-trim (min desired-trim local-q-actual)))
               (loop repeat actual-trim do (demote-1-from-quarantine qix)))))))

(defun promote-person-to-quarantine (pix qix)
  "take a person alreafy taken from out of the missing queue into quarantine"
  (let ((occupying-flag-ix (aref +quarantine-types-to-occupying-flag-ixs+ qix))
        (missing-out-flag-ix (aref +quarantine-types-to-missing-out-flag-ixs+ qix))
        (missing-out-ix (q2m qix))
        (occupying-ix (q2o qix)))
    (assert (person-flag-is-on pix missing-out-flag-ix))
    ;; Put in missing out bucket and then rebalance
    (decf (aref *quarantine-beds-tally-count* missing-out-ix))
    (incf (aref *quarantine-beds-tally-count* occupying-ix))
    (change-of-flag missing-out-flag-ix pix nil)
    (change-of-flag occupying-flag-ix pix t)
    ;;(funcall (multiple-dll-eps-del *quarantine-linked-lists-mleps*) pix missing-out-ix)
    (funcall (multiple-dll-eps-add *quarantine-linked-lists-mleps*) pix occupying-ix)
    (if (aref +quarantine-fallback-isolation-flags+ qix)
        (maybe-isolate-person pix)) ;; isolate until put in the quarantine
    (quarantine-in-out-action qix pix :in-flag)
    ;;(rebalance-quarantines-by-priority)
    ))

(defun promote-1-to-quarantine (qix)
  (let* ((missing-out-ix (q2m qix))
         (pix (funcall (multiple-dll-eps-pop *quarantine-linked-lists-mleps*) missing-out-ix)))
    (assert pix)
    (promote-person-to-quarantine pix qix)))

(defun try-to-free-quarantine-spaces-from-quarantine (qix-to-cut nbr-to-free)
  (let ((nbr-freed 0))
    (block done1
      (loop repeat nbr-to-free do
           (if (plusp (aref *quarantine-beds-tally-count* (q2o qix-to-cut)))
               (progn
                 (incf nbr-freed)
                 (demote-1-from-quarantine qix-to-cut)
                 ))))
    nbr-freed))

(defun try-to-free-quarantine-spaces (qix nbr-to-free)
  (let ((total-nbr-freed 0))
    (block success1
      (loop for vix in (aref *quarantine-lower-priorities-numeric-vec* qix) do
           (incf total-nbr-freed (try-to-free-quarantine-spaces-from-quarantine qix nbr-to-free))
           (if (>= total-nbr-freed nbr-to-free)
               (return-from success1))))
    total-nbr-freed))

;; In order q priority highest to lowest then unprioritized:
;; - for each q if there are "missing out" in this quarantine
;; - within local and global limits move to "occupying" status
;; If some are remaining,
;; - find a lower priority quarantine to steal from (in q > ) - if none, terminate
;; - steal from it - remove person from the lower priority Q, add 1 person from higher priority Q
;; 
;; N.B. Need to handle isolation for the diagnosed also here TODO!!!!!!! funcall see *quarantine-action-in-out-quarantine-by-quarantine-ix*

(defun rebalance-quarantines-by-priority ()
  (let* ((global-limit-p (parms-quarantine-global-pool-p *parms*))
         (global-q-limit
          (if global-limit-p
              (to-pop-size (get-array-entry-or-scalar (aref *quarantine-beds-per-capita-vec* +quarantine-global+) *dayix*))
              MOST-POSITIVE-FIXNUM))) ;; should be enough for anyone
    (loop for qix in *quarantine-all-normal-priority-numeric* do
         (if (aref *quarantine-parm-on-by-quarantine-ix* qix)
             (let* ((missing-out-ix (q2m qix))
                    (desired-promotions (aref *quarantine-beds-tally-count* missing-out-ix)) ;; number in missing out list
                    (local-q-actual (aref *quarantine-beds-tally-count* (q2o qix)))
                    (local-q-limit (to-pop-size (get-array-entry-or-scalar (aref *quarantine-beds-per-capita-vec* qix) *dayix*)))
                    (local-spare-capacity (max 0 (- local-q-limit local-q-actual)))
                    (local-desired-promotion-count (min local-spare-capacity desired-promotions)))
               (if (plusp local-desired-promotion-count)
                   (let* ((global-q-actual (aref *quarantine-beds-tally-count* +occupying-quarantine-global-ix+))
                          (global-spare-capacity (max 0 (- global-q-limit global-q-actual))))
                     (if (< global-spare-capacity local-spare-capacity)
                         (try-to-free-quarantine-spaces qix (- local-spare-capacity global-spare-capacity)))
                     (let* ((global-q-actual2 (aref *quarantine-beds-tally-count* +occupying-quarantine-global-ix+))
                            (global-spare-capacity2 (max 0 (- global-q-limit global-q-actual2)))
                            (actual-boost (min global-spare-capacity2 local-desired-promotion-count))) 
                       (loop repeat actual-boost do (promote-1-to-quarantine qix))))))))))

(defun maybe-allocate-quarantine-diagnosed-bed (person-ix)
  (if (not (person-is-in-quarantine person-ix))
      (person-direct-into-missing-out person-ix +quarantine-diagnosed+)))

(defun no-event-action (person-ix event new-state)
  person-ix event new-state
  :ok)

(defun can-test-still ()
  (if (< *tests-done-today* (* (parms-pop-size *parms*) (get-array-entry-or-scalar *testing-capacity-per-capita-vec* *dayix*)))
      (progn
        (incf *tests-done-today*)
        *tests-done-today*)
      nil))

(defun unmark-person-for-contact-tracing (person-ix)
  (if (person-contact-tracing-bb-is-on person-ix)
      (progn
        (unset-person-contact-tracing-bb person-ix)
        (unisolate-person person-ix)
        (values :done-unmark))
      (values :not-done-unmark)))

(defun mark-person-for-contact-tracing (person-ix)
  (if (person-contact-tracing-bb-is-off person-ix)
      (progn
        (set-person-contact-tracing-bb person-ix)
        (maybe-isolate-person person-ix)
        (if (parms-test-all-contacts-p *parms*)
            (maybe-test-person person-ix (parms-contact-tracing-test-validity-days-override *parms*)))
        (values :done-mark))
      (values :not-done-mark)))

(defun mark-n-random-persons-for-contact-tracing (nbr)
  ;; if positive then with p((parms-contact-tracing-fraction-contacts-found *parms*)) then carry forward the contact tracing
  (loop repeat nbr do
       (let* ((victim-nbr (random *pop-size*)))
         (mark-person-for-contact-tracing victim-nbr))))

(defun mark-contacts-for-contact-tracing ()
  (let* ((contacts-to-mark-r
          (* (parms-contact-tracing-fraction-contacts-found *parms*)
             (- (parms-number-of-contacts-per-person *parms*) (parms-base-r0 *parms*) 1))) ;; 1 is for the person who infected me
         (contacts-to-mark (real-to-integer-probabilistically contacts-to-mark-r)))
    (mark-n-random-persons-for-contact-tracing contacts-to-mark)))

(defun mark-those-i-infected-maybe (person-ix)
  (let ((iter-fun (funcall (multiple-dll-eps-meta-iter *infection-lists-mleps*) person-ix)))
    (loop for pix = (funcall iter-fun ) then (funcall iter-fun) until (not pix) do
     (if (choose-with-p-q+d (parms-contact-tracing-fraction-contacts-found *parms*))
         (mark-person-for-contact-tracing pix)))))

(defun mark-he-who-infected-me-maybe (person-ix)
  (let ((infected-me-ix (- (person-who-infected-me person-ix) 1)))
    (if (and (not (eql -1 infected-me-ix)) (choose-with-p-q+d (parms-contact-tracing-fraction-contacts-found *parms*)))
        (mark-person-for-contact-tracing infected-me-ix))))

(defun actually-mark-all-contacts (person-ix)
  (mark-contacts-for-contact-tracing) ;; mark some random people as contacts
  (mark-those-i-infected-maybe person-ix) ;; mark those i infected
  (mark-he-who-infected-me-maybe person-ix))

(defun schedule-mark-all-contacts (person-ix)
  "Contact marking now scheduled to avoid excessive recursion"
  (schedule-event-maybe (1+ *dayix*) +event-get-contact-tracing+ person-ix -1))

(fmakunbound 'maybe-test-person) ;; remove fake definition

(defun maybe-test-person (person-ix &optional (test-validity-override nil))
  (let ((unable-to-test-p nil))
    (if (and (person-tested-positive-bb-is-off person-ix)
             (person-antigen-tested-positive-bb-is-off person-ix))
        (if (can-test-still)
            (progn
              (set-person-recently-tested-bb person-ix)
              (schedule-event (+ *dayix* (or test-validity-override (parms-test-validity *parms*))) +event-unrecently-tested+ person-ix -1)
              (if (person-is-infected person-ix)
                  (progn
                    (set-person-tested-positive-bb person-ix)
                    (maybe-allocate-quarantine-diagnosed-bed person-ix)
                    (if (eq (parms-testing-regime *parms*) :contact-tracing)
                        (schedule-mark-all-contacts person-ix)))))
            (setf unable-to-test-p t)))
    unable-to-test-p))

(defun contact-tracing-event-action (person-ix event new-state)
  (with-slots ((e-id id) (e-person-ix person-ix) (e-originating-person-ix originating-person-ix)) event
    (cond
      ;; record who infected them and who they infected, so if they test positive the people they infected can be traced (probably)
      ((eql e-id +event-get-infected+)
       (if (not (person-is-active person-ix)) ;; race condition, two people can decide to infect someone on the same day
           (progn
             (if (not (eql e-originating-person-ix -1))
                 (progn
                   ;; link person-ix into infected list of e-originating-person-ix
                   (funcall (multiple-dll-eps-add *infection-lists-mleps*) person-ix e-originating-person-ix)
                   ;; if the person who infected me was in contact-tracing then put me in contact-tracing with p((parms-contact-tracing-fraction-contacts-found *parms*))
                   (if (and (person-contact-tracing-bb-is-on e-originating-person-ix)
                            (choose-with-p-q+d (parms-contact-tracing-fraction-contacts-found *parms*)))
                       (mark-person-for-contact-tracing person-ix))))
             (setf (person-who-infected-me person-ix) (1+ e-originating-person-ix)))))
      ;;
      ((eql e-id +event-get-contact-tracing+)
       (actually-mark-all-contacts person-ix))
      ;;
      ((or (and (eql e-id +event-get-symptomatic+) ;; if symptoms, & contact tracing this person, then do a test
                (person-contact-tracing-bb-is-on person-ix))
           (eql e-id +event-get-icu+)) ;; always test icu and contact trace accordingly
       (if (can-test-still)
           (schedule-mark-all-contacts person-ix)))
      ;; mark he who infected me
      ((or (eql e-id +event-get-recovered+) (eql e-id +event-get-deceased+) (eql e-id +event-stop-monitoring+))
       (unmark-person-for-contact-tracing person-ix)
       ;; clear the contacts list later if at all
       ;;(loop while (funcall (multiple-dll-eps-pop *infection-lists-mleps*) person-ix) do (progn :empty-loop))
       )
      (t :ok))
    new-state
    :ok))

(defparameter *testing-regime-action-ht*
  (list2ht (list (list nil #'no-event-action)
                 (list :simple-biased #'no-event-action)
                 (list :contact-tracing #'contact-tracing-event-action))
           'eq))

(defparameter *first-time-warning-flag-ht* (make-hash-table :test 'equalp))

(defun process-state-transition (person-ix following-events-maybe)
  (let ((save-allocation-failure (person-icu-bed-allocation-failure-bb-is-on person-ix)))
    (block found-new-event
      (loop for state-table-transition in following-events-maybe do
           (let* ((raw-probability (state-table-transition-probability state-table-transition))
                  (adjusted-probability1
                   (if raw-probability
                       (let ((adj-power (state-table-transition-adjustable-probability-power state-table-transition)))
                         (if adj-power
                             (min 1.0
                                  (let* ((raw-adj-value (age-health-sex-group-fatality-rate-adjuster (aref *age-health-sex-groups-vec* (person-group-nbr person-ix))))
                                         (adj-value (expt raw-adj-value adj-power))
                                         (new-p (* raw-probability adj-value)))
                                    (if (and (> new-p 1.0) (find :probability-warnings (parms-terse *parms*)))
                                        (progn
                                          (if (not (gethash state-table-transition *first-time-warning-flag-ht*))
                                              (progn
                                                (setf (gethash state-table-transition *first-time-warning-flag-ht*) new-p)
                                                (warn "warning invalid new probability state tables wrong or too much adjustment needed~%stte ~S~%raw ~S adj ~S new-p ~S"
                                                      state-table-transition raw-probability adj-value new-p)))))
                                    new-p))
                             raw-probability))))
                  (adjusted-probability
                   (if adjusted-probability1
                       (progn
                         (if (and (state-table-transition-adjustable-probability-no-icu-bed state-table-transition)
                                  save-allocation-failure)
                             (progn
                               (let ((new-prob (min 1.0 (* adjusted-probability1 (parms-death-rate-multiple-needs-icu-no-icu-bed *parms*)))))
                                 new-prob))
                             adjusted-probability1))))
                  (adjusted-probability2
                   (if adjusted-probability
                       (* adjusted-probability1
                          (if (and (parms-variolationp *parms*) (person-deliberately-infected-bb-is-on person-ix))
                              (parms-variolation-severity-factor *parms*)
                              1.0)))))
             (if (or (not adjusted-probability2) (choose-with-p-q+d adjusted-probability2))
                 (progn
                   (schedule-event (+ *dayix* (state-table-transition-days-delay state-table-transition))
                                   (state-table-transition-event state-table-transition)
                                   person-ix)
                   (return-from found-new-event))))))))

(defun maybe-schedule-lost-immunity (person-ix)
  (if (parms-survivor-immunity-limitation-p *parms*)
      (if (choose-with-p-q+d (parms-survivor-immunity-failure-fraction *parms*))
          (schedule-event-maybe (intify (+ *dayix* (get-first-or-scalar *survivor-immunity-failure-days-adj*)))
                                +event-get-lost-immunity+
                                person-ix))))

(defun remove-from-isolations (person-ix)
  (unset-person-in-isolation-bb person-ix)
  (unset-person-in-taiwan-isolation-bb person-ix))

(defun remove-person-from-quarantine-all (person-ix)
  "unmark a person as eligible for a quarantine and if needed take them out of it"
  (block exit-loop
    (loop for qix in +all-normal-quarantines+ do
         (if (and (aref *quarantine-parm-on-by-quarantine-ix* qix)
                  (let ((occupying-flag-ix (aref +quarantine-types-to-occupying-flag-ixs+ qix)))
                    (person-flag-is-on person-ix occupying-flag-ix)))
             (progn
               (demote-person-from-quarantine person-ix qix)
               (return-from exit-loop)))))
  (remove-from-isolations person-ix))

(defun deallocate-quarantine-bed-if-needed-due-positive-antigen (person-ix)
  ;;(if (or (not (quarantine-is-applicable new-state))
    (if (and (parms-antigen-testing-positive-remove-quarantine-p *parms*)
             (person-antigen-tested-positive-bb-is-on person-ix))
        (remove-person-from-quarantine-all person-ix)
        :not-applicable))

;;;INITITALIZATION

(defun compute-infection-probability1 (person-ix outboundp)
  "compute the infection p assuming the other party did nothing special"
  (min
   (if (parms-taiwan-isolation-p *parms*)
       (if (person-in-taiwan-isolation-bb-is-on person-ix)
           (get-array-entry-or-scalar *taiwan-isolation-ineffectiveness-vec* *dayix*)
           1.0)
       1.0)
   (if (person-is-in-quarantine person-ix)
       (parms-quarantine-ineffectiveness *parms*)
       1.0)
   (if (parms-immunizationp *parms*)
       (if (eql (person-state person-ix) +person-state-immunized+)
           (parms-immunization-ineffectiveness *parms*)
           1.0)
       1.0)
   ;; (if (eql (person-state person-ix) +person-state-recovered+)
   ;;     (if (choose-with-p-q+d (parms-recovered-fraction-not-fully-immune *parms*))
   ;;         (parms-recovered-ineffectiveness-when-not-fully-immune *parms*)
   ;;         1.0)
   ;;     1.0)
   (let ((groupn (person-group-nbr person-ix)))
     (cond ((and (parms-social-isolation-a-p *parms*) (eql (aref *social-isolation-groups-vec* groupn) 0))
            (get-array-entry-or-scalar *social-isolation-ineffectiveness-a-vec* *dayix*))
           ((and (parms-social-isolation-b-p *parms*) (eql (aref *social-isolation-groups-vec* groupn) 1))
            (get-array-entry-or-scalar *social-isolation-ineffectiveness-b-vec* *dayix*))
           ((and (parms-social-isolation-c-p *parms*) (eql (aref *social-isolation-groups-vec* groupn) 2))
            (get-array-entry-or-scalar *social-isolation-ineffectiveness-c-vec* *dayix*))
           (t
            1.0)))
   (if (and (parms-mask-distancing-p *parms*)
            (choose-with-p-q+d (get-array-entry-or-scalar *mask-distancing-fraction-compliant-vec* *dayix*)))
       (if outboundp
           (parms-mask-distancing-outbound-ineffectiveness *parms*)
           (parms-mask-distancing-inbound-ineffectiveness *parms*))
       1.0)))

(defun compute-infection-probability (originating-person-ix person-ix)
  "if person is less infectable than originating-person pick the probability accordinly. it is assumed that the safest person dictates the outcome"
  (multiple-value-bind (p-orig)
      (compute-infection-probability1 originating-person-ix :outboundp)
    (multiple-value-bind (p-new)
        (compute-infection-probability1 person-ix (not :outboundp))
      (min p-new p-orig))))

(defun maybe-infect (person-ix originating-person-ix)
  (if (person-is-infectable person-ix)
      (if (choose-with-p-q+d (compute-infection-probability originating-person-ix person-ix))
          (progn
            (schedule-event *dayix* +event-get-infected+ person-ix originating-person-ix)
            (values 1 :infected person-ix))
          (values 0 :not-infected-too-unlikely))
      (values 0 :not-infectable)))

(defun person-init-array (name ival etype test-fun)
  (list 'if `(funcall ,test-fun)
        (list 'if t ;;'(not (eql *pop-size* *new-pop-size*)) ;; always do due to reallocate is faster than reinit
              (list 'setf (person-define-variable-name name)
                    (list 'make-array 'pop-size :element-type etype :initial-element ival))
              :ok-nothing-to-do)
        (list 'setf (person-define-variable-name name)
              (person-define-variable-default-init name))))

(defmacro person-normal-arrays-initm ()
  (append (list 'progn)
           (loop for el in +person-fields-not-flags+ collect ;; +person-fields+
                (destructuring-bind (name ival etype test-fun &rest dont-care)
                    el
                  dont-care
                  (person-init-array name ival etype test-fun)))))

(defun create-person-history-vectors (pop-size)
  (setf *person-state-history* (make-array (list +person-state-count+ pop-size) :element-type 'bit :initial-element 0))
  (setf *person-flag-history* (make-array (list +person-flag-indexes-ix-count+ pop-size) :element-type 'bit :initial-element 0)))

(defun init-person-arrays (pop-size)
  (person-normal-arrays-initm)
  (setf *person-flags* (make-array (list pop-size +person-flag-count+) :element-type 'bit :initial-element 0)))

(defun person-init-vecs (pop-size)
  (if (> pop-size 100) (note :person-init-vecs pop-size))
  (setf *new-pop-size* pop-size)
  (init-person-arrays pop-size)
  (create-person-history-vectors pop-size) ;; faster than reinitialization always to recreate
  (person-init-infection-lists pop-size)
  (person-init-quarantine-linked-lists pop-size)
  (loop for ix from 0 by 1 until (>= ix pop-size) do (set-up-person-specials ix))
  (setf *pop-size* pop-size)
  (if (> pop-size 100) (note :person-init-vecs-end pop-size)))

(defun init-event-days (max-days-to-run)
  (setf *events-by-day*
        (make-array (+ max-days-to-run 1 (max (parms-monitoring-duration *parms*) (parms-test-validity *parms*) (parms-contact-tracing-test-validity-days-override *parms*)))
                    :initial-element nil)))

(defun init-infect-1 (person-ix duplicates-ht)
  (if (gethash person-ix duplicates-ht) (return-from init-infect-1 nil))
  (setf (gethash person-ix duplicates-ht) t)
  (schedule-event 0 +event-get-infected+ person-ix -1)
  person-ix)

(defun init-immunize-1 (person-ix ht)
  (if (gethash person-ix ht) (return-from init-immunize-1 nil))
  (setf (gethash person-ix ht) t)
  (schedule-event 0 +event-get-immunized+ person-ix)
  person-ix)

(defun init-next-states-raw ()
  (setf *next-states-ht* (make-hash-table :test 'equalp))
  (loop for next-states in +next-states-raw+ do
       (let ((key (list (state-table-entry-event next-states) (state-table-entry-prev-state next-states))))
         (assert (not (gethash key *next-states-ht*))) ;; duplicate/overlap
         (setf (gethash key *next-states-ht*) next-states))))

(defparameter *vulnerable-groups-vec* (gensym))

(defun init-vulnerable-groups ()
  (let ((vulnerable-groups-vec (make-array (length +age-health-sex-groups+) :initial-element nil)))
    (loop for el in *quarantine-vulnerable-groups* do
         (setf (aref vulnerable-groups-vec el) t))
    (setf *vulnerable-groups-vec* vulnerable-groups-vec)))

(defun person-is-vulnerable (person-ix)
  (aref *vulnerable-groups-vec* (person-group-nbr person-ix)))

(defun init-infect (pop-size initially-infected-nbr touched-ht)
 (let ((success-count 0)
       (person-count pop-size)
       (try-count 0))
   (loop until (or (>= try-count (* 10 pop-size)) (>= success-count initially-infected-nbr)) do
        (let ((person-ix (random person-count)))
          (if (not (person-is-in-quarantine person-ix))
              (let ((successp (init-infect-1 person-ix touched-ht)))
                (if successp (incf success-count))))
          (incf try-count)))
   (if (< success-count initially-infected-nbr)
       (if (find :initial-infection-failure (parms-terse *parms*))
           (warn "there is no try, just do or not do #1 succ ~s init-infect ~s" success-count initially-infected-nbr)))
   (values :ok success-count)))

(defun init-immunize (pop-size number-to-immunize touched-ht)
  (let ((immunize-try-count 0)
        (immunize-success-count 0))
    (loop until (or (>= immunize-try-count (* pop-size 2)) (>= immunize-success-count number-to-immunize)) do
         (let ((person-ix (random pop-size)))
           (if (not (person-is-in-quarantine person-ix))
               (let ((successp (init-immunize-1 person-ix touched-ht)))
                 (incf immunize-try-count)
                 (if successp
                     (incf immunize-success-count))))))
    (if (< immunize-success-count number-to-immunize)
        (if (find :initial-immunization-failure (parms-terse *parms*))
            (warn "there is no try, just do or not do #2 succ ~s to imminize ~s" immunize-success-count number-to-immunize)))
    (values :ok immunize-success-count)))

(defun make-vulnerable-quarantine-try-fun (touched-ht0 vulnerable-group-lookup0)
  (let ((touched-ht touched-ht0)
        (vulnerable-group-lookup vulnerable-group-lookup0))
    (lambda (person-ix)
      (block exit-fun
        (assert (not (person-in-or-missing-out-quarantine person-ix)))
        (if (not (aref vulnerable-group-lookup (person-group-nbr person-ix))) (return-from exit-fun (values nil :not-vulnerable)))
        (if (gethash person-ix touched-ht) (return-from exit-fun (values nil :already)))
        (setf (gethash person-ix touched-ht) t)
        (person-direct-into-missing-out person-ix +quarantine-vulnerable+) ;;(person-to-quarantine person-ix +quarantine-vulnerable+)
        ))))

(defun init-vulnerable (vulnerable-groups touched-ht)
  "quarantine some of the vulnerable"
  (setf *dayix* 0)
  (if (parms-quarantine-vulnerable-p *parms*)
      (let* ((vulnerable-group-lookup (make-array (length +age-health-sex-groups+) :initial-element nil))
             (number-to-record-for-quarantine (current-quarantine-beds +quarantine-vulnerable+))
             (try-fun (make-vulnerable-quarantine-try-fun touched-ht vulnerable-group-lookup))
             (limit-try-nbr (* number-to-record-for-quarantine 2)))
        (loop for gnbr in vulnerable-groups do (setf (aref vulnerable-group-lookup gnbr) t))
        (multiple-value-bind (successp got-count try-count)
            (scan-people-do-things try-fun number-to-record-for-quarantine limit-try-nbr)
          successp try-count
        (if (< got-count number-to-record-for-quarantine)
            (if (find :quarantine-vulnerable-failure (parms-terse *parms*))
                (warn "there is no try, just do or not do recording for quarantine vulnerable #3 success ~s want ~s" got-count number-to-record-for-quarantine )))
        (values :ok got-count)))
      :not-doing-vulnerable))

(defparameter *touched-ht* nil)

(defun init-pop (pop-size initially-infected-nbr number-to-immunize vulnerable-groups)
  (assert (integerp pop-size))
  (assert (> pop-size 0))
  (assert (integerp initially-infected-nbr))
  (assert (> initially-infected-nbr 0))
  (assert (<= initially-infected-nbr pop-size))
  (assert (<= number-to-immunize pop-size))
  ;;(assert (<= number-to-quarantine pop-size))
  (let ((touched-ht (make-hash-table :test 'eql)))
    (setf *touched-ht* touched-ht)
    (person-init-vecs pop-size)
    (init-vulnerable vulnerable-groups touched-ht)
    (init-infect pop-size initially-infected-nbr touched-ht)
    (init-immunize pop-size number-to-immunize touched-ht))
  (values :ok (aref *quarantine-beds-tally-count* +occupying-quarantine-vulnerable-ix+)))

(defparameter *save-quarantine-beds-per-capita-vecs* :not-init-save-quarantine-beds-per-capita-vecs)
(defparameter *save-icu-beds-for-pandemic-per-capita-vec* :not-init-icu-beds-for-pandemic-per-capita-vec)

(defparameter *qaly-by-group* :not-init-qaly-by-group "by group, qaly remaining")

(defun init-qaly ()
  (using-parms
    (setf *qaly-by-group* (make-array (length *age-health-sex-groups-vec*) :initial-element :not-init-qaly-by-group-element))
    (loop for ix from 0 by 1 for ahsg across *age-health-sex-groups-vec* do
         (with-slots (life-years healthy age-mid) ahsg
           (let ((qaly1 (loop for year from age-mid by 1/10 until (> year (+ age-mid life-years))
                           summing (* 1/10 (expt qaly-year-diff-factor (abs (- year qaly-age-adjustment-base)))))))
             (setf (aref *qaly-by-group* ix) (* 1.0 qaly1 (if healthy 1.0 qaly-not-healthy-adjustment-factor)))))))
  *qaly-by-group*)

(defparameter *prev-r0-distribution-type* (gensym))
(defparameter *prev-daily-r0* (gensym))
(defparameter *prev-nsamples* (gensym))
(defparameter *prev-r0-distribution-cv (gensym))

(defun init-constant-distribution (average cv nsamples)
  nsamples cv
  (values (vectorize (list average)) (vectorize (list 1.0)) 1))

(defparameter *gamma-distribution-1m-samples* :gamma-distribution-1m-samples-not-init)
(defparameter *gamma-distribution-1m-samples-gamma* :gamma-distribution-1m-samples-gamma-not-init)

(defun get-gamma-full-samples (gamma)
  (if (not (eql gamma *gamma-distribution-1m-samples-gamma*))
      (let ((nsamples 1000000)) ;; temp test if < 1M
        (setf *gamma-distribution-1m-samples*
              (vectorize
               (sort (loop for ix from 0 by 1 until (>= ix nsamples) collect (randist::random-gamma gamma 1.0))
                     #'<)))
        (setf *gamma-distribution-1m-samples-gamma* gamma)))
  *gamma-distribution-1m-samples*)

(defun init-gamma-distribution (average cv sample-nbr exponent)
  (let* ((gamma (/ (expt cv 2))) ;; cv = 1/sqrt(gamma)
         (scale (/ average gamma)) ;; gamma = mean
         (full-samples (get-gamma-full-samples gamma))
         (items-per-group (max 1 (intify (floor (/ (length full-samples) sample-nbr)))))
         (samples-list0 (loop for ix from 0 by 1 until (>= ix sample-nbr) collect
                                  (* scale
                                     (/ (loop for ix2 from (* ix items-per-group) by 1 until (>= ix2 (* (1+ ix) items-per-group)) summing (aref full-samples ix2))
                                        items-per-group 1.0))))
         (max-sample (reduce #'max samples-list0))
         (receiving-selection-probabilities0 (mapcar (lambda (val) (expt (/ val max-sample) exponent)) samples-list0))
         (randoms (loop for  ix from 0 by 1 until (>= ix sample-nbr) collect (random 1.0)))
         ;; scramble the lists, consistently between the two. 
         (sorted (sort (mapcar #'list randoms samples-list0 receiving-selection-probabilities0)
                       (lambda (el1 el2) (< (first el1) (first el2)))))
         (samples (vectorize (mapcar #'second sorted)))
         (receiving-selection-probabilities (vectorize (mapcar #'third sorted))))
    (values samples receiving-selection-probabilities sample-nbr max-sample)))

(defparameter *current-distribution-samples* :current-distribution-samples-not-init)
(defparameter *current-distribution-receiving-selection-probabilities* :current-distribution-receiving-selection-probabilities-not-init)
(defparameter *current-distribution-nsamples* :current-distribution-nsamples-not-init)

(defparameter *gamma-sample-sample-nbr-limit* 1000)

(defun init-r0-distribution ()
  (let ((rdt (parms-r0-distribution-type *parms*))
        (sample-nbr (min *gamma-sample-sample-nbr-limit* (parms-pop-size *parms*)))
        (cv (parms-r0-distribution-cv *parms*)))
    (if (not (and (eql *prev-r0-distribution-type* rdt)
                  (eql *prev-daily-r0* *daily-r0*)
                  (eql *prev-nsamples* sample-nbr)
                  (eql *prev-r0-distribution-cv cv)))
        (multiple-value-bind (samples receiving-selection-probabiities nsamples)
          (cond ((eq rdt nil)
                 (init-constant-distribution *daily-r0* cv sample-nbr))
                ((eq rdt :gamma-distribution)
                 (let ((exponent (parms-r0-distribution-exponent-to-determine-receiving-selection-probabilities *parms*)))
                   (init-gamma-distribution *daily-r0* cv sample-nbr exponent)))
                (t (error "invalid r0 distribution type ~S" rdt)))
          (setf *current-distribution-samples* samples)
          (setf *current-distribution-receiving-selection-probabilities* receiving-selection-probabiities)
          (setf *current-distribution-nsamples* nsamples)
          (setf *prev-r0-distribution-type* rdt)
          (setf *prev-daily-r0* *daily-r0*)
          (setf *prev-nsamples* samples)
          (setf *prev-r0-distribution-cv (parms-r0-distribution-cv *parms*))
          :OK)
        :not-needed)))

(defun init-vector-stats ()
    (setf *vec-stats-all* (make-array (length +counter-and-accumulator-names-vec+) :initial-element 0))
    (setf *stats-vec-groups-vecs* (make-array (length +age-health-sex-groups+) :initial-element nil))
    (loop for ix from 0 by 1 until (>= ix (length *stats-vec-groups-vecs*)) do
         (setf (aref *stats-vec-groups-vecs* ix) (make-array (length +counter-and-accumulator-names-vec+) :initial-element 0))))

(defun init-deliberately-infecting-groups ()
    (setf *deliberately-infecting-groups-vec* (make-array (length *age-health-sex-groups-vec*) :initial-element nil))
    (loop for el in (parms-deliberately-infect-groups *parms*) do (setf (aref *deliberately-infecting-groups-vec* el) t)))

(defun init-social-isolation-groups ()
  (setf *social-isolation-groups-vec* (make-array (length *age-health-sex-groups-vec*) :initial-element nil))
  (if (parms-social-isolation-a-p *parms*)
      (loop for el in (parms-social-isolation-a-groups *parms*) do (setf (aref *social-isolation-groups-vec* el) 0)))
  (if (parms-social-isolation-b-p *parms*)
      (loop for el in (parms-social-isolation-b-groups *parms*) do (setf (aref *social-isolation-groups-vec* el) 1)))
  (if (parms-social-isolation-c-p *parms*)
      (loop for el in (parms-social-isolation-c-groups *parms*) do (setf (aref *social-isolation-groups-vec* el) 2)))
  *social-isolation-groups-vec*)

(defun interpolate (goal-val high-val low-val high low)
  (+ low (* (- high low) (/ (- goal-val low-val) (- high-val low-val)))))

(defun goal-seeker (initial-low initial-high goal-value eval-fun max-error max-iterations)
  "goal seeker for monotonic function"
  (let* ((low (* 1.0 initial-low))
         (high (* 1.0 initial-high))
         (high-val (funcall eval-fun high))
         (low-val (funcall eval-fun low))
         (iter-ct 0)
         (mid nil)
         (mid-val nil)
         (last-error nil))
    (setf goal-value (* 1.0 goal-value))
    (if (not (> high low)) (error "high <= low~S" (list low high)))
    (if (= high-val low-val) (error "function appears to be constant ~S" (list low high low-val high-val)))
    (if  (> goal-value (max low-val high-val))
         (progn
           (warn "goal value too high - out of range ~S" (list low high low-val goal-value high-val))
           (goal-seeker initial-low (* initial-high 10) goal-value eval-fun max-error max-iterations))
         (progn
           (if  (< goal-value (min low-val high-val)) (error "goal value too low - out of range ~S" (list low high low-val goal-value high-val)))
           (if (< high-val low-val) (return-from goal-seeker (goal-seeker initial-low initial-high (- goal-value) (lambda (x) (- (funcall eval-fun x))) max-error max-iterations)))
           (block done1
             (loop do
                ;;(setf mid (/ (+ low high) 2))
                  (setf mid (interpolate goal-value high-val low-val high low))
                  (setf mid-val (funcall eval-fun mid))
                  (setf last-error (- mid-val goal-value))
                  ;; (format t "ct ~S low ~S mid ~S high ~S low-val ~S mid-val ~S goal-value ~S high-val ~S last-error ~S~%"
                  ;;         iter-ct low mid high low-val mid-val goal-value high-val last-error)
                  (if (< (abs last-error) max-error)
                      (progn
                        ;;(format t "returning OK last-error ~S goal ~S iters ~S~%" last-error goal-value iter-ct)
                        (return-from done1 (values t mid mid-val iter-ct))))
                  (incf iter-ct)
                  (if (> iter-ct max-iterations)
                      (progn
                        (format t "returning after exhausting ~S iterations~%" iter-ct)
                        (return-from done1 (values nil mid mid-val iter-ct))))
                  (if (< mid-val goal-value)
                      (progn
                        (setf low mid)
                        (setf low-val mid-val))
                      (progn
                        (setf high mid)
                        (setf high-val mid-val)))))))))

(defun test-goal-seeker ()
  (goal-seeker 0.0 5.0 2.0 (lambda (x) (* x x)) 0.001 100))

(defparameter *save-calculated-adjusters* :not-init-save-adjusters)

(defun check-fatality-1 (gnbr)
  (let ((person-ix 0)
        (max-daynbr 100))
    (person-init-vecs 1)
    (setf (person-group-nbr 0) gnbr)
    (init-event-days (1+ max-daynbr))
    (if (not (init-infect-1 person-ix (make-hash-table :test 'eql))) (error "fail to init person ~s" person-ix))
    (let ((deadp
           (block endl1
             (loop for daynbr from 0 by 1 until (> daynbr max-daynbr)
                do
                  (setf *dayix* daynbr)
                  (setf *tests-done-today* 0)
                  (loop while (aref *events-by-day* daynbr) do
                       (let ((event (pop (aref *events-by-day* daynbr))))
                         (assert (eql (event-person-ix event) person-ix))
                         (progn
                           (process-event event)
                           (cond
                             ((eql (person-state person-ix) +person-state-recovered+)
                              (return-from endl1 nil))
                             ((eql (person-state person-ix) +person-state-deceased+)
                              (return-from endl1 t))
                             (t nil))))))
             (error "loop in check-fatality-1 ~s" person-ix))))
      deadp)))

(defun check-fatality-rate (gnbr)
  (init-vector-stats)
  (let ((ct 0)
        (sum 0))
    (loop repeat (parms-fatality-rate-sampling-count *parms*) do
         (let ((fatality (check-fatality-1 gnbr)))
           (incf ct)
           (incf sum (if fatality 1 0))))
    (/ sum (* 1.0 ct))))

(defun compute-fatality-rate-state-event (state-id event-id probability-adjustment-factor)
  (let* ((state-table-entry (or (gethash (list event-id state-id) *next-states-ht*)
                                (gethash (list event-id nil) *next-states-ht*)))
         (new-state (state-table-entry-next-state state-table-entry))
         (following-events-maybe (state-table-entry-transitions state-table-entry)))
    (cond ((= new-state +person-state-deceased+) (return-from compute-fatality-rate-state-event 1.0))
          ((= new-state +person-state-recovered+) (return-from compute-fatality-rate-state-event 0.0)))
    (assert following-events-maybe) ;; final state should be recovered or deceased, covered above
    (min 1.0
         (let ((remaining-probability 1.0))
           (loop for following-transition in following-events-maybe summing
                (let* ((new-event (state-table-transition-event following-transition))
                       (basic-probability-raw (state-table-transition-probability following-transition))
                       (basic-probability (or basic-probability-raw remaining-probability))
                       (adjusted-probability-power (state-table-transition-adjustable-probability-power following-transition))
                       (probability-adjustment
                        (if (not adjusted-probability-power)
                            1.0
                            (progn
                              (expt probability-adjustment-factor adjusted-probability-power))))
                       (probability (min 1.0 (* basic-probability probability-adjustment))))
                  (decf remaining-probability probability)
                  (let* ((lower-res (compute-fatality-rate-state-event new-state new-event probability-adjustment-factor))
                         (res (* probability lower-res)))
                    ;; (format t "bpr ~S bp ~S app ~S pa ~S prob ~S lower-res ~S res ~S~%"
                    ;;         basic-probability-raw basic-probability adjusted-probability-power probability-adjustment probability lower-res res)
                    res)))))))

(defun compute-fatality-rate (gnbr)
  (let* ((group-entry (aref *age-health-sex-groups-vec* gnbr))
         (probability-adjustment-factor (age-health-sex-group-fatality-rate-adjuster group-entry)))
    ;;(format t "group entry ~S adj fact ~S~%" group-entry probability-adjustment-factor)
    (compute-fatality-rate-state-event +person-state-never-infected+ +event-get-infected+ probability-adjustment-factor)))

(defun compute-fatality-rate-adjusters ()
  "compute factors used to adjust state table probabilities so each group has the right fatality rate"
  (let ((adjusters nil)
        (desired-fatality-rate-ht (make-hash-table)))
    (if (find :computing-adjusted-fatality-rates (parms-terse *parms*))
        (format t "computing adjusted daily fatality rates~%"))
    (if (vectorp *testing-capacity-per-capita-vec*)
        (fill-vec *testing-capacity-per-capita-vec* (expt 2 40))
        (setf *testing-capacity-per-capita-vec* (expt 2 40)))
    (loop for gnbr from 0 by 1 until (>= gnbr (length *age-health-sex-groups-vec*)) do
         (let ((group (aref *age-health-sex-groups-vec* gnbr))
               (desired-fatality-rate (age-health-sex-group-fr-death-rate-cv (aref *age-health-sex-groups-vec* gnbr)))
               (eval-fun (lambda (adjr)
                           (setf (age-health-sex-group-fatality-rate-adjuster (aref *age-health-sex-groups-vec* gnbr)) adjr)
                           (note :adjr adjr)
                           (compute-fatality-rate gnbr))))
           (let ((already-done (gethash desired-fatality-rate desired-fatality-rate-ht)))
             (if already-done
                 (progn
                   ;;(format t "Already calculated adjuster for grp ~S desired rate ~S adjuster ~S~%" gnbr desired-fatality-rate already-done)
                   (setf (age-health-sex-group-fatality-rate-adjuster group) already-done))
                 (progn
                   (multiple-value-bind (successp best-adjuster best-value iters)
                       (goal-seeker 0.0 100.0 desired-fatality-rate eval-fun (max (/ desired-fatality-rate 1000000.0) 0.000001) 100)
                     (if (not successp)
                         (warn "failure adjusting fatality rate for group ~S ~S~%"
                               gnbr (list :successp successp :best-adjuster best-adjuster :best-value best-value :iters iters)))
                     (setf (gethash desired-fatality-rate desired-fatality-rate-ht) best-adjuster)
                     (setf (age-health-sex-group-fatality-rate-adjuster group) best-adjuster)))))
           (let ((new-fatality-rate (check-fatality-rate gnbr)))
             new-fatality-rate
             (push (list gnbr (age-health-sex-group-fatality-rate-adjuster (aref *age-health-sex-groups-vec* gnbr)) new-fatality-rate) adjusters)
             (if (find :adjuster-calculation-results (parms-terse *parms*))
                 (format t "Fatality rates desired ~S achieved ~S group ~S adjuster ~S~%"
                         desired-fatality-rate new-fatality-rate gnbr (age-health-sex-group-fatality-rate-adjuster group)))
             )))
    (setf *save-calculated-adjusters* adjusters)
    (values *age-health-sex-groups-vec* adjusters)))

(defun compute-days-infectious-1 (gnbr)
  (setf *tests-done-today* 0)
  (let ((person-ix 0)
        (first-day-infectious nil)
        (last-day-infectious nil)
        (max-daynbr 100))
    (person-init-vecs 1)
    (setf (person-group-nbr 0) gnbr)
    (init-event-days (1+ max-daynbr))
    (if (not (init-infect-1 person-ix (make-hash-table :test 'eql))) (error "fail to init person ~s" person-ix))
    (let ((days-infectious
           (block endl1
             (loop for daynbr from 0 by 1 until (> daynbr max-daynbr) do
                  (setf *dayix* daynbr)
                  (if (person-is-infectious person-ix)
                      (progn
                        (if (not first-day-infectious)
                            (setf first-day-infectious *dayix*))
                        (setf last-day-infectious *dayix*))
                      (progn
                        (if first-day-infectious
                            (return-from endl1 (- last-day-infectious first-day-infectious)))))
                  (loop while (aref *events-by-day* *dayix*) do
                       (let ((event (pop (aref *events-by-day* *dayix*))))
                         (assert (eql (event-person-ix event) person-ix))
                         (progn
                           (process-event event))))
                  (if (person-is-infectious person-ix) ;; check this each day before and after any transition
                      (progn
                        (if (not first-day-infectious)
                            (setf first-day-infectious *dayix*))
                        (setf last-day-infectious *dayix*))
                      (progn
                        (if first-day-infectious
                            (return-from endl1 (- last-day-infectious first-day-infectious))))))
             (error "loop in compute-days-infectious-1 ~s" person-ix))))
          days-infectious)))

(defun compute-days-infectious ()
  (init-vector-stats)
  (if (vectorp *testing-capacity-per-capita-vec*)
      (fill-vec *testing-capacity-per-capita-vec* (expt 2 40))
      (setf *testing-capacity-per-capita-vec* (expt 2 40)))
  (let* ((sum 0)
         (ct 0)
         (rough-nbr 10000)
         (groups (loop for gdata in +age-health-sex-groups+ append
                      (loop repeat (intify (* rough-nbr (fourth gdata))) collect (first gdata)))))
    ;;(format t "groups~%")
    ;;(csv (mapcar #'list groups))
    (loop for gnbr in groups do
         (let ((days-infectious (compute-days-infectious-1 gnbr)))
           (incf ct)
           (incf sum days-infectious)))
    (/ sum (* 1.0 ct))))

(defparameter *prev-age-health-sex-groups-vec* :not-init-prev-age-health-sex-groups-vec)

(defparameter *save-parms* :not-init-save-parms)

(defun init-set-up-temp-parms ()
  "turn off all the things that would mess up calibration of the daily r0 and days infectious"
  (setf *save-parms* *parms*)
  (setf *parms* (copy-parms *parms*))
  (setf (parms-antigen-testing-program-p *parms*) nil)
  (setf (parms-mask-distancing-p *parms*) nil)
  (setf (parms-quarantine-diagnosed-p *parms*) nil)
  (setf (parms-isolation-for-diagnosed-p *parms*) nil)
  (setf (parms-quarantine-vulnerable-p *parms*) nil)
  (setf (parms-social-isolation-a-p *parms*) nil)
  (setf (parms-social-isolation-b-p *parms*) nil)
  (setf (parms-social-isolation-c-p *parms*) nil)
  (setf (parms-taiwan-isolation-p *parms*) nil)
  (setf (parms-testing-regime *parms*) nil)
  (setf (parms-test-all-contacts-p *parms*) nil)
  (setf (parms-deliberately-infect-p *parms*) nil)
  (setf (parms-variolationp *parms*) nil)
  (setf (parms-quarantine-deliberately-infected-p *parms*) nil)
  (setf (parms-survivor-immunity-limitation-p *parms*) nil)
  (setf (parms-exogenously-reintroduce-infection-p *parms*) nil)
  (setf (parms-r0-distribution-type *parms*) nil)
  (setf (parms-quarantine-global-pool-p *parms*) nil))

(defun init-cancel-temp-parms ()
  "restore normal parameters"
  (assert (not (symbolp *save-parms*)))
  (setf *parms* *save-parms*))

(defun init ()
  (setf *q+d-random-0-1* (make-q+d-random-0-1 :size 10000000))
  (using-parms
    (if (and (eql r0-distribution-type :gamma-distribution) (not (zerop (mod pop-size *gamma-sample-sample-nbr-limit*))) (> pop-size *gamma-sample-sample-nbr-limit*))
        (warn "population size > ~S and not multiple of ~S - gamma distribution simulation may be somwwhat inaccurate" pop-size pop-size))
    (setf *quarantine-vulnerable-groups* quarantine-vulnerable-groups)
    (setf *save-quarantine-beds-per-capita-vecs* (copy-seq *quarantine-beds-per-capita-vec*))
    (setf *save-icu-beds-for-pandemic-per-capita-vec* *icu-beds-for-pandemic-per-capita-vec*)
    (if (vectorp *icu-beds-for-pandemic-per-capita-vec*)
        (fill-vec *icu-beds-for-pandemic-per-capita-vec* (expt 2 40))
        (setf *icu-beds-for-pandemic-per-capita-vec* (expt 2 40)))
    (if quarantine-diagnosed-p
        (if (vectorp (aref *quarantine-beds-per-capita-vec* +quarantine-diagnosed+))
            (fill-vec (aref *quarantine-beds-per-capita-vec* +quarantine-diagnosed+) (expt 2 40))
            (setf (aref *quarantine-beds-per-capita-vec* +quarantine-diagnosed+) (expt 2 40))))
    (if quarantine-deliberately-infected-p
        (if (vectorp (aref *quarantine-beds-per-capita-vec* +quarantine-deliberately-infected+))
            (fill-vec (aref *quarantine-beds-per-capita-vec* +quarantine-deliberately-infected+) (expt 2 40))
            (setf (aref *quarantine-beds-per-capita-vec* +quarantine-deliberately-infected+) (expt 2 40))))
    (if quarantine-vulnerable-p
        (if (vectorp (aref *quarantine-beds-per-capita-vec* +quarantine-vulnerable+))
            (fill-vec (aref *quarantine-beds-per-capita-vec* +quarantine-vulnerable+) (expt 2 40))
            (setf (aref *quarantine-beds-per-capita-vec* +quarantine-vulnerable+) (expt 2 40))))
    (if quarantine-global-pool-p
        (if (vectorp (aref *quarantine-beds-per-capita-vec* +quarantine-global+))
            (fill-vec (aref *quarantine-beds-per-capita-vec* +quarantine-global+) (expt 2 40))
            (setf (aref *quarantine-beds-per-capita-vec* +quarantine-global+) (expt 2 40))))
    (init-vector-stats)
    (init-next-states-raw)
    (if (not (equalp *prev-age-health-sex-groups-vec* *age-health-sex-groups-vec*))
        (progn
          (init-set-up-temp-parms)
          (setf *avg-days-infectious*
                (compute-days-infectious))
          (compute-fatality-rate-adjusters)
          (init-cancel-temp-parms)
          (setf *prev-age-health-sex-groups-vec* (copy-seq *age-health-sex-groups-vec*))))
    (init-vulnerable-groups)
    (init-vector-stats)
    (setf *daily-r0* (/ base-r0 *avg-days-infectious*))
    (init-r0-distribution)
    (init-event-days max-days-to-run)
    (init-qaly)
    (setf *icu-beds-used* 0)
    (setf *quarantine-failure-count* 0)
    (setf *icu-failure-count* 0)
    (setf *quarantine-diagnosed-failure-count* 0)
    (setf *exogenously-reintroduce-infection-count* 0)
    (setf *exogenously-reintroduce-infection-failure-count* 0)
    (setf *quarantine-deliberately-infected-failure-count* 0)
    (setf *daily-stats* nil)
    (setf *deliberately-infected-tried-count* 0)
    (init-social-isolation-groups)
    (init-deliberately-infecting-groups)
    (setf *deliberately-infect-count* 0)
    (setf *deliberately-infect-desired-number* (intify (* pop-size deliberately-infect-fraction)))
    (zero-vec *quarantine-beds-tally-count*)
    (setf *quarantine-beds-per-capita-vec* (copy-seq *save-quarantine-beds-per-capita-vecs*))
    (setf *icu-beds-for-pandemic-per-capita-vec* *save-icu-beds-for-pandemic-per-capita-vec*)
    (init-pop pop-size initially-infected-nbr (intify (* immunize-initial-fraction pop-size)) quarantine-vulnerable-groups)
    (values :ok)))

(defun maybe-fill-quarantine-diagnosed-bed (person-ix)
  (if (and (person-is-infected person-ix)
           (not (person-is-in-quarantine person-ix))
           (not (quarantine-excluded-antigen person-ix)))
      (person-direct-into-missing-out person-ix +quarantine-diagnosed+)))

;;;MAIN CODE

(defun find-likely-victim ()
  "find a victim for possible infection, probability possibly weighted by the extroversion factor - see :r0-distribution-type :r0-distribution-cv :r0-distribution-exponent-to-determine-receiving-selection-probabilities"
  (let ((count 0))
    (block found1
      (loop repeat 10000 do
           (incf count)
           (let* ((victim-nbr (random *pop-size*))
                  (receiving-ix (mod victim-nbr *current-distribution-nsamples*))
                  (victim-receiving-infectiousness-probability (aref *current-distribution-receiving-selection-probabilities* receiving-ix)))
             (if (choose-with-p-q+d victim-receiving-infectiousness-probability)
                 (return-from found1 (values victim-nbr count)))))
      (error "cannot find victim, count ~S" count))))

(defun process-contagion (person-ix)
  "person is contagious - sneeze on people"
  ;; how many people?
  (let* ((icount 0)
         (scan-count 0)
         (contagion-ix (mod person-ix *current-distribution-nsamples*))
         (person-contagiousness-daily-r0 (aref *current-distribution-samples* contagion-ix)))
    (loop repeat (real-to-integer-probabilistically person-contagiousness-daily-r0) do
         (multiple-value-bind (victim-nbr scan-count1)
             (find-likely-victim)
           (incf scan-count scan-count1)
           (incf icount (maybe-infect victim-nbr person-ix))))
    icount))

(defun run-day-contagion-person (person-ix)
  (if (person-is-infectious person-ix)
      (process-contagion person-ix)))

(defun run-day-contagion ()
  (loop for person-ix from 0 by 1 until (>= person-ix *pop-size*) do
       (run-day-contagion-person person-ix)))

(defun run-day-events ()
  ;;(format t "events for today ~s~%~s~%" *dayix* (aref *events-by-day* *dayix*))
  (loop while (aref *events-by-day* *dayix*) do
       ;;(format t "events left for today ~s~%~s~%" *dayix* (aref *events-by-day* *dayix*))
       (let ((event (pop (aref *events-by-day* *dayix*))))
         (progn
           (process-event event))))
  *dayix*)

(defun run-simple-biased-daily-tests ()
  (let ((person-ix (random *pop-size*)))
    (block exitl1
      (loop repeat *pop-size* do
           (setf person-ix (mod (1+ person-ix) *pop-size*))
           (if (person-tested-positive-bb-is-off person-ix)
               (if (or (person-is-infected person-ix) (choose-with-p-q+d (/ (parms-testing-bias-infected-people *parms*))))
                   (if (maybe-test-person person-ix) ;; if out of capacity
                       (return-from exitl1 nil))))))
    :ok))

(defun run-contact-tracing-daily-tests ()
  "test anyone in contact tracing who randomly shows symptoms, or is symptomatic from the disease"
  (let ((person-ix (random *pop-size*)))
    (block exitl2
      (loop repeat *pop-size* do
           (setf person-ix (mod (1+ person-ix) *pop-size*))
           ;; only test contact tracing people
           (if (person-contact-tracing-bb-is-on person-ix)
               (progn
                 ;; if person is infectious process accordingly
                 (if (or (eql +person-state-icu-needed+ (person-state person-ix))
                         (eql +person-state-symptomatic+ (person-state person-ix))
                         (choose-with-p-q+d (parms-not-cv-symptomatic-fraction *parms*)))
                         (if (maybe-test-person person-ix) ;; if out of capacity
                             (return-from exitl2 nil))))))
      :ok)))

(defun maybe-test-symptomatic ()
  "set who is symptomatic that we pick up for use with the idea of isolation of the symptomatic, treat accordingly"
  (if (and (parms-test-symptomatic-p *parms*) (plusp (parms-disclose-symptomatic-fraction *parms*)))
      (block exitl3
        (loop for person-ix from 0 by 1 until (>= person-ix  *pop-size*) do
             (let ((has-symptoms-p nil))
               (if (eql +person-state-symptomatic+ (person-state person-ix))
                   (setf has-symptoms-p t)
                   (if  (choose-with-p-q+d (parms-not-cv-symptomatic-fraction *parms*))
                        (setf has-symptoms-p t)))
               (if has-symptoms-p
                   (if (choose-with-p-q+d (parms-disclose-symptomatic-fraction *parms*))
                       (progn
                         (if (maybe-test-person person-ix)
                             (return-from exitl3 nil)))))))))
  :ok)

(defun taiwan-try-isolation-fun (person-ix)
  (if (and (not (person-is-in-isolation person-ix))
           (not (person-is-in-quarantine person-ix)))
      (if (or (not (parms-antigen-testing-positive-remove-isolations-p *parms*))
              (person-antigen-tested-negative-bb-is-off person-ix))
          (taiwan-isolate-person person-ix))))

(defun maybe-taiwan-isolation ()
  "taiwan isolation is not dependent on people volunteering. we are talking about temperature/symptom testing on entrance to shops etc, and exclusion if sympatomatic or feverish"
  (if (parms-taiwan-isolation-p *parms*)
      (let ((nbr-to-try (intify (* *pop-size* (parms-not-cv-symptomatic-fraction *parms*)))))
        (scan-people-do-things  #'taiwan-try-isolation-fun nbr-to-try nbr-to-try) ;; this is OK we only scan the number needed.
        :done)
      :not-done))

(defun antigen-positive (person-ix)
  (let ((result-positive
         (if (person-has-been-ever-infected-bb-is-on person-ix)
             (if (choose-with-p-q+d (parms-antigen-false-negative-rate *parms*))
                 t
                 nil)
             (if (choose-with-p-q+d (parms-antigen-false-positive-rate *parms*))
                 nil
                 t))))
    (if result-positive
        (set-person-antigen-tested-positive-bb person-ix)
        (set-person-antigen-tested-negative-bb person-ix))
    result-positive))

(defun try-test-for-antigens (person-ix)
  (if (or (person-antigen-tested-positive-bb-is-on person-ix)
          (aref +active-state-flags+ (person-state person-ix))
          (not (antigen-positive person-ix)))
      (progn
        nil)
      (progn
        (if (parms-antigen-testing-positive-remove-quarantine-p *parms*)
            (deallocate-quarantine-bed-if-needed-due-positive-antigen person-ix))
        (if (parms-antigen-testing-positive-remove-isolations-p *parms*)
            (remove-from-isolations person-ix)))))

(defun maybe-test-for-antigens ()
  (if (parms-antigen-testing-program-p *parms*)
      (progn
        (let ((number-to-test (to-pop-size (get-array-entry-or-scalar *antigen-testing-rate-vec* *dayix*))))
        (multiple-value-bind (donep got-count try-count)
            (scan-people-do-things #'try-test-for-antigens number-to-test number-to-test) ;; OK we will not test every # we want to, like in life
          (values (if donep :OK :fail) got-count try-count))))))

(defun run-tests ()
  (cond
    ((not (parms-testing-regime *parms*)) t)
    ((eq (parms-testing-regime *parms*) :simple-biased)
     (run-simple-biased-daily-tests))
    ((eq (parms-testing-regime *parms*) :contact-tracing)
     (run-contact-tracing-daily-tests)) ;;(run-contact-tracing-daily-tests *dayix*)
    (t (error "invalid testing regime ~s not nil or :simple-biased or :contact-tracing" (parms-testing-regime *parms*))))
  (maybe-test-symptomatic)
  (maybe-test-for-antigens))

(defun run-daily-immunize ()
  (let ((dup-ht (make-hash-table)))
    (using-parms
      (if immunizationp
          (let ((rate-today (get-array-entry-or-scalar *immunize-rate-vec* *dayix*)))
            (if (plusp rate-today)
                (let ((number-to-immunize (intify (* pop-size rate-today)))
                      (number-immunized 0)
                      (number-tries 0)
                      (next-person-ix (random pop-size)))
                  (block done1
                    (loop do
                         (if (> number-tries pop-size)
                             (return-from done1 (values nil :ran-out-of-tries number-tries number-immunized pop-size)))
                         (if (> number-immunized number-to-immunize)
                             (return-from done1 (values t :immunized-all-i-wanted-to number-tries number-immunized pop-size)))
                         (if (and (not (person-is-active next-person-ix))
                                  (not (eql (person-state next-person-ix) +person-state-immunized+))
                                  (not (gethash next-person-ix dup-ht)))
                             (progn
                               (schedule-event *dayix* +event-get-immunized+ next-person-ix)
                               (setf (gethash next-person-ix dup-ht) t)
                               (incf number-immunized)))
                         (incf number-tries)
                         (setf next-person-ix (mod (1+ next-person-ix) pop-size)))))))))))

(defun maybe-quarantine-deliberately-infected (person-ix)
  (if (parms-quarantine-deliberately-infected-p *parms*)
      (progn
        (if (person-occupying-quarantine-deliberately-infected-bb-is-off person-ix)
            (progn
              (if (and (not (person-is-in-quarantine person-ix))
                       (= (person-state person-ix) +person-state-immunized+))
                  (progn
                    (if (not (person-in-or-missing-out-quarantine person-ix)) ;; something may have happened in the meantime so just leave it alone in that case
                        (person-direct-into-missing-out person-ix +quarantine-deliberately-infected+)))))))))

(defun maybe-deliberately-infect (person-ix)
  (if (and (person-is-infectable person-ix)
           (or (not (parms-antigen-testing-positive-remove-deliberately-infect-p *parms*))
               (person-antigen-tested-positive-bb-is-off person-ix))
           (aref *deliberately-infecting-groups-vec* (person-group-nbr person-ix)))
      (progn
        (schedule-event *dayix* +event-get-infected-deliberately+ person-ix -1)
        (set-person-deliberately-infected-bb person-ix)
        (values t :infected person-ix))
      (values nil :already-infected-or-not-infectable)))

(defun try-deliberately-infect (person-ix)
  (let ((ok1 (maybe-deliberately-infect person-ix)))
    ok1))

(defun max-infectable-today ()
  (if (parms-quarantine-deliberately-infected-required-to-infect-p *parms*)
      (return-from max-infectable-today MOST-POSITIVE-FIXNUM))
  (let* ((free-global-capacity
          (if (not (parms-quarantine-global-pool-p *parms*))
              MOST-POSITIVE-FIXNUM
              (- (to-pop-size (get-array-entry-or-scalar (aref *quarantine-beds-per-capita-vec* +quarantine-global+) *dayix*))
                 (aref *quarantine-beds-tally-count* +occupying-quarantine-global-ix+))))
         (freeable-capacity-lower-priorities
          (loop for lql in (aref *quarantine-lower-priorities-numeric-vec* +quarantine-deliberately-infected+)
             summing
               (let ((ocupying-ix-l (q2o lql)))
                 (aref *quarantine-beds-tally-count* ocupying-ix-l))))
         (latent-demand-higher-priorities
          (loop for lqh in (aref *quarantine-higher-priorities-numeric-vec* +quarantine-deliberately-infected+)
             summing
               (let ((missing-ix-l (q2m lqh)))
                 (aref *quarantine-beds-tally-count* missing-ix-l))))
         (avaiilable-global-capacity
          (+ free-global-capacity freeable-capacity-lower-priorities (- latent-demand-higher-priorities))))
    (min avaiilable-global-capacity
        (- (to-pop-size (get-array-entry-or-scalar (aref *quarantine-beds-per-capita-vec* +quarantine-deliberately-infected+) *dayix*))
           (aref *quarantine-beds-tally-count* (q2o +quarantine-deliberately-infected+))))))

(defun run-deliberately-infect ()
  (using-parms
    (if (and (parms-deliberately-infect-p *parms*) (< *deliberately-infect-count* *deliberately-infect-desired-number*))
        (let ((max-days-infect (get-first-or-scalar *deliberately-infect-max-days-adj*)))
          (if (>= *dayix* max-days-infect)
              (return-from run-deliberately-infect (values nil *dayix* max-days-infect)))
          (let ((nbr-want-today (max 0
                                     (min (max-infectable-today)
                                          (current-deliberately-infect-number)
                                          (- *deliberately-infect-desired-number* *deliberately-infect-count*)))))
            (if (plusp nbr-want-today)
                (multiple-value-bind (donep got-count try-count)
                    (scan-people-do-things  #'try-deliberately-infect nbr-want-today nbr-want-today) ;; We try to infect but not all will succeed do not scan whole population
                  (incf *deliberately-infect-count* got-count)
                  (incf *deliberately-infected-tried-count* try-count)
                  (values (if donep :OK :fail) *deliberately-infected-tried-count* got-count *deliberately-infect-count*))
                nil)))))
  nil)

(defun run-exogenously-reinfect ()
  (let ((flag (parms-exogenously-reintroduce-infection-p *parms*)))
    (if flag
        (progn
          (let* ((sirr (current-spontaneous-infection-rate))
                 (lt (choose-with-p-q+d sirr)))
            (if lt
                (let ((first-person-ix (random (parms-pop-size *parms*))))
                  (block done1
                    (loop for person-ix = first-person-ix then (mod (1+ person-ix) (parms-pop-size *parms*))
                       repeat (parms-pop-size *parms*) do
                         (if (person-is-infectable person-ix)
                             (progn
                               (set-person-exogenously-infected-bb person-ix)
                               (incf *exogenously-reintroduce-infection-count*)
                               (schedule-event *dayix* +event-get-infected-exogenously+ person-ix -1)
                               (return-from done1 (values t person-ix)))))
                    (incf *exogenously-reintroduce-infection-failure-count*)
                    (values nil)))))
          :nothing-done)
        :parm-off)))

(defun run-day (daynbr)
  (progn
    (note :active (aref *vec-stats-all* +ix-active-count+) :daynbr daynbr)
    (setf *dayix* daynbr)
    (if (zerop (mod daynbr 10))
        (progn
          ;;(mem)
          (gc :full t) ;; sbcl dan blow up unless you gc fairly aggressively
          ;;(mem)
          ))
     (setf *tests-done-today* 0)
     (daily-trim-quarantines)
     (run-daily-immunize)
     (run-deliberately-infect)
     (run-exogenously-reinfect)
     (run-day-events)
     (run-tests)
     (maybe-taiwan-isolation)
     (rebalance-quarantines-by-priority)
     (run-day-contagion)
     (run-day-events) ;; now run the contagion events at end of day
     (rebalance-quarantines-by-priority)
     (assert (not (aref *events-by-day* *dayix*)))))

;;;STATISTICS

;; which counters to report, exclude, synonyms, order

(defparameter *counters-accumulators-synonyms-ht*
  (list2ht *counters-accumulators-synonyms* 'equalp))

(defun sort-prioritize-select-counter-and-accumulator-names ()
  (let* ((default-seq (- MOST-POSITIVE-FIXNUM 1000000))
         (selected
          (remove-if
           (lambda (el)
             (let ((fnd (find (symbol-name (second el)) *counters-accumulators-to-exclude* :test 'equalp)))
               ;;(if fnd (format t "..dbg2356 found? ~S item ~S (~S) from~%~S~%" fnd el (symbol-name el) *counters-accumulators-to-exclude*))
               fnd))
           (loop for ix8 from 0 by 1 for el in +counter-and-accumulator-names+ collect (list ix8 el))))
         (prep-for-sort (mapcar (lambda (el)
                                  (let* ((posn (position (symbol-name (second el)) *counters-accumulators-sort-order* :test #'equalp))
                                         (loc (or posn default-seq)))
                                    ;;(format t "..dbg2462 el ~S posn ~S loc ~S~%" el posn loc)
                                    (incf default-seq)
                                    (list loc el)))
                                 selected))
         (sorted (progn (sort (copy-list prep-for-sort) (lambda (el1 el2) (< (first el1) (first el2)))))))
    (values (mapcar #'second sorted) default-seq)))

(defparameter *sorted-prioritized-selective-counter-and-accumulator-names*
  (sort-prioritize-select-counter-and-accumulator-names))

(defun collect-daily-stats ()
  ;; Calculations by group
  (let* ((scaling-output (parms-scale-output-costs-and-dayix *parms*))
         (scaled-dayix (if scaling-output (* *dayix* *dayix-adjustment-factor-out*) *dayix*))
         (scaling-factor-costs (if scaling-output *dayix-adjustment-factor-out* 1.0))
         (not-scaling-factor-costs 1.0)) ;; some things do not need to be scaled up e.g. cost of deaths. What does is things that depend on the duration of the pandemic
  (loop for group-ix from 0 by 1
     for group-stats-vec across *stats-vec-groups-vecs* do
       (setf (aref group-stats-vec +ix-last-dayix+) scaled-dayix)
       (setf (aref group-stats-vec +ix-active-count+)
             (+ (aref group-stats-vec +ix-count-current-person-state-infected+)
                (aref group-stats-vec +ix-count-current-person-state-shedding+)
                (aref group-stats-vec +ix-count-current-person-state-symptomatic+)
                (aref group-stats-vec +ix-count-current-person-state-icu-needed+)
                (aref group-stats-vec +ix-count-current-person-state-still-shedding+)))
       (setf (aref group-stats-vec +ix-alive-count+)
             (- (loop for ix from 0 by 1 until (>= ix +person-state-count+) summing
                     (progn
                       ;;(format t "..dbg2517 ix ~S (+ +states-count-base+ (* +unit-size+ ix) +current-offset+) ~S~%" ix (+ +states-count-base+ (* +unit-size+ ix) +current-offset+))
                       (aref group-stats-vec (aref +states-to-counters-current+ ix))))
                (aref group-stats-vec +ix-count-current-person-state-deceased+)))
     ;; "IX-POPULATION"
       (let ((population
              (+ (aref group-stats-vec +ix-alive-count+)
                 (aref group-stats-vec +ix-count-current-person-state-deceased+))))
         (setf (aref group-stats-vec +ix-population+) population))
       (setf (aref group-stats-vec +ix-infectious-count+)
             (+ (aref group-stats-vec +ix-count-current-person-state-shedding+)
                (aref group-stats-vec +ix-count-current-person-state-symptomatic+)
                (aref group-stats-vec +ix-count-current-person-state-icu-needed+)
                (aref group-stats-vec +ix-count-current-person-state-still-shedding+)))
       (incf (aref group-stats-vec +ix-icu-needed-days+)
             (aref group-stats-vec +ix-count-current-person-state-icu-needed+))
       (incf (aref group-stats-vec +ix-icu-days+)
             (aref group-stats-vec +ix-count-ever-person-icu-bed-allocated-bb+))
       (incf (aref group-stats-vec +ix-mask-days+)
             (* (aref group-stats-vec +ix-alive-count+)
                (if (parms-mask-distancing-p *parms*) (get-array-entry-or-scalar *mask-distancing-fraction-compliant-vec* *dayix*) 0)))
       (let* ((quarantine-current
               (+ (aref group-stats-vec +ix-count-current-person-occupying-quarantine-diagnosed-bb+)
                  (aref group-stats-vec +ix-count-current-person-occupying-quarantine-deliberately-infected-bb+)
                  (aref group-stats-vec +ix-count-current-person-occupying-quarantine-vulnerable-bb+))))
         (incf (aref group-stats-vec +ix-quarantine-effectiveness-days+)
               (* quarantine-current (- 1.0 (parms-quarantine-ineffectiveness *parms*))))
         (incf (aref group-stats-vec +ix-recuperating-days+)
               (aref group-stats-vec +ix-count-current-person-recuperating-bb+))
         (cond ((and (parms-social-isolation-a-p *parms*) (eql (aref *social-isolation-groups-vec* group-ix) 0))
                (incf (aref group-stats-vec +ix-social-isolation-a-effectiveness-days+)
                      (* (max 0 (- (aref group-stats-vec +ix-alive-count+) (aref group-stats-vec +ix-active-count+) quarantine-current))
                         (- 1.0 (get-array-entry-or-scalar *social-isolation-ineffectiveness-a-vec* *dayix*)))))
               ((and (parms-social-isolation-b-p *parms*) (eql (aref *social-isolation-groups-vec* group-ix) 1))
                (incf (aref group-stats-vec +ix-social-isolation-b-effectiveness-days+)
                      (* (max 0 (- (aref group-stats-vec +ix-alive-count+) (aref group-stats-vec +ix-active-count+) quarantine-current))
                         (- 1.0 (get-array-entry-or-scalar *social-isolation-ineffectiveness-b-vec* *dayix*)))))
               ((and (parms-social-isolation-c-p *parms*) (eql (aref *social-isolation-groups-vec* group-ix) 2))
                (incf (aref group-stats-vec +ix-social-isolation-c-effectiveness-days+)
                      (* (max 0 (- (aref group-stats-vec +ix-alive-count+) (aref group-stats-vec +ix-active-count+) quarantine-current))
                         (- 1.0 (get-array-entry-or-scalar *social-isolation-ineffectiveness-c-vec* *dayix*)))))
               (t nil)))
       (setf (aref group-stats-vec +ix-social-isolation-effectiveness-days+)
             (+ (aref group-stats-vec +ix-social-isolation-a-effectiveness-days+)
                (aref group-stats-vec +ix-social-isolation-b-effectiveness-days+)
                (aref group-stats-vec +ix-social-isolation-c-effectiveness-days+)))
       (incf (aref group-stats-vec +ix-symptomatic-days+)
             (aref group-stats-vec +ix-count-current-person-state-symptomatic+))
       (incf (aref group-stats-vec +ix-taiwan-isolation-effectiveness-days+)
             (aref group-stats-vec +ix-count-current-person-in-taiwan-isolation-bb+))
     ;; "IX-LIFE-YEARS-LOST"
       (let ((life-years-lost
              (* (aref group-stats-vec +ix-count-current-person-state-deceased+)
                 (age-health-sex-group-life-years (aref *age-health-sex-groups-vec* group-ix)))))
         (setf (aref group-stats-vec +ix-life-years-lost+) life-years-lost))
     ;; "IX-QALY-LOST"
       (let* ((qaly-lost-per-deceased-person (aref *qaly-by-group* group-ix))
              (qaly-lost-per-recovered-person
               (* (- 1.0  (parms-qaly-fraction-retain-full-quality *parms*))
                  (- 1.0 (parms-qaly-fraction-retained-when-not-fully-retained-after-recovery *parms*))
                  qaly-lost-per-deceased-person)))
         ;;(format t "..dbg2554 qaly-lost-per-recovered-person ~S per deceased person ~S~%" qaly-lost-per-reovered-person qaly-lost-per-deceased-person)
         (setf (aref group-stats-vec +ix-qaly-lost-death+)
               (* (aref group-stats-vec +ix-count-ever-person-state-deceased+) qaly-lost-per-deceased-person))
         (setf (aref group-stats-vec +ix-qaly-lost-disability+)
               (* (aref group-stats-vec +ix-count-ever-person-state-recovered+) qaly-lost-per-recovered-person))
         (setf (aref group-stats-vec +ix-qaly-lost-total+)
               (+
                (* (aref group-stats-vec +ix-count-ever-person-state-recovered+) qaly-lost-per-recovered-person)
                (* (aref group-stats-vec +ix-count-ever-person-state-deceased+) qaly-lost-per-deceased-person)))
         ;; Economic costs
         (setf (aref group-stats-vec +ix-qaly-lost-total-economic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-qaly-lost-total+) (parms-economic-value-qaly-dollars *parms*)))
         (setf (aref group-stats-vec +ix-icu-days-economic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-icu-days+) (parms-economic-cost-icu-day *parms*)))
         (setf (aref group-stats-vec +ix-mask-days-economic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-mask-days+) (parms-economic-cost-mask-day *parms*)))
         (setf (aref group-stats-vec +ix-quarantine-effectiveness-days-economic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-quarantine-effectiveness-days+) (parms-economic-cost-quarantine-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-recuperating-days-economic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-recuperating-days+) (parms-economic-cost-recuperating-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-a-effectiveness-days-economic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-social-isolation-a-effectiveness-days+) (parms-economic-cost-social-isolation-a-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-b-effectiveness-days-economic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-social-isolation-b-effectiveness-days+) (parms-economic-cost-social-isolation-b-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-c-effectiveness-days-economic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-social-isolation-c-effectiveness-days+) (parms-economic-cost-social-isolation-c-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-effectiveness-days-economic-cost+)
             (+ (aref group-stats-vec +ix-social-isolation-a-effectiveness-days-economic-cost+)
                (aref group-stats-vec +ix-social-isolation-b-effectiveness-days-economic-cost+)
                (aref group-stats-vec +ix-social-isolation-c-effectiveness-days-economic-cost+)))
         (setf (aref group-stats-vec +ix-symptomatic-days-economic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-symptomatic-days+) (parms-economic-cost-symptomatic-day *parms*)))
         (setf (aref group-stats-vec +ix-taiwan-isolation-effectiveness-days-economic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-taiwan-isolation-effectiveness-days+) (parms-economic-cost-taiwan-isolation-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-economic-cost+)
               (+ (aref group-stats-vec +ix-qaly-lost-total-economic-cost+)
                  (aref group-stats-vec +ix-icu-days-economic-cost+)
                  (aref group-stats-vec +ix-mask-days-economic-cost+)
                  (aref group-stats-vec +ix-quarantine-effectiveness-days-economic-cost+)
                  (aref group-stats-vec +ix-recuperating-days-economic-cost+)
                  (aref group-stats-vec +ix-social-isolation-effectiveness-days-economic-cost+)
                  (aref group-stats-vec +ix-symptomatic-days-economic-cost+)
                  (aref group-stats-vec +ix-taiwan-isolation-effectiveness-days-economic-cost+)))
         ;; Hedonic costs
         (setf (aref group-stats-vec +ix-qaly-lost-total-hedonic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-qaly-lost-total+) (parms-hedonic-value-qaly-dollars *parms*)))
         (setf (aref group-stats-vec +ix-icu-needed-days-hedonic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-icu-needed-days+) (parms-hedonic-cost-icu-needed-day *parms*)))
         (setf (aref group-stats-vec +ix-mask-days-hedonic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-mask-days+) (parms-hedonic-cost-mask-day *parms*)))
         (setf (aref group-stats-vec +ix-quarantine-effectiveness-days-hedonic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-quarantine-effectiveness-days+) (parms-hedonic-cost-quarantine-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-recuperating-days-hedonic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-recuperating-days+) (parms-hedonic-cost-recuperating-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-a-effectiveness-days-hedonic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-social-isolation-a-effectiveness-days+) (parms-hedonic-cost-social-isolation-a-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-b-effectiveness-days-hedonic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-social-isolation-b-effectiveness-days+) (parms-hedonic-cost-social-isolation-b-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-c-effectiveness-days-hedonic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-social-isolation-c-effectiveness-days+) (parms-hedonic-cost-social-isolation-c-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-social-isolation-effectiveness-days-hedonic-cost+)
             (+ (aref group-stats-vec +ix-social-isolation-a-effectiveness-days-hedonic-cost+)
                (aref group-stats-vec +ix-social-isolation-b-effectiveness-days-hedonic-cost+)
                (aref group-stats-vec +ix-social-isolation-c-effectiveness-days-hedonic-cost+)))
         (setf (aref group-stats-vec +ix-symptomatic-days-hedonic-cost+)
               (* not-scaling-factor-costs (aref group-stats-vec +ix-symptomatic-days+) (parms-hedonic-cost-symptomatic-day *parms*)))
         (setf (aref group-stats-vec +ix-taiwan-isolation-effectiveness-days-hedonic-cost+)
               (* scaling-factor-costs (aref group-stats-vec +ix-taiwan-isolation-effectiveness-days+) (parms-hedonic-cost-taiwan-isolation-effectiveness-day *parms*)))
         (setf (aref group-stats-vec +ix-hedonic-cost+)
               (+ (aref group-stats-vec +ix-qaly-lost-total-hedonic-cost+)
                  (aref group-stats-vec +ix-icu-needed-days-hedonic-cost+)
                  (aref group-stats-vec +ix-mask-days-hedonic-cost+)
                  (aref group-stats-vec +ix-quarantine-effectiveness-days-hedonic-cost+)
                  (aref group-stats-vec +ix-recuperating-days-hedonic-cost+)
                  (aref group-stats-vec +ix-social-isolation-effectiveness-days-hedonic-cost+)
                  (aref group-stats-vec +ix-symptomatic-days-hedonic-cost+)
                  (aref group-stats-vec +ix-taiwan-isolation-effectiveness-days-hedonic-cost+)))
         ;; Total Cost
         (setf (aref group-stats-vec +ix-total-cost+) (+ (aref group-stats-vec +ix-hedonic-cost+) (aref group-stats-vec +ix-economic-cost+)))))
  ;; Accumulate to global
  (zero-vec *vec-stats-all*)
  (loop for group-ix from 0 by 1
     for group-stats-vec across *stats-vec-groups-vecs* do
       (loop for field-ix from 0 by 1 until (>= field-ix (length *vec-stats-all*)) do
            (incf (aref *vec-stats-all* field-ix) (aref group-stats-vec field-ix))))
  (setf (aref *vec-stats-all* +ix-last-dayix+) scaled-dayix)
  (setf (aref *vec-stats-all* +ix-dayix-adjustment-factor+) *dayix-adjustment-factor-out*)
  (setf (aref *vec-stats-all* +ix-rate-adjustment-factor+) *rate-adjustment-factor-out*)
  (push (list :day scaled-dayix :all (listify *vec-stats-all*) :groups (loop for gv across *stats-vec-groups-vecs* collect (listify gv)) ) *daily-stats*)
  (if (find :daily-all-stats (parms-terse *parms*))
      (format t "day ~S scaled ~S all stats~%~S~%" *dayix* scaled-dayix (mapcar #'list +counter-and-accumulator-names+ (listify *vec-stats-all*))))
  (values *dayix* scaled-dayix)))

(defun get-parm-names ()
  (mapcar #'caar +fields-in-parms+))

(defun get-parm-names-formatted ()
  (format t "~{~A~}" (get-parm-names)))

(defun get-parm-values (&optional (parms nil))
  (loop for el in +fields-in-parms+ collect
       (let* ((field-name (caar el))
              (field-print-fun (second el))
              (getter-fun (gethash field-name *getter-ht*)))
         (funcall field-print-fun (funcall getter-fun parms)))))

(defun get-counter-values (vec)
  (mapcar (lambda (ix) (aref vec ix))
          (mapcar #'first *sorted-prioritized-selective-counter-and-accumulator-names*)))
  
(defun get-counter-names ()
  (mapcar #'second *sorted-prioritized-selective-counter-and-accumulator-names*))

(defun get-counter-names-pretty ()
  (mapcar (lambda (name)
            (let ((new-name (gethash name *counters-accumulators-synonyms-ht*)))
              ;;(format t "..dbg2618 name ~S type-of name ~S new-name ~S~%" name (type-of name) new-name)
              (or new-name name)))
          (get-counter-names)))

(defun get-counter-indexes ()
  (mapcar #'first *sorted-prioritized-selective-counter-and-accumulator-names*))

(defun get-nondefault-parms ()
  (let* ((names (get-parm-names))
         (default-values (get-parm-values (make-parms)))
         (used-values (get-parm-values))
         (combined (mapcar #'list names default-values used-values)))
    (remove-if (lambda (el) (equalp (second el) (third el))) combined)))

(defun get-nondefault-parm-names ()
  (mapcar #'first (get-nondefault-parms)))

(defun summary-stats ()
  (using-parms
    ;;(let ((counters-list (mapcar #'first (vec-ixs-members *vec-stats-all*))))
    (when headings
      (format t "~Afinal state totals,sex,agelow,agehigh,agemid,healthy,overridden,~{~a,~}~{~a,~}~%"
              (parms-heading-prefix *parms*) (get-counter-names-pretty) (get-parm-names)))
    (when (find :summary terse)
      ;;(let ((global-ctrs (loop for ctr in counters-list collect (aref *vec-stats-all* ctr))))
      (if (find :nondefault-parms-detailed terse)
          (let ((ndp (get-nondefault-parms)))
            (format t "***,Nondefault-parameters~%***,Parameter,default,actual,~%~{~{***,~A,~S,~S~%~}~}~&" ndp)))
      (format t "~Aglobal,,,,,,~A,~{~a,~}"
              (parms-summary-line-prefix *parms*)
              (flm (get-nondefault-parm-names) "/")
              (get-counter-values *vec-stats-all*))
      (format t "~{~a,~}~%" (get-parm-values)))
    (if (find :easy-to-read-output (parms-terse *parms*))
        (progn
          (loop for (nm val) in
               (mapcar #'list
                       (append (get-counter-names-pretty) (get-parm-names))
                       (append (get-counter-values *vec-stats-all*) (get-parm-values))) do
               (format t "~a=~a," nm val))
          (format t "~%")))
    (if (find :group-stats terse)
        (loop for gix from 0 by 1 for gv across *stats-vec-groups-vecs* for grp across *age-health-sex-groups-vec* do
             (let ((group-ctrs (get-counter-values gv)))
               (if (plusp (reduce #'max group-ctrs))
                   (progn
                     (format t "~A~A,~A,~A,~A,~A,~A,~A,~{~A,~}~%"
                             (parms-summary-line-prefix *parms*)
                             gix (age-health-sex-group-sex grp) (age-health-sex-group-age-first grp) (age-health-sex-group-age-last grp)
                             (age-health-sex-group-age-mid grp) (age-health-sex-group-healthy grp)
                             (flm (get-nondefault-parm-names) "=")
                             group-ctrs))))))))

(defun filter-all-ctrs-list (list)
  (let* ((vec (vectorize list))
         (selected-ctrs (loop for ix in (get-counter-indexes) collect (aref vec ix))))
    selected-ctrs))

(defun running-stats (&key (groupsp nil))
  (using-parms
    (if (find :running-stats terse)
        (progn
          (format t "Running stats, days ~S~%" (length *daily-stats*))
          (format t "Running stats~%Day,[group],~{~A,~}~%" (get-counter-names-pretty))
          (loop for daily-stat in (reverse *daily-stats*) do
                 (destructuring-bind (day-lit daynbr all-lit all-ctrs groups-lit groups-ctrs) daily-stat
                   (assert (eq day-lit :day)) (assert (eq all-lit :all)) (assert (eq groups-lit :groups))
                   (format t "~S,All," daynbr)
                   (format t "~{~A,~}~%" (filter-all-ctrs-list all-ctrs))
                   (when groupsp
                     (loop for gix from 0 by 1 for gl in groups-ctrs do
                          (when (plusp (reduce #'max gl))
                            (format t "~S,~S," daynbr gix)
                            (format t "~{~A,~}~%" (filter-all-ctrs-list gl)))))))))
    :OK))

;;;CORE INITIAL PARAMETER VALIDATION AND SETUP

(defun validate-and-store-adjustable-parameters (field-name global-variable-or-aref daysp ratesp dayix-adjustment-factor rate-adjustment-factor)
  ;;(format t ".dbg1847 fn ~S gv ~S type ~S~%" field-name global-variable (type-of global-variable))
  (using-parms
    (let* ((getter-fun (gethash field-name *getter-ht*))
           (check1 (assert getter-fun))
           (field-contents (funcall getter-fun))
           (val (if (listp field-contents) (make-array max-days-to-run :initial-element 0.0) 0.0))
           (prev-dayn nil)
           (prev-valx 0.0)
           )
      check1
      ;;(format t ".dbg1867 ~{~S~%~}~%" (list getter-fun check1 field-contents val prev-dayn prev-valx dayix-adjustment-factor rate-adjustment-factor))
      (if (listp field-contents)
          (progn
            (loop for el in field-contents do
                 ;;(format t ".dbg1871 el ~S~%" el)
                 (if (not (listp el)) (error "not list ~S in ~S" el field-contents))
                 (if (not (= (length el) 2)) (error "not length 2 ~S in ~S" el field-contents))
                 (destructuring-bind (dayn0 val0) el
                   (if (not (and (integerp dayn0) (not-minusp dayn0))) (error "invalid dayn ~S in ~S in ~S" dayn0 el field-contents))
                   (let ((dayn (if daysp (intify (* dayn0 dayix-adjustment-factor)) dayn0))
                         (valx (if ratesp (* val0 rate-adjustment-factor) val0)))
                     (if prev-dayn
                         (progn
                           (if (< dayn prev-dayn) (error "invalid dayn sequence ~S prev ~S in ~S in ~S" dayn prev-dayn el field-contents))
                           (loop for ix2 from prev-dayn by 1 until (or (>= ix2 dayn) (>= ix2 max-days-to-run)) do
                                (setf (aref val ix2) prev-valx))))
                     (setf prev-dayn dayn)
                     (setf prev-valx valx))))
            (loop for ix3 from (or prev-dayn 0) by 1 until (>= ix3 max-days-to-run) do (setf (aref val ix3) prev-valx)))
          (progn
            (let ((field-contents-adjusted
                   (if ratesp
                       (* field-contents rate-adjustment-factor)
                       (if daysp
                           (* field-contents dayix-adjustment-factor)
                           field-contents))))
              (setf val field-contents-adjusted))))
      ;;(format t ".dbg adjusted field-name ~S from ~S to ~S~%" field-name field-contents val)
      (if (listp global-variable-or-aref)
          (progn
            (assert (and (eq 'aref (first global-variable-or-aref)) (eql (length global-variable-or-aref) 3)))
            (setf (aref (symbol-value (second global-variable-or-aref)) (symbol-value (third global-variable-or-aref))) val))
          (progn
            (setf (symbol-value global-variable-or-aref) val))))))

(defun make-adjustments-for-size ()
  (using-parms
    (let* ((dayix-adjustment-factor-in
            (if scale-to-pop-size
                (/ (+ adjust-days-a (* adjust-days-b (log pop-size)))
                   (+ adjust-days-a (* adjust-days-b (log adjust-days-canonical-population-size))))
                1.0))
           (rate-adjustment-factor-in (/ dayix-adjustment-factor-in)))
      (setf *dayix-adjustment-factor-out* rate-adjustment-factor-in) ;; correct that these are swapped (result scaling is inverse to paramater scaling)
      (setf *rate-adjustment-factor-out* dayix-adjustment-factor-in)
      ;; (if scale-to-pop-size
      (loop for (field-name global-variable ratesp daysp) in +adjustment-list+ do
           (validate-and-store-adjustable-parameters field-name global-variable daysp ratesp dayix-adjustment-factor-in rate-adjustment-factor-in)))))

(defun validate-tweak-parms ()
  (using-parms
    (assert (and (numberp pop-size) (plusp pop-size) (< pop-size 327000001)))
    (assert (and (numberp initially-infected-nbr) (not (minusp initially-infected-nbr)) (<= initially-infected-nbr pop-size)))
    (assert (and (numberp max-days-to-run) (not (minusp max-days-to-run))))
    (assert (and (numberp immunize-initial-fraction) (>= immunize-initial-fraction 0) (<= immunize-initial-fraction 1)))
    (assert (and (numberp fatality-rate-sampling-count)
                 (integerp fatality-rate-sampling-count) (plusp fatality-rate-sampling-count) (< fatality-rate-sampling-count 10000001)))
    (assert (and (numberp base-r0) (plusp base-r0)))
    (assert (and (numberp icu-beds-per-capita-spoken-for) (plusp icu-beds-per-capita-spoken-for) (<= icu-beds-per-capita-spoken-for 1.0)))
    (assert (and (numberp death-rate-multiple-needs-icu-no-icu-bed) (plusp death-rate-multiple-needs-icu-no-icu-bed) (<= death-rate-multiple-needs-icu-no-icu-bed 3.999)))
    (assert (and (numberp testing-bias-infected-people) (not (minusp testing-bias-infected-people)) (<= testing-bias-infected-people 10.0)))
    (assert (and (numberp number-of-contacts-per-person) (not (minusp number-of-contacts-per-person)) (<= number-of-contacts-per-person 1000)))
    (assert (and (numberp monitoring-duration) (not (minusp monitoring-duration)) (<= monitoring-duration 1000)))
    (assert (and (numberp min-days-to-run-base) (not (minusp min-days-to-run-base))))
    ;;(assert (and (numberp terse) (not (minusp terse)) (<= terse 4)))
    (assert (or (not test-all-contacts-p) (eq testing-regime :contact-tracing)))
    (assert (and (numberp test-validity) (not (minusp test-validity)) (<= test-validity 1000)))
    (assert (and (numberp contact-tracing-test-validity-days-override)
                 (not (minusp contact-tracing-test-validity-days-override)) (<= contact-tracing-test-validity-days-override 1000)))
    (assert (and (integerp isolation-duration) (>= isolation-duration 1) (<= isolation-duration 999)))
    (assert (and (integerp taiwan-isolation-duration) (>= taiwan-isolation-duration 1) (<= taiwan-isolation-duration 999)))
    (assert (listp deliberately-infect-groups))
    (loop for el in deliberately-infect-groups do
         (assert (and (numberp el) (integerp el) (not (minusp el))
                      (< el (length +age-health-sex-groups+)))))
    ;;
    (setf (aref *quarantine-parm-on-by-quarantine-ix* +quarantine-deliberately-infected+) quarantine-deliberately-infected-p)
    (setf (aref *quarantine-parm-on-by-quarantine-ix* +quarantine-vulnerable+) quarantine-vulnerable-p)
    (setf (aref *quarantine-parm-on-by-quarantine-ix* +quarantine-diagnosed+) quarantine-diagnosed-p)
    (setf (aref *quarantine-parm-on-by-quarantine-ix* +quarantine-global+) quarantine-global-pool-p)
    ;;
    (make-adjustments-for-size)
    :OK))

;;CORE MAIN CODE

(defun clear-contact-tracing (person-ix)
  "This is needed because I support only one contact tracing list out of a given person so if a person is reinfected you would get an assertion failure. Such lists are likely stale in any case."
  ;;(format t "dbg3200 clear-contact-tracing~%~S~%" (display-person person-ix))
  ;; (if (not (zerop (person-who-infected-me person-ix)))
  ;;     (format t "dbg3200 clear-contact-tracing~%~S~%" (display-person (- (person-who-infected-me person-ix) 1))))
  (if (eq (parms-testing-regime *parms*) :contact-tracing)
      (let* ((infected-me-ix-1 (- (person-who-infected-me person-ix) 1)))
        (if (not (minusp infected-me-ix-1))
            (progn
              ;; remove from list of peers
              (funcall (multiple-dll-eps-del *infection-lists-mleps*) person-ix infected-me-ix-1)
              ;; clear person-who-infected-me
              (setf (person-who-infected-me person-ix) 0)
              )))
      ;; clear list of everyone I infected, and reset person-who-infected-me for them
      (loop while (funcall (multiple-dll-eps-pop *infection-lists-mleps*) person-ix) do (progn))))

(defun process-event (event)
  "handle event in the course of the disease, set up next event if any"
  (let* ((event-id (event-id event))
         (person-ix (event-person-ix event))
         ;;(person-details (display-person person-ix))
         (person-state (person-state person-ix))
         (event-result (gethash (list event-id person-state) *next-states-ht*)))
    (if (eql event-id +event-get-lost-immunity+)
        (progn
          (clear-contact-tracing person-ix)
          (set-person-lost-immunity-bb person-ix)
          (unset-person-tested-positive-bb person-ix)
          (unset-person-recently-tested-bb person-ix)
          (unset-person-deliberately-infected-bb person-ix)
          (unset-person-exogenously-infected-bb person-ix)))
    (if (not event-result) (setf event-result (gethash (list event-id nil) *next-states-ht*)))
    (if (not event-result) (error "dayix ~s invalid event~%~s~%~s for state of person ix ~s ~s" *dayix* event event-id person-ix (display-person person-ix)))
    (let ((new-state (state-table-entry-next-state event-result))
          (following-events-maybe (state-table-entry-transitions event-result)))
      (if (and (not (eql (person-state person-ix) +person-state-infected+))
               (eql new-state +person-state-infected+)
               (person-has-been-ever-infected-bb-is-off person-ix))
          (progn
            (set-person-has-been-recently-infected-bb person-ix)
            (set-person-has-been-ever-infected-bb person-ix)))
      (when new-state
        (deallocate-icu-bed-if-needed person-ix new-state)
        (deallocate-quarantine-bed-if-needed-due-positive-antigen person-ix))
      (cond ((eql event-id +event-unrecently-tested+)
             (unset-person-recently-tested-bb person-ix))
            ((eql event-id +event-stop-isolating+)
             (unset-person-in-isolation-bb person-ix))
            ((eql event-id +event-stop-taiwan-isolating+)
             (unset-person-in-taiwan-isolation-bb person-ix))
            ((eql event-id +event-get-symptomatic+)
             (if (parms-taiwan-isolation-p *parms*)
                 (set-person-in-taiwan-isolation-bb person-ix)))
            ((eql event-id +event-get-deceased+)
             (unset-person-recently-deceased-bb person-ix)
             (unset-person-has-been-recently-infected-bb person-ix))
             ;;(format t "..dbg2146 ~S set recently-deceased / unset recently-infected~%~S~%" *dayix* (display-person person-ix)))
            ((eql event-id +event-get-recovered+)
             (unset-person-lost-immunity-bb person-ix)
             (let ((rp (cond ((eql (person-state person-ix) +person-state-icu-needed+) (parms-recuperation-period-icu *parms*))
                             ((eql (person-state person-ix) +person-state-symptomatic+) (parms-recuperation-period-symptomatic *parms*))
                             (t nil))))
               (if rp
                   (progn
                     (set-person-recuperating-bb person-ix)
                     (schedule-event (+ *dayix* rp) +event-get-fully-recuperated+ person-ix)))))
            ((eql event-id +event-get-fully-recuperated+)
             (unset-person-recuperating-bb person-ix))
            ((eql event-id +event-get-infected-deliberately+)
             (maybe-quarantine-deliberately-infected person-ix)))
      (funcall (gethash (parms-testing-regime *parms*) *testing-regime-action-ht*
                        (lambda (a b c d) (error "~s not found in *testing-regime-action-ht* for dayix ~s p ~s e ~s ns ~s" (parms-testing-regime *parms*) a b c d)))
               person-ix event new-state)
      (if (not new-state) ;; ie ignore this event for this state
          (progn
            (return-from process-event nil)))
      (if (and (eql person-state +person-state-symptomatic+) (not (eql new-state +person-state-symptomatic+)))
          (if (parms-taiwan-isolation-p *parms*)
              (unset-person-in-taiwan-isolation-bb person-ix)))
      (record-exit-old-state (person-state person-ix) person-ix)
      (record-entry-new-state new-state person-ix)
      (setf (person-state person-ix) new-state)
      (if (eql new-state +person-state-icu-needed+)
          (allocate-icu-bed person-ix))
      (if (eql new-state +person-state-recovered+)
          (maybe-schedule-lost-immunity person-ix))
      (process-state-transition person-ix following-events-maybe))
    ;;(display-person person-ix)
    person-ix))

(defun run-sim (parms)
  (setf *parms* parms)
  (validate-tweak-parms)
  (set-up-quarantine-priorities)
  (let ((last-dayix (parms-max-days-to-run parms)))
    (init)
    (block its-all-over
      (loop for daynbr from 0 by 1 until (>= daynbr (parms-max-days-to-run parms)) do
           (setf *dayix* daynbr)
           (let ((count-active (aref *vec-stats-all* +ix-active-count+)))
             (if (and (zerop count-active) (> daynbr 1)
                      (> daynbr (get-first-or-scalar *min-days-to-run-adj*)))
                 (progn
                   (if (find :report-why-end (parms-terse *parms*))
                       (format t "No active cases, past min days to run, stopping on day ~S min days ~S~%" daynbr *min-days-to-run-adj*))
                   (setf last-dayix daynbr)
                   (return-from its-all-over nil))))
           ;;(if (zerop (mod daynbr 10)) (gc :full t))
           (run-day daynbr)
           (collect-daily-stats)))
    (setf *last-dayix* last-dayix)
    (setf *dayix* last-dayix)
    (rebalance-quarantines-by-priority)
    (collect-daily-stats))
  (summary-stats)
  (running-stats :groupsp nil)
  :OK)

;;; Calibration

(defun rwo2 (first-hdr overrides)
  "run with default parms overrides for internal use"
  (let ((parms (apply #'make-parms (append (list :headings first-hdr)  overrides))))
    (run-sim parms)
    (setf first-hdr nil)))

(defun rr-scan-pop-sizes () ;; for calibration of scaling
  (let ((first-time-p t))
    (loop for pop-size = 3270 then (* pop-size 10) until (> pop-size 32700) do
         (rwo2 first-time-p
               (list
                :pop-size pop-size :terse '(:summary)
                :heading-prefix "Pop-size,"
                :summary-line-prefix (format nil "~A," pop-size)
                :scale-to-pop-size nil
                :scale-output-costs-and-dayix nil))
         (setf first-time-p nil))))

;; Callable routines from REPL interactive

;;sample: (t rwo :pop-size 327000 :terse '(:summary :group-stats) :heading-prefix "special test," :summary-line-prefix "Special Summary,")

(defun doc ()
  (format t "<HTML><HEAD><TITLE>parameter documentation for (rwo)</TITLE><HEAD>")
  (let ((name+doc (mapcar (lambda (el) (list (caar el) (cadar el) (sixth el))) +fields-in-parms+)))
    (loop for el in name+doc do
         (destructuring-bind (name default doc) el
           (assert (symbolp name))
           (assert (stringp doc))
           (if (eql (aref doc 0) #\|)
               (progn
                 (let ((nxt (position #\| doc :start 1)))
                   (if (not nxt) (warn "invalid documentation ~S missing second |" doc))
                   (let ((main-doc (subseq doc 1 nxt)))
                     (format t main-doc)
                     (setf doc (subseq doc (1+ nxt)))))))
           (format t "<H2>")
           (format t "~A" (string-downcase (symbol-name name)))
           (format t "</H2>")
           (format t doc)
           (format t "~%<br><br>Default<br>~%~S<br>~%" default)
           (format t "~%"))))
  (format t "</HTML>")
  :OK)

(defun rwo (first-hdr overrides)
  "Run with Default parms Overrides"
  (let ((parms (apply #'make-parms (append (list :headings first-hdr)  overrides))))
    (run-sim parms)
    (setf first-hdr nil)))

(defun temp-test1 ()
  (let ((firstp t))
    (loop for parms in (list (list :terse '(:summary :running-stats  :group-stats) :pop-size 327000 :heading-prefix "Test Type," :summary-line-prefix "Lockdown," :social-isolation-p t))
       do
         (rwo firstp parms)
         (setf firstp nil))))

(defparameter *test-overrides*
  (let ((pop 327000))
    (list
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Vanilla,")
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Contact Tracing," :testing-regime :contact-tracing :test-all-contacts-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Low Tech (Taiwan) Testing," :taiwan-isolation-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Masks," :mask-distancing-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Low Tech (Taiwan) testing and Masks," :taiwan-isolation-p t :mask-distancing-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Immunize Now," :immunizationp t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Immunize in 90 days," :immunizationp t :immunize-rate-fv '((0 0.00) (90 0.02)))
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown," :social-isolation-a-p t  :social-isolation-b-p t :social-isolation-c-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown Old/infirm," :social-isolation-c-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Isolate diagnosed," :isolation-for-diagnosed-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Quarantine diagnosed," :quarantine-diagnosed-p t :isolation-for-diagnosed-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Quarantine vulnerable," :quarantine-vulnerable-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Hyper-spreaders," :r0-distribution-type :gamma-distribution
           :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect," :deliberately-infect-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect isolate old/infirm," :deliberately-infect-p t :social-isolation-c-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "DIV Deliberately Infect Variolate," :deliberately-infect-p t :variolationp t :social-isolation-c-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect incl Young Prot Old/V,"
           :deliberately-infect-p t :variolationp t
           :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
           :quarantine-vulnerable-p t
           :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+)))
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "DIVVPMV Deliberately Infect Variolate Vulnerablish Protect Most Vulnerable,"
           :deliberately-infect-p t :variolationp t
           :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (> (third el) 0.005) (< (third el) 0.20))) +age-health-sex-groups+))
           :quarantine-vulnerable-p t
           :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.20)) +age-health-sex-groups+))
            :social-isolation-c-p t)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "BQ Deliberately Infect incl Young Prot Old/V Big Q,"
           :deliberately-infect-p t :variolationp t
           :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
           :quarantine-vulnerable-p t
           :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+))
           :quarantine-deliberately-infected-beds-per-capita-fv 0.5
           :quarantine-vulnerable-beds-per-capita-fv 0.5
           :social-isolation-c-p t)
     (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Antigen Testing," :antigen-testing-program-p t)
     )))

(defun run-set ()
  (let ((firstp t))
  (loop for parms in *test-overrides* do
       (rwo firstp  parms)
       (setf firstp nil))))

(defun temp-run-set-3 ()
  (let ((firstp t)
        (pop 327000))
    (loop for parms in
         (list
          (list :terse '(:summary :running-stats :group-stats) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Vanilla," )
          (list :terse '(:summary :running-stats :group-stats) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown,"
                :social-isolation-a-p t :social-isolation-b-p t :social-isolation-c-p t)
          ;;(list :terse '(:summary :group-stats :running-stats) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown Old/infirm," :social-isolation-c-p t)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type,"
          ;;       :summary-line-prefix "DLH DIVVPMVH Deliberately Infect Variolate Vulnerablish Protect Most Vulnerable Hyper-spreaders,"
          ;;       :deliberately-infect-p t :variolationp t
          ;;       :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (> (third el) 0.005) (< (third el) 0.20))) +age-health-sex-groups+))
          ;;       :quarantine-vulnerable-p t
          ;;       :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.20)) +age-health-sex-groups+))
          ;;       :social-isolation-c-p t
          ;;       :r0-distribution-type :gamma-distribution
          ;;       :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del BQH Deliberately Infect incl Young Prot Old/V Big Q Hyper,"
          ;;       :deliberately-infect-p t :variolationp t
          ;;       :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
          ;;       :quarantine-vulnerable-p t
          ;;       :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+))
          ;;       :quarantine-deliberately-infected-beds-per-capita-fv 0.5
          ;;       :quarantine-vulnerable-beds-per-capita-fv 0.5
          ;;       :social-isolation-c-p t
          ;;       :r0-distribution-type :gamma-distribution
          ;;       :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Hyper-spreaders," :r0-distribution-type :gamma-distribution
          ;;       :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect," :deliberately-infect-p t)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect isolate old/infirm,"
          ;;       :deliberately-infect-p t :social-isolation-c-p t)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del DIV Deliberately Infect Variolate,"
          ;;       :deliberately-infect-p t :variolationp t :social-isolation-c-p t)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect incl Young Prot Old/V,"
          ;;       :deliberately-infect-p t :variolationp t
          ;;       :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
          ;;       :quarantine-vulnerable-p t
          ;;            :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+)))
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del DIVVPMV Deliberately Infect Variolate Vulnerablish Protect Most Vulnerable,"
          ;;       :deliberately-infect-p t :variolationp t
          ;;       :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (> (third el) 0.005) (< (third el) 0.20))) +age-health-sex-groups+))
          ;;       :quarantine-vulnerable-p t
          ;;       :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.20)) +age-health-sex-groups+))
          ;;       :social-isolation-c-p t)
          ;; (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del BQ Deliberately Infect incl Young Prot Old/V Big Q,"
          ;;       :deliberately-infect-p t :variolationp t
          ;;       :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
          ;;       :quarantine-vulnerable-p t
          ;;       :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+))
          ;;       :quarantine-deliberately-infected-beds-per-capita-fv 0.5
          ;;       :quarantine-vulnerable-beds-per-capita-fv 0.5
          ;;       :social-isolation-c-p t)
          )
       do
       (rwo firstp parms)
       (setf firstp nil))))

(defun temp-run-set-2 ()
  (let ((firstp t)
        (pop 327000))
    (loop for parms in
         (list
          ;; (list :terse '(:summary :running-stats :group-stats) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown,"
          ;;       :social-isolation-a-p t :social-isolation-b-p t :social-isolation-c-p t)
          ;;(list :terse '(:summary :group-stats :running-stats) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown Old/infirm," :social-isolation-c-p t)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Vanilla," )
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type,"
                :summary-line-prefix "DLH DIVVPMVH Deliberately Infect Variolate Vulnerablish Protect Most Vulnerable Hyper-spreaders,"
                :deliberately-infect-p t :variolationp t
                :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (> (third el) 0.005) (< (third el) 0.20))) +age-health-sex-groups+))
                :quarantine-vulnerable-p t
                :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.20)) +age-health-sex-groups+))
                :social-isolation-c-p t
                :r0-distribution-type :gamma-distribution
                :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del BQH Deliberately Infect incl Young Prot Old/V Big Q Hyper,"
                :deliberately-infect-p t :variolationp t
                :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
                :quarantine-vulnerable-p t
                :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+))
                :quarantine-deliberately-infected-beds-per-capita-fv 0.5
                :quarantine-vulnerable-beds-per-capita-fv 0.5
                :social-isolation-c-p t
                :r0-distribution-type :gamma-distribution
                :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Hyper-spreaders," :r0-distribution-type :gamma-distribution
                :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect," :deliberately-infect-p t)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect isolate old/infirm,"
                :deliberately-infect-p t :social-isolation-c-p t)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del DIV Deliberately Infect Variolate,"
                :deliberately-infect-p t :variolationp t :social-isolation-c-p t)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect incl Young Prot Old/V,"
                :deliberately-infect-p t :variolationp t
                :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
                :quarantine-vulnerable-p t
                     :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+)))
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del DIVVPMV Deliberately Infect Variolate Vulnerablish Protect Most Vulnerable,"
                :deliberately-infect-p t :variolationp t
                :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (> (third el) 0.005) (< (third el) 0.20))) +age-health-sex-groups+))
                :quarantine-vulnerable-p t
                :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.20)) +age-health-sex-groups+))
                :social-isolation-c-p t)
          (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del BQ Deliberately Infect incl Young Prot Old/V Big Q,"
                :deliberately-infect-p t :variolationp t
                :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
                :quarantine-vulnerable-p t
                :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+))
                :quarantine-deliberately-infected-beds-per-capita-fv 0.5
                :quarantine-vulnerable-beds-per-capita-fv 0.5
                :social-isolation-c-p t)
          )
       do
       (rwo firstp parms)
       (setf firstp nil))))

(defparameter *temp-overrides*
  (let ((donep nil)
        (limit 327000))
    (loop for pop = limit then (min limit (* pop 2)) until donep append
         (loop repeat 1 append
              (progn
              (if (>= pop limit) (setf donep t))
              (list
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Contact Tracing and lose immunity,"
                     :testing-regime :contact-tracing :test-all-contacts-p t
                     :survivor-immunity-limitation-p t
                     :survivor-immunity-failure-fraction 0.2
                     :survivor-immunity-failure-days-base 20)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Vanilla,")
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Contact Tracing," :testing-regime :contact-tracing :test-all-contacts-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Low Tech (Taiwan) Testing," :taiwan-isolation-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Masks," :mask-distancing-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Low Tech (Taiwan) testing and Masks," :taiwan-isolation-p t :mask-distancing-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Immunize Now," :immunizationp t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Immunize in 90 days," :immunizationp t :immunize-rate-fv '((0 0.00) (90 0.02)))
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown," :social-isolation-a-p t :social-isolation-b-p t :social-isolation-c-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Lockdown Old/infirm," :social-isolation-c-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Isolate diagnosed," :isolation-for-diagnosed-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Quarantine diagnosed," :quarantine-diagnosed-p t :isolation-for-diagnosed-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Quarantine vulnerable," :quarantine-vulnerable-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Hyper-spreaders," :r0-distribution-type :gamma-distribution
                     :r0-distribution-exponent-to-determine-receiving-selection-probabilities 1.0)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect," :deliberately-infect-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect isolate old/infirm,"
                     :deliberately-infect-p t :social-isolation-c-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del DIV Deliberately Infect Variolate,"
                     :deliberately-infect-p t :variolationp t :social-isolation-c-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Deliberately Infect incl Young Prot Old/V,"
                     :deliberately-infect-p t :variolationp t
                     :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
                     :quarantine-vulnerable-p t
                     :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+)))
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del DIVVPMV Deliberately Infect Variolate Vulnerablish Protect Most Vulnerable,"
                     :deliberately-infect-p t :variolationp t
                     :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (> (third el) 0.005) (< (third el) 0.20))) +age-health-sex-groups+))
                     :quarantine-vulnerable-p t
                     :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.20)) +age-health-sex-groups+))
                     :social-isolation-c-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Del BQ Deliberately Infect incl Young Prot Old/V Big Q,"
                     :deliberately-infect-p t :variolationp t
                     :deliberately-infect-groups (mapcar #'first (remove-if (lambda (el) (and (< (third el) 0.01))) +age-health-sex-groups+))
                     :quarantine-vulnerable-p t
                     :quarantine-vulnerable-groups (mapcar #'first (remove-if (lambda (el) (>= (third el) 0.01)) +age-health-sex-groups+))
                     :quarantine-deliberately-infected-beds-per-capita-fv 0.5
                     :quarantine-vulnerable-beds-per-capita-fv 0.5
                     :social-isolation-c-p t)
               (list :terse '(:summary) :pop-size pop :heading-prefix "Test Type," :summary-line-prefix "Antigen Testing," :antigen-testing-program-p t)
               ))))))

(defun temp-run-set ()
  (let ((firstp t))
  (loop for parms in *temp-overrides* do
       (rwo firstp  parms)
       (setf firstp nil))))
