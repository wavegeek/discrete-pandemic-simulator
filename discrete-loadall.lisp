;;; -*- lisp -*-

;; Copyright (C) 2020 by Tim Josling. See BSD.txt for terms.

(defparameter *speed-above-all* nil) ;; controls optimization and inlining

(if *speed-above-all*
    (defparameter *optimization-options* '(optimize (debug 0) (speed 3) (safety 0)))
    (defparameter *optimization-options* '(optimize (debug 3) (speed 0) (safety 3))))

;;(setf *read-default-float-format* 'double-float)
(setf *read-default-float-format* 'long-float)

(defun pp ()
  (if (not (find-package "CL-RANDIST")) (ql:quickload "cl-randist")) ;; avoid annoying repeated load messages
  (load "discrete.lisp"))

(defun ll ()
  (setf *read-default-float-format* 'long-float)
  (pp))

(ll)

;; (require :sb-sprof)
;;(sb-sprof:with-profiling (:max-samples 1000000 :mode :time :report :graph :loop nil) (time (rrr)))
